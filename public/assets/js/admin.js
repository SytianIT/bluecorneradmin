$(function() {

	var baseUrl = $('meta[name="base-url"]').attr('content');

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	if ($('#datatable').length && $('#datatable').find('tr').length > 3) {

		var table = $('#dataTables-example').DataTable({
			columnDefs : [
				{
					targets: [2],
					render: function ( data, type, full, meta ) {
						if (type == 'sort') {
							return $(data).find('span').hasClass('on') ? 1 : 0;
						}  else {
							return data;
						}
					}
				}
			]
		});

		$('.table-data').each(function() {
			var aoColumns = [],
				$this = $(this);

			var $doIndex,
				$doOrdering,
				options = {
					responsive: true,
					paging : false,
					filter : false,
					"bInfo": false
				};

			if( $(this).data('default-order') ) {
				$doIndex = i;
				$doOrdering = $(this).attr('data-default-order');
			} else {
				$doIndex = 0;
				$doOrdering = 'desc';
			}

			if( $(this).data('no-default-order') ) {
				options.order = [];
			} else {
				options.order = [[$doIndex, $doOrdering]];
			}

			if ($this.find('thead > tr').length == 1) {
				$this.find('thead th').each(function(i) {
					var sType = $(this).data("type"),
						oFalse = $(this).data("orderable");

					if(sType != '' || sType != undefined){
						aoColumns.push({ "type" : sType, "targets" : i });
						// if (sType != 'status-sort') {
						// 	aoColumns.push({ "type" : sType, "targets" : i });
						// } else {
						// 	aoColumns.push({ "targets" : i, "render" : 	function ( data, sType, full, meta ) {
						// 													alert(sType);
						//        if (stype == 'status-sort') {
						//            return $(data).find('span').hasClass('Move Up') ? 1 : 0;
						//        }  else {
						//            return data;
						//        }
						//   	}
						// 	});
						// }
					}
					if (oFalse != undefined){
						aoColumns.push({ "orderable" : oFalse, "targets" : i });
					}
					options.columnDefs = aoColumns;
				});
			}

			$.fn.dataTable.moment( 'MMM D, YYYY' );
			$this.DataTable(options);
		});
	}

	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true
	});


	$('.table-data').parent().parent().css('width', '100%');

	$(".confirm").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmWarning").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes, continue!",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmDelete").click(function(e) {

		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				_self.closest("form").submit();
			}
		});
	});

	$(".confirmCancelSubmit").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				_self.closest("form").submit();
			}
		});
	});

	$(".confirmSubmit").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);

		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				_self.closest("form").submit();
			}
		});


	});

});