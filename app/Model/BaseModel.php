<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

class BaseModel extends Model
{
    /*
     * Audit Creation Methods
     */

    public function getStrToLowerModelName()
    {
        return strtolower($this->modelName());
    }

    public function audit($type)
    {
        // $auditRepo = resolve(\Philbritish\Audit\AuditRepository::class);
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New {$this->getStrToLowerModelName()} entitled '{$this->title}'";
    }

    public function auditUpdate()
    {
        return "{$this->modelName()} entitled '{$this->title}'";
    }

    public function auditDelete()
    {
        return "{$this->modelName()} entitled '{$this->title}'";
    }

    public function auditRecover()
    {
        return "{$this->modelName()} entitled '{$this->title}'";
    }

    public function auditPermanent()
    {
        return "{$this->modelName()} entitled '{$this->title}'";
    }
}
