<?php

namespace App\Mail;

use BC\Repositories\Contest\Contest;
use BC\Repositories\Contest\ContestEntry;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackSubmission extends Mailable
{
    use Queueable, SerializesModels;

    public $entry;
    public $contest;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ContestEntry $entry, Contest $contest)
    {
        $this->entry = $entry;
		$this->contest = $contest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_FROM'))
					->markdown('email.notif-feedback-submission');
    }
}
