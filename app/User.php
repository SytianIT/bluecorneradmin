<?php

namespace App;

use BC\Repositories\Role\Role;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    const IMG_LOGO = 'logo';

    use Notifiable;

    public function roles() {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function audits()
    {
        return $this->morphMany(Audit::class, 'auditable');
    }

    public function getImgLogoAttribute() // img_icon
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_LOGO ? true : false;
        })->first();

        return $uploads;
    }

    public function getImgLogoPathAttribute() // img_icon_path
    {
        return $this->IMG_LOGO ? $this->img_logo->path : Uploadable::IMG_PLACEHOLDER;
    }

    /**
     * Custom Attributes
     */
    public function getNameAttribute()
    {
        return $this->first_name . (' ' . $this->last_name ?: '');
    }

    public function isSuperAdmin()
    {
        if ($this->roles->where('id', Role::SU_ID)->first()) {
            return true;
        }

        return false;
    }

    public function isAdmin()
    {
        if ($this->roles->where('id', '<>', Role::USER_ID)->first()) {
            return true;
        }

        return false;
    }

    public function isUser()
    {
        if ($this->roles->where('id', Role::USER_ID)->first()) {
            return true;
        }

        return false;
    }

    /*
     * Helper for Audits
     */

    public function auditPrefix()
    {
        return $this->isUser() ? 'User' : 'Admin';
    }

    public function audit($type)
    {
        $auditRepo = resolve(\Philbritish\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New {$this->auditPrefix()} '{$this->firstname}'";
    }

    public function auditUpdate()
    {
        return "{$this->auditPrefix()} '{$this->firstname}'";
    }

    public function auditRecover()
    {
        return "{$this->auditPrefix()} '{$this->firstname}'";
    }

    public function auditDelete()
    {
        return "{$this->auditPrefix()} '{$this->firstname}'";
    }

    public function auditDestroy()
    {
        return "{$this->auditPrefix()} '{$this->firstname}'";
    }
}
