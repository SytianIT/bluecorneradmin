<?php 

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller
{

	public function check()
	{
		$user = Auth::user();

		if ($user) {
			return response()->json(['success' => true, 'object' => $user->only('first_name', 'last_name', 'email')]);
		}

		return response()->json(['success' => false, 'object' => false]);
	}
	
}