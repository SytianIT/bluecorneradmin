<?php

namespace App\Http\Controllers\Apie;

use BC\Repositories\Settings\Setting;
use BC\Repositories\Settings\SettingRepository;
use Illuminate\Http\Request;

class SettingsController extends ApieController
{
	public function __construct(
		SettingRepository $settingRepo
	)
	{
		$this->settingRepo = $settingRepo;
	}

	public function fetchSettings(Request $request)
	{
		$settings = [];

		$settings = Setting::getSettings([
			'id', 'site_title', 'site_email', 'site_contact_no', 'homepage_title', 'homepage_sub_title', 'product_title', 'product_sub_title', 'updated_at'
		], [
			'uploads' => function ($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'sync_count', 'path');
			}
		]);

		if (count($settings) > 0) {
			$settings = $settings->setAppends([
				'tablet_sliders',
				'mobile_sliders'
			]);
			$settings = $this->settingRepo->cleanSettings($settings);
		}

		return response()->json($settings, 200);
    }
}
