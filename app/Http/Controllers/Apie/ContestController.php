<?php

namespace App\Http\Controllers\Apie;

use App\Http\Controllers\Controller;
use BC\Repositories\Contest\ContestRepository;
use Illuminate\Http\Request;

class ContestController extends ApieController
{
	public function __construct(
		ContestRepository $contestRepo
	)
	{
		$this->contestRepo = $contestRepo;
    }

	public function fetchContest(Request $request)
	{
		$contests = [];
		$request['actives'] = true;
		$request['published'] = true;
		$request['not_archive'] = 1;
        $contests = $this->contestRepo->search($request, [
            'contestItem' => function($q) {
                $q->select('id', 'contest_id', 'type', 'question', 'choices', 'order', 'updated_at');
            }
        ], ['id', 'title', 'slug', 'description', 'date_from', 'date_to', 'status', 'published', 'updated_at', 'sync_count']);

		if (count($contests) > 0) {
			$contests = $this->contestRepo->cleanContest($contests);
		}

		return response()->json(['contests' => $contests], 200);
    }

	public function submitContest(Request $request)
	{
		$result = $this->contestRepo->saveContestAnswer($request);
		$success = $result['success'];
		$message = $result['message'];
		$proceed = isset($result['proceed']) ? $result['proceed'] : false;

		return response()
			->json(['success' => $success, 'message' => $message, 'proceed' => $proceed], !$success ? 400 : 200);
	}
}
