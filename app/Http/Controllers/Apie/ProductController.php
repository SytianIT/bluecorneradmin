<?php

namespace App\Http\Controllers\Apie;

use BC\Repositories\Product\ProductRepository;
use BC\Repositories\ProductCategory\ProductCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends ApieController
{
	public function __construct(
		ProductRepository $productRepo,
		ProductCategoryRepository $categoryRepo
	)
	{
		$this->productRepo = $productRepo;
		$this->categoryRepo = $categoryRepo;
    }

	public function fetchProducts(Request $request)
	{
		$products = [];
		$request['api_fetch'] = 1;
		$products = $this->productRepo->search($request, [
			'sizes' => function($q) {
				$q->select('product_sizes.id', 'product_sizes.product_id', 'product_sizes.size_id', 'sizes.name', 'sizes.description', 'sizes.width', 'sizes.length', 'sizes.order', 'sizes.updated_at');
			},
			'uploads' => function($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
			}
		], [
			'id', 'category_id', 'name', 'description', 'has_styles', 'updated_at', 'sync_count', 'status', 'deleted_at'
		]);

		/*$products = $result['products'];
		$totalCount = $result['totalCount'];*/

		if ($products->count() > 0) {
			$products = $this->productRepo->cleanProduct($products, $request->path_only ?: false);
		}

		$doSkip = true;
		if ($request->filled('do_skip')) {
			$doSkip = false;
		}

		return response()->json(['products' => $products/*, 'do_skip' => $doSkip, 'skip_count' => $request->skip_count, 'total_count' => $totalCount*/], 200);
    }

	public function fetchProductVariations(Request $request)
	{
		$colors = [];
		$request['api_fetch'] = 1;
		$productVariations = $this->productRepo->searchProductVariation($request, [
			'uploads' => function ($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
			},
			'color' => function($q) {
				$q->select('id', 'name', 'value', 'order', 'orderable', 'updated_at');
			},
			'style' => function($q) {
				$q->select('id', 'product_id', 'name', 'order', 'updated_at');
			}

		], [
			'product_id', 'id', 'style_id', 'color_id', 'updated_at', 'deleted_at', 'sync_count', 'order as variation_order'
		]);

		if ($productVariations->count() > 0) {
			$productVariations = $this->productRepo->cleanVariation($productVariations, $request->path_only ?: false);
		}

		$product = [];
		$product = $this->productRepo->search($request, [
			'sizes' => function($q) {
				$q->select('product_sizes.id', 'product_sizes.product_id', 'product_sizes.size_id', 'sizes.name', 'sizes.description', 'sizes.width', 'sizes.length', 'sizes.order', 'sizes.updated_at');
			},
		]);
        $product = $this->productRepo->cleanProduct($product, $request->path_only ?: false);

		return response()->json(['product_variations' => $productVariations, 'sizes' => $product ? $product : []], 200);
    }
}
