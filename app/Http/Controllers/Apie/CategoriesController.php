<?php

namespace App\Http\Controllers\Apie;

use BC\Repositories\ProductCategory\ProductCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends ApieController
{
	public function __construct(
		ProductCategoryRepository $productCateRepo)
	{
		$this->productCateRepo = $productCateRepo;
	}

	public function fetchCategories(Request $request)
	{
		$categories = [];
		$categories = $this->productCateRepo->search($request, [
			'uploads' => function ($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
			},
			'childs' => function ($q) {
				$q->with([
					'uploads' => function ($q) {
						$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
					}
				])
					->select('id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'sync_count');
			}
		], ['id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'deleted_at', 'sync_count']);

		if (count($categories) > 0) {
			$categories = $this->productCateRepo->cleanCategory($categories);
		}

		return response()->json(['categories' => $categories], 200);
	}
}
