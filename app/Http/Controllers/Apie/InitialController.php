<?php

namespace App\Http\Controllers\Apie;

use BC\Repositories\Contest\ContestRepository;
use BC\Repositories\ProductCategory\ProductCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InitialController extends ApieController
{
	public function __construct(
		ProductCategoryRepository $productCateRepo,
		ContestRepository $contestRepo)
	{
		$this->productCateRepo = $productCateRepo;
		$this->contestRepo = $contestRepo;
	}

	public function fetchInitial(Request $request)
	{
        $categories = [];
        $categories = $this->productCateRepo->search($request, [
            'uploads' => function ($q) {
                $q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
            },
            'childs' => function ($q) {
                $q->with([
                    'uploads' => function ($q) {
                        $q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
                    }
                ])
                    ->select('id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'sync_count');
            }
        ], ['id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'deleted_at', 'sync_count']);

        if (count($categories) > 0) {
            $categories = $this->productCateRepo->cleanCategory($categories);
        }

        $ftCategories = $categories->filter(function($item) {
            return $item->featured ?: false;
        });

		/*$ftCategories = [];
		$ftCategories = $this->productCateRepo->search($request, [
			'uploads' => function ($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
			}
		], ['id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'sync_count']);*/

		if (count($ftCategories) > 0) {
			$ftCategories = $this->productCateRepo->cleanCategory($ftCategories);
		}

		/*unset($request['featured']);
		$categories = [];
		$categories = $this->productCateRepo->search($request, [
			'uploads' => function ($q) {
				$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
			},
			'childs' => function ($q) {
				$q->with([
					'uploads' => function ($q) {
						$q->select('id', 'uploadable_id', 'uploadable_type', 'key', 'filename', 'path', 'sync_count');
					}
				])
				->select('id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'sync_count');
			}
		], ['id', 'name' , 'description', 'parent_id', 'featured', 'updated_at', 'deleted_at', 'sync_count']);

		if (count($categories) > 0) {
			$categories = $this->productCateRepo->cleanCategory($categories);
		}*/

		$contests = [];
		$request['published'] = true;
		$request['actives'] = true;
		$request['not_archive'] = 1;
		$contests = $this->contestRepo->search($request, [
			'contestItem' => function($q) {
				$q->select('id', 'contest_id', 'type', 'question', 'choices', 'order', 'updated_at');
			}
		], ['id', 'title', 'slug', 'description', 'date_from', 'date_to', 'status', 'published', 'updated_at', 'sync_count']);

		if (count($categories) > 0) {
			$contests = $this->contestRepo->cleanContest($contests);
		}
		return response()->json(['categories' => $categories, 'ft_categories' => $ftCategories, 'contests' => $contests], 200);
	}
}
