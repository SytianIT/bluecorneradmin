<?php

namespace App\Http\Controllers;

use BC\Validation\ValidationException;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function redirectFormRequest(ValidationException $e, $ajax = false)
    {
        if ($ajax) {
            return response()->json($e->getErrors(), 422);
        }
        
        return response()->json(['success' => false, 'error' => $e->getErrors()]);
    	// return redirect()->back(
                    //  ->withInput())
					// 	->withErrors($e->getErrors())
					// 	->withError("Please check required fields.");

    }
}
