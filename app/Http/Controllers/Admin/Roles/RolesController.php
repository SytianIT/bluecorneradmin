<?php

namespace App\Http\Controllers\Admin\Roles;

use BC\ContextInterface;
use BC\Repositories\Role\Role;
use BC\Repositories\Role\RoleRepository;
use BC\Validation\ValidationException;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class RolesController extends Controller
{
	public function __construct(RoleRepository $roleRepo) {

        $this->roleRepo = $roleRepo;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::withCount('users')->with('users')->get();
        if ($roles) {
            $itemarrays = $roles->values();
            return response()->json(['success' => true, 'object' => $roles]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Role $roles)
    {
       /* $categories = ProductCategory::all();
        $itemarrays = $categories->values();

        if ( $itemarrays ) {

            return response()->json(['success' => true, 'object' => $itemarrays]);
        
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Role $roles)
    {
        try {
            $product = $this->roleRepo->store($request, $roles);
            if ( ! $product->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($product->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Role Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $role_id)
    {
        $role = Role::find($role_id);
        if ($role) {
            return response()->json(['success' => true, 'object' => $role]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        try {
            $roles = $this->roleRepo->update($request);
            if ( ! $roles->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($roles->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Role Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $role_id)
    {
        try {
            $roleName = $this->roleRepo->delete($request, $role_id);
            $roles = Role::withCount('users')->with('users')->get();
            return response()->json(['success' => true, 'deleted' => $roleName , 'object' => $roles ]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}