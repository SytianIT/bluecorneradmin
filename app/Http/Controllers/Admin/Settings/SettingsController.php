<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;

use BC\Repositories\Settings\Setting;
use BC\Repositories\Settings\SettingRepository;
use BC\Validation\ValidationException;
use BC\Repositories\Uploadable\Uploadable;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
	public function __construct(Uploadable $uploadable, SettingRepository $settingsRepo) {
        $this->modelUploadables = $uploadable;
        $this->settingsRepo = $settingsRepo;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::find(1);
        // $thumb_mobile = isset($settings) && $settings->exists ? asset($settings->featured_mobile_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        // $thumb_tablet = isset($settings) && $settings->exists ? asset($settings->featured_tablet_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        if ( isset($settings) && $settings->exists ) {
            $arrayMobileImage = [];
            $arrayTabletImage = [];
            foreach ( $settings->uploads as $mobile ) {
                if ( $mobile->key == 'featured_mobile' ) {
                    $draw = [
                        'mobile_thumbnail' => asset($mobile->path),
                        'filename' => $mobile->filename,
                        'upload_id' => $mobile->id
                    ];
                    $arrayMobileImage[] = $draw;
                } else {
                    $draw = [
                        'mobile_thumbnail' => asset($mobile->path),
                        'filename' => $mobile->filename,
                        'upload_id' => $mobile->id
                    ];
                    $arrayTabletImage[] = $draw;
                }
            }
        }
        if ( $settings ) {
            return response()->json(['success' => true, 'object' => $settings, 'thumb_mobile' => $arrayMobileImage, 'thumb_tablet' => $arrayTabletImage]);
        } 
        return response()->json(['success' => false]);
    }
    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $settings = $this->settingsRepo->update($request);
            if ( !$settings->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($settings->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Settings Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    public function remove(Request $request, $UploadId)
    {
        $image = $this->modelUploadables->find($UploadId); // removed contest
        $image->delete();
        if ( $image ){
            return response()->json(['success' => true, 'object' => 'Image Successfully Deleted']);
        }
    }
}