<?php

namespace App\Http\Controllers\Admin\Contests;

use App\Http\Controllers\Controller;

use BC\Repositories\Contest\Contest;
use BC\Repositories\Contest\ContestRepository;
use BC\Repositories\Items\Item;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class ContestsController extends Controller
{
	public function __construct(ContestRepository $contestsRepo) {
        $this->contestsRepo = $contestsRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Contest $contests)
    {
        $contests = Contest::where('has_entries','!=',1)->withCount('contestItem')->get();
        if ( $contests ) {
            $arrayImage = [];
            foreach ( $contests as $key => $args ):
                $hasEntries = $args->contestEntries()->get();
                $hasEntries = $hasEntries->count();
                $draw = [
                    'id' => $args->id,
                    'thumbnail' => $args->contest_thumb_mobile_path,
                    'title' => $args->title,
                    'count_items' => $args->contest_item_count,
                    'date_from' => $args->date_from,
                    'date_to' => $args->date_to,
                    'status' => $args->status,
                    'has_entries' => $hasEntries,
                ];
                $arrayImage[] = $draw;
            endforeach;
            return response()->json(['success' => true, 'object' => $arrayImage]);
        }
    }

    public function archiveIndex(Contest $contests)
    {
        $contests = Contest::where('has_entries','>',0)->withCount('contestItem')->get();
        if ( $contests ) {
            $arrayImage = [];
            foreach ( $contests as $key => $args ):
                $hasEntries = $args->contestEntries()->get();
                $hasEntries = $hasEntries->count();
                $draw = [
                    'id' => $args->id,
                    'thumbnail' => $args->contest_thumb_mobile_path,
                    'title' => $args->title,
                    'count_items' => $args->contest_item_count,
                    'date_from' => $args->date_from,
                    'date_to' => $args->date_to,
                    'status' => $args->status,
                    'has_entries' => $hasEntries,
                ];
                $arrayImage[] = $draw;
            endforeach;
            return response()->json(['success' => true, 'object' => $arrayImage]);
        }
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Contest $contests)
    {

    }

    /**
     * Show the form for storing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Contest $contests)
    {
        try {
            $contest = $this->contestsRepo->store($contests, $request);
            if ( !$contest->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($contest->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'args' => $contest->id , 'object' => 'Contest Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    public function moveToArchive(Contest $contest, Request $request) {
        try {
            $contest = $this->contestsRepo->moveToArchive($contest, $request);

            if (!$contest->save()) {
				return response()->json(['message' => $contest->title.' Failed to move to archive!', 'success' => FALSE]);
			}

            return response()->json(['message' => $contest->title.' Successfull moved to archived!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Contest $contests, $contest_id)
    {
        $contests = Contest::find($contest_id);

        $contest_mobile_banner = isset($contests) && $contests->exists ? asset($contests->contest_banner_mobile_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        $contest_mobile_banner_atts = $contests->getContestBannerMobileAttribute($contests);

        $contest_tablet_banner = isset($contests) && $contests->exists ? asset($contests->contest_banner_tablet_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        $contest_tablet_banner_atts = $contests->getContestBannerTabletAttribute($contests);

        $format_contest_mobile_banner[] = [
            'name'     => '',
            'old_name' => isset($contest_mobile_banner_atts->filename) ? $contest_mobile_banner_atts->filename: null,
            'id'       => isset($contest_mobile_banner_atts->id) ? $contest_mobile_banner_atts->id: null,
            'src'      => $contest_mobile_banner,
        ];
        $contests['banner_mobile'] = $format_contest_mobile_banner;

        $format_contest_tablet_banner[] = [
            'name'     => '',
            'old_name' => isset($contest_tablet_banner_atts->filename) ? $contest_tablet_banner_atts->filename: null,
            'id'       => isset($contest_tablet_banner_atts->id) ? $contest_tablet_banner_atts->id: null,
            'src'      => $contest_tablet_banner,
        ];
        $contests['banner_tablet'] = $format_contest_tablet_banner;

        $contest_mobile_thumb = isset($contests) && $contests->exists ? asset($contests->contest_thumb_mobile_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        $contest_mobile_thumb_atts = $contests->getContestThumbMobileAttribute($contests);

        $contest_tablet_thumb = isset($contests) && $contests->exists ? asset($contests->contest_thumb_tablet_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        $contest_tablet_thumb_atts = $contests->getContestThumbTabletAttribute($contests);

        $format_contest_mobile_thumb[] = [
            'name'     => '',
            'old_name' => isset($contest_mobile_thumb_atts->filename) ? $contest_mobile_thumb_atts->filename: null,
            'id'       => isset($contest_mobile_thumb_atts->id) ? $contest_mobile_thumb_atts->id: null,
            'src'      => $contest_mobile_thumb,
        ];
        $contests['thumb_mobile'] = $format_contest_mobile_thumb;

        $format_contest_tablet_thumb[] = [
            'name'     => '',
            'old_name' => isset($contest_tablet_thumb_atts->filename) ? $contest_tablet_thumb_atts->filename: null,
            'id'       => isset($contest_tablet_thumb_atts->id) ? $contest_tablet_thumb_atts->id: null,
            'src'      => $contest_tablet_thumb,
        ];
        $contests['thumb_tablet'] = $format_contest_tablet_thumb;

        // dd( $contests );

        if ($contests) {
            return response()->json([
                'success' => true,
                'object' => $contests
            ]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contest_id){
        try {
            $contests = $this->contestsRepo->update($request);
            if ( ! $contests->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($contests->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true,  'args' => $contest_id, 'object' => 'Contest Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Item $items, $contests_id)
    {
        try {
            $contestsName = $this->contestsRepo->delete($request, $items, $contests_id);
            $contests = Contest::all();
            if ( $contests ) {
                return response()->json(['success' => true, 'object' => $contests ]);
            }
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
