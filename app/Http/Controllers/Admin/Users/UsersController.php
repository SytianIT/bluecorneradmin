<?php

namespace App\Http\Controllers\Admin\Users;

use BC\ContextInterface;
use BC\Repositories\Role\Role;
use BC\Repositories\Users\UserRepository;
use BC\Validation\ValidationException;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{

    public function __construct(UserRepository $userRepo) {
        $this->userRepo = $userRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withCount('roles')->get();
        $userCurrentlyLogin = Auth::user()->id;
        if ($users) {
            $arrayImage = [];
            foreach ( $users as $key => $args ):
                if ( $userCurrentlyLogin == $args->id ) {
                } else {
                    $draw = [
                        'id' => $args->id,
                        'thumbnail' => $args->img_logo_path,
                        'first_name' => $args->first_name,
                        'last_name' => $args->last_name,
                        'email' => $args->email,
                        'roles' => $args->roles,
                        'roles_count' => $args->roles_count,
                        'active' => $args->active
                    ];
                    $arrayImage[] = $draw;
                }
            endforeach;
            return response()->json(['success' => true, 'object' => $arrayImage]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Role $roles)
    {
        
        $roles = Role::all();
        $itemarrays = $roles->values();
        if ( $itemarrays ) {
            return response()->json(['success' => true, 'object' => $itemarrays]);
        }

    }

    /**
     * Show the form for storing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        try {
            $user = $this->userRepo->store($user, $request);
            if ( ! $user->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($user->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'User Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $user_id)
    {
        $user = User::with('roles')->get()->find($user_id);
        $image = isset($user) && $user->exists ? asset($user->img_logo_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        if ($user) {
            return response()->json(['success' => true, 'object' => $user, 'image' => $image, 'user_role' => $user->roles ]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        try {
            $user = $this->userRepo->update($request);
            if ( ! $user->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($user->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'User Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $userID)
    {
        try {
            $userName = $this->userRepo->delete($request, $userID);
            $users = User::with('roles')->get();
            if ($users) {
                $arrayImage = [];
                foreach ( $users as $key => $args ):
                    $draw = [
                        'id' => $args->id,
                        'thumbnail' => $args->img_logo_path,
                        'first_name' => $args->first_name,
                        'last_name' => $args->last_name,
                        'email' => $args->email,
                        'roles' => $args->roles,
                        'active' => $args->active
                    ];
                    $arrayImage[] = $draw;
                endforeach;
            }
            return response()->json(['success' => true, 'deleted' => $userName , 'object' => $arrayImage ]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

}
