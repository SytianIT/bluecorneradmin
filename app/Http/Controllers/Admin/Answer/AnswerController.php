<?php

namespace App\Http\Controllers\Admin\Answer;

use App\Http\Controllers\Controller;
use App\User;
use BC\Repositories\Contest\Contest;
use BC\Repositories\Contest\ContestEntry;
use BC\Repositories\Contest\ContestEntryAnswer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function __construct() {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Contest $contest,ContestEntry $contestEntry)
    {
        $contest = $contest->all();
        $contest = $contest->map(function($item, $key) {
            $newItem = [
                'label' => $item['title'],
                'value' => $item['id'],
            ];

            $item = $newItem;
            return $item;
        });

        $contestEntries = $contestEntry->orderBy('created_at', 'desc')->where('has_entries','!=',1)->get();
        $contestEntries = $contestEntries->map(function($item) {
            $item['created_at_formatted'] = $item->created_at->format('m/d/Y');
            return $item;
        });
        return response()->json(['success' => true, 'object' => $contestEntries, 'feedback'=> $contest]);
    }

    public function archiveIndex(Contest $contest,ContestEntry $contestEntry)
    {
        $contest = $contest->all();
        $contest = $contest->map(function($item, $key) {
            $newItem = [
                'label' => $item['title'],
                'value' => $item['id'],
            ];

            $item = $newItem;
            return $item;
        });

        $contestEntries = $contestEntry->orderBy('created_at', 'desc')->where('has_entries','>',0)->get();
        $contestEntries = $contestEntries->map(function($item) {
            $item['created_at_formatted'] = $item->created_at->format('m/d/Y');
            return $item;
        });
        return response()->json(['success' => true, 'object' => $contestEntries, 'feedback'=> $contest]);
    }

    public function filteredIndex(Contest $contest)
    {
        $contestEntriesFiltered = $contest->contestEntries()->orderBy('created_at', 'desc')->get();
        $contestEntriesFiltered = $contestEntriesFiltered->map(function($item) {
            $item['created_at_formatted'] = $item->created_at->format('m/d/Y');
            return $item;
        });
        return response()->json(['success' => true, 'object' => $contestEntriesFiltered ]);
    }

    public function show(ContestEntry $contestEntry)
    {
        $contest_entry_answer = $contestEntry->answers()->get();
        $contest_entry_answer->map(function($item) use($contestEntry) {
            $jsonAnswers = json_decode($item->answer);
            if (is_array($jsonAnswers)) {
                $item['answer'] = $jsonAnswers;
                $item['is_array'] = true;
            }

            return $item;
        });

        return response()->json([ 'success' => true, 'object' => $contest_entry_answer,'title'=> $contestEntry->name, 'birthday'=> $contestEntry->birthday, 'contact_number'=> $contestEntry->contact_number, 'email' =>$contestEntry->email ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(ContestEntry $contestEntry)
    {
        $contestEntry->delete();
        $allContestEntry = $contestEntry->all();

        return response()->json(['message' => 'Feedback '.$contestEntry->name.' from '.$contestEntry->contest_title.' Successfully deleted!', 'success' => TRUE, 'object'=> $allContestEntry ]);
    }
}
