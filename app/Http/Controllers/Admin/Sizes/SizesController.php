<?php

namespace App\Http\Controllers\Admin\Sizes;

use App\Http\Controllers\Controller;

use BC\Repositories\SizesCategory\SizesCategory;
use BC\Repositories\Sizes\Sizes;
use BC\Repositories\Sizes\SizesRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class SizesController extends Controller
{

    public function __construct(SizesRepository $sizesRepo) {
        $this->sizesRepo     = $sizesRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sizes = Sizes::all();

        $allSizes = [];

        $sizes->each(function($item) use (&$allSizes) {
            $cat = $item->sizeCategory;
            $allSizes[] = [
                'id' => $item['id'],
                'name' => $item['name'],
                'category' => ($cat['name']) ? $cat['name'] : 'Assign Category',
                'width' => $item['width'],
                'length' => $item['length'],
                'description' => $item['description']
            ];
        });

        return response()->json(['success' => true, 'object' => $allSizes]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Sizes $sizes)
    {
        try {
            $sizes = $this->sizesRepo->store($sizes, $request);

            if ( ! $sizes->save() ) {
                return response()->json(['message' => $request->name.' Size did not save successfully!', 'success' => FALSE]);
            }
            return response()->json(['message' => $request->name.'  Size Added Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Sizes $sizes)
    {
        $sizeCat = $sizes->sizeCategory;
        $sizeCategories = [];
        if( $sizeCat != null ) {
            $sizeCategories = [
                'label' => $sizeCat['name'],
                'value' => $sizeCat['id']
            ];
        }
        $sizes['sizescat'] = $sizeCategories;
        return response()->json(['success' => true, 'object' => $sizes, 'sizecategory' => $sizeCategories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Sizes $sizes, Request $request)
    {
        try {
            $sizes = $this->sizesRepo->update($sizes, $request);

            if (!$sizes->save()) {
				return response()->json(['message' => $sizes->name.' size did not save succesfully!', 'success' => FALSE]);
			}

            return response()->json(['message' => $sizes->name.' Update Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Sizes $sizes)
    {
        $this->sizesRepo->delete($sizes);
        $allSizes = Sizes::all();

        return response()->json(['message' => $sizes->name.' Successfully deleted!', 'success' => TRUE, 'object'=> $allSizes ]);
    }
}
