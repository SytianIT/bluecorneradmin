<?php

namespace App\Http\Controllers\Admin\Audit;

use App\Http\Controllers\Controller;

use App\User;
use BC\Repositories\Audit\Audit;
use BC\Repositories\Audit\AuditRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class AuditController extends Controller
{

    public function __construct(AuditRepository $auditRepo) {
        $this->auditRepo = $auditRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Audit $audit, User $user)
    {
        $auditLogs = $audit->orderBy('created_at', 'desc')->get();
        $auditLogs->each(function($item) use($user){
            $user_id = $item['user_id'];
            $user = $user->where('id',$user_id)->first();
            $item['name'] = $user;
        });

        return response()->json(['success' => true, 'object' => $auditLogs, 'name' => $user]);
	}
}
