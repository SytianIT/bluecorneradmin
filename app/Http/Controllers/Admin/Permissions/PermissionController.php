<?php

namespace App\Http\Controllers\Admin\Permissions;

use App\Http\Controllers\Controller;

use BC\Repositories\Contest\Contest;
use BC\Repositories\Permission\Permission;
use BC\Repositories\Permission\PermissionRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class PermissionController extends Controller
{
	public function __construct(PermissionRepository $permissionRepo) {
        $this->permissionRepo = $permissionRepo;
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $permissions = Permission::all();
        if ($permissions) {
            return response()->json(['success' => true, 'object' => $permissions ]);
        }
        return response()->json(['success' => false, 'object' => false]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    public function store(Request $request)
    {
        try {
            $permissions = $this->permissionRepo->store($request);
            if ( !$permissions->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($permissions->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Permission Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    public function edit(Request $request, $permission_id) 
    {
        $permissions = Permission::find($permission_id);
        if ($permissions) {
            return response()->json(['success' => true, 'object' => $permissions, 'name' => $permissions->title ]);
        }
    }

    public function update(Request $request, $permission_id) 
    {
        $result = $this->permissionRepo->update($request, $permission_id);
        if ( $result ) {
            return response()->json(['success' => true, 'object' => 'Permission Successfully Updated']);
        }
    }

    public function destroy(Request $request, $permission_id) 
    {
        $result = $this->permissionRepo->delete($request, $permission_id);
        $permissions = Permission::all();
        if ( $result ) {
            return response()->json(['success' => true, 'deleted' => $result , 'object' => $permissions]);
        }
    }
}