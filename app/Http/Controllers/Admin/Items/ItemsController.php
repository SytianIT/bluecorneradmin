<?php

namespace App\Http\Controllers\Admin\Items;

use App\Http\Controllers\Controller;

use BC\Repositories\Contest\Contest;
use BC\Repositories\Items\Item;
use BC\Repositories\Items\ItemRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class ItemsController extends Controller
{
	public function __construct(ItemRepository $itemsRepo) {
        $this->itemsRepo = $itemsRepo;
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	}

    public function store(Request $request)
    {
        $items = $this->itemsRepo->store($request);
        if ( $items ){ 
            return response()->json(['success' => true, 'object' => 'Items Successfully added']);
        }
    }

    public function edit(Request $request, $contest_id) 
    {
        $result = Item::where('contest_id', $contest_id)->orderBy('order','asc')->get();
        $contest = Contest::find($contest_id);
        if ($result) {
            return response()->json(['success' => true, 'object' => $result, 'name' => $contest->title ]);
        }
    }

    public function update(Request $request) 
    {
        $result = $this->itemsRepo->update($request);
        if ( $result ) {
            return response()->json(['success' => true, 'object' => 'Item Successfully Updated']);
        }
    }

    public function destroy(Request $request, $contestId, $itemId) 
    {
        $result = $this->itemsRepo->delete($request, $itemId);
        $items = Item::all();
        if ( $result ) {
            return response()->json(['success' => true, 'object' => $items]);
        }
    }
}