<?php
namespace App\Http\Controllers\Admin\SizesCategory;

use App\Http\Controllers\Controller;

use BC\Repositories\SizesCategory\SizesCategory;
use BC\Repositories\SizesCategory\SizesCategoryRepository;

use BC\Repositories\Sizes\Sizes;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;

class SizesCategoryController extends Controller
{

    public function __construct(SizesCategoryRepository $sizesCategoryRepo) {
        $this->sizesCategoryRepo = $sizesCategoryRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $sizes = SizesCategory::all();

        return response()->json(['success' => true, 'object' => $sizes]);
    }

    public function getSizeCategories()
    {
        $sizes = SizesCategory::all();

        $sizeCategories = [];
        $sizes->each(function($item) use (&$sizeCategories) {
            $sizeCategories[] = [
                'label' => $item->name,
                'value' => $item->id
            ];
        });

        return response()->json(['success' => true, 'object' => $sizeCategories]);
    }

    public function getSizePerCategories(SizesCategory $sizecat) {
        $allSizes = $sizecat->size;

        $allSizes = array_values(array_sort($allSizes, function($val) {
            return $val['order'];
        }));
        return response()->json(['success' => true, 'allSizes' => $allSizes]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SizesCategory $sizesCategory)
    {
        try {
            $sizesCategory = $this->sizesCategoryRepo->store($sizesCategory, $request);

            if ( ! $sizesCategory->save() ) {
                return response()->json(['message' => $request->name.' Size Category did not save successfully!', 'success' => FALSE]);
            }
            return response()->json(['message' => $request->name.'  Size Category Added Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, SizesCategory $sizecatid)
    {
        $allSizes = $sizecatid->size;

        $allSizes = array_values(array_sort($allSizes, function($val) {
            return $val['order'];
        }));

        return response()->json(['success' => true, 'object' => $sizecatid, 'allSizes' => $allSizes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // SizesCategory $sizecatid
    public function update(Request $request)
    {
        try {
            $sizesCategory = $this->sizesCategoryRepo->update($request);

            if (!$sizesCategory->save()) {
				return response()->json(['message' => $sizesCategory->name.' size category did not save succesfully!', 'success' => FALSE]);
			}

            return response()->json(['message' => $sizesCategory->name.' Update Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(SizesCategory $sizecatid)
    {
        $sizecatid = $this->sizesCategoryRepo->delete($sizecatid);
        $allSizes = SizesCategory::all();

        return response()->json(['message' => $sizecatid.' Successfully deleted!', 'success' => TRUE, 'object'=> $allSizes ]);
    }
}
