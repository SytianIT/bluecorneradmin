<?php

namespace App\Http\Controllers\Admin\Products;

use App\Http\Controllers\Controller;

use BC\Repositories\Color\Color;
use BC\Repositories\ProductCategory\ProductCategory;
use BC\Repositories\ProductVariation\ProductVariation;
use BC\Repositories\ProductVariation\ProductVariationRepository;
use BC\Repositories\Product\Product;
use BC\Repositories\Product\ProductRepository;
use BC\Repositories\SizesCategory\SizesCategory;
use BC\Repositories\Sizes\Sizes;
use BC\Repositories\Style\Style;
use BC\Repositories\Style\StyleRepository;

use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{

    public function __construct(ProductRepository $productRepo, StyleRepository $styleRepo, ProductVariationRepository $pvRepo)
    {
        $this->productRepo = $productRepo;
        $this->styleRepo   = $styleRepo;
        $this->pvRepo      = $pvRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( ProductCategory $productCategory )
    {

        $products = Product::with('category')->get();
        $categories = ProductCategory::all();
        $itemarrays = $categories->values();
        $AllProductCategories = $productCategory->getNestedList();

        if ($products) {
            return response()->json(['success' => true, 'object' => $products, 'category' => $AllProductCategories ]);
        }
        return response()->json(['success' => false, 'object' => false]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Product $product, ProductCategory $productCategory)
    {
        $colors = Color::all();
        $sizes = Sizes::all();
        $categories = ProductCategory::all();
        $itemarrays = $categories->values();
        $AllProductCategories = $productCategory->getNestedList();

        if ( $itemarrays ) {
            return response()->json(['success' => true, 'object' => $AllProductCategories, 'colors' => $colors, 'sizes' => $sizes ]);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, Style $style, ProductVariation $productVariation)
    {
        try {
            $sizeID = $request->size_ids;
            $colorID = $request->color_ids;
            $request['size_ids'] = array_pluck($sizeID, 'value');
            $request['color_ids'] = array_pluck($colorID, 'value');
            $product = $this->productRepo->store($product, $request);
            if ( ! $product->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($product->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Product Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Product $product)
    {
        $product->load(['colors','sizes','sizeCategory', 'sizeCategory.size', 'styles','styles.productVariations','styles.productVariations.color','productVariations','productVariations.color']);
        $allcolors = Color::all();
        $allsizes = ($product->sizeCategory) ? $product->sizeCategory->size : Sizes::all();
        $colors = $product->colors;
        $sizes = $product->sizes;
        $styles = $product->styles;
        $sizeCat = $product->sizeCategory;
        $productVariations = $product->productVariations;
        $product_featured_image = isset($product) && $product->exists ? ( $product->product_featured_image_path != '' ? asset($product->product_featured_image_path) : null ) : null;
        $product_featured_image_atts = $product->getProductFeaturedImageAttribute($product);
        $product_mobile_banner = isset($product) && $product->exists ? ( $product->product_mobile_banner_image_path != '' ? asset($product->product_mobile_banner_image_path) : null ) : null;
        $product_mobile_banner_atts = $product->getProductMobileBannerImageAttribute($product);
        $product_tablet_banner = isset($product) && $product->exists ? ( $product->product_tablet_banner_image_path != '' ? asset($product->product_tablet_banner_image_path) : null ) : null;
        $product_tablet_atts = $product->getProductTabletBannerImageAttribute($product);

        $format_product_featured_image[] = [
            'name'     => '',
            'old_name' => isset($product_featured_image_atts->filename) ? $product_featured_image_atts->filename: null,
            'id'       => isset($product_featured_image_atts->id) ? $product_featured_image_atts->id: null,
            'url'      => $product_featured_image,
        ];
        $product['product_featured_image_info'] = $format_product_featured_image;

        $format_product_mobile_banner[] = [
            'name'     => '',
            'old_name' => isset($product_mobile_banner_atts->filename) ? $product_mobile_banner_atts->filename: null,
            'id'       => isset($product_mobile_banner_atts->id) ? $product_mobile_banner_atts->id: null,
            'url'      => $product_mobile_banner,
        ];
        $product['product_mobile_banner_info'] = $format_product_mobile_banner;

        $format_product_tablet_banner[] = [
            'name'     => '',
            'old_name' => isset($product_tablet_atts->filename) ? $product_tablet_atts->filename: null,
            'id'       => isset($product_tablet_atts->id) ? $product_tablet_atts->id: null,
            'url'      => $product_tablet_banner,
        ];
        $product['product_tablet_banner_info'] = $format_product_tablet_banner;

        $sizesWrapper = array_values(array_sort($sizes, function($val){
            return $val['order'];
        }));

        $formattedSizes = [];
        foreach( $sizesWrapper as $item ) {
            $formattedSizes[] = [
                'label' => $item->name,
                'value' => $item->id
            ];
        }
        // $sizesWrapper->each(function($item) use(&$formattedSizes) {
        //     $formattedSizes[] = [
        //         'label' => $item->name,
        //         'value' => $item->id
        //     ];
        // });
        $product['size_ids'] = $formattedSizes;

        $formattedColors = [];
        $productVariations->each(function($item) use(&$formattedColors) {
            $product_variation_image = isset($item) && $item->exists ? ( $item->product_variation_image_path != '' ? asset($item->product_variation_image_path) : null ) : null;
            $product_variation_image_atts = $item->getProductVariationImageAttribute($item);
            $product_variation_color = isset($item) && $item->exists ? ( $item->product_variation_color_path != '' ? asset($item->product_variation_color_path) : null ) : null;
            $product_variation_color_atts = $item->getProductVariationColorAttribute($item);
            if( $item->style_id == NULL ) {
                $formattedColors[] = [
                    'id'    => $item->id,
                    'order' => $item->order,
                    'label' => $item->color->name,
                    'value' => $item->color->id,
                    'hex'   => $item->color->value,
					'productvariation_image_name'     => '',
					'productvariation_image_old_name' => isset($product_variation_image_atts->filename) ? $product_variation_image_atts->filename : null,
					'productvariation_image_id'       => isset($product_variation_image_atts->id) ? $product_variation_image_atts->id : null,
					'productvariation_image_url'      => $product_variation_image,
					'productvariation_color_name'     => '',
					'productvariation_color_old_name' => isset($product_variation_color_atts->filename) ? $product_variation_color_atts->filename: null,
					'productvariation_color_id'       => isset($product_variation_color_atts->id) ? $product_variation_color_atts->id : null,
					'productvariation_color_url'      => $product_variation_color,
                ];
            }
        });

        $formattedColors = array_values(array_sort($formattedColors, function($val) {
            return $val['order'] != null ? $val['order'] : 999;
        }));

        $product['color_ids']  = $formattedColors;
        $product['colorPanel'] = $formattedColors;

        $sizeCatToArray = ($sizeCat) ? $sizeCat->toArray() : '';
        $sizeCatArr = [];
        if( $sizeCatToArray ) {
            $sizeCatArr[] = [
                'label' => $sizeCatToArray['name'],
                'value' => $sizeCatToArray['id']
            ];
        }
        $product['sizeCat'] = ($sizeCatArr) ? $sizeCatArr : '';

        // dd($product['colorPanel']);

        $formattedStyles = [];
        $styles->each(function($item) use(&$formattedStyles, $colors, $productVariations) {
            $formattedColorStyles = [];
            $thisStyleVariations = $item->productVariations;
            foreach ($thisStyleVariations as $variation) {
                $product_variation_image = isset($variation) && $variation->exists ? ( $variation->product_variation_image_path != '' ? asset($variation->product_variation_image_path) : null ) : null;
                $product_variation_image_atts = $variation->getProductVariationImageAttribute($variation);
                $product_variation_color = isset($variation) && $variation->exists ? ( $variation->product_variation_color_path != '' ? asset($variation->product_variation_color_path) : null ) : null;
                $product_variation_color_atts = $variation->getProductVariationColorAttribute($variation);
                $formattedColorStyles[] = [
                    'label' => $variation->color->name,
                    'value' => $variation->color->id,
                    'hex'   => $variation->color->value,
                    'id'    => $variation->id,
					'productvariation_image_name'     => '',
					'productvariation_image_old_name' => isset($product_variation_image_atts->filename) ? $product_variation_image_atts->filename : null,
					'productvariation_image_id'       => isset($product_variation_image_atts->id) ? $product_variation_image_atts->id : null,
					'productvariation_image_url'      => $product_variation_image,
					'productvariation_color_name'     => '',
					'productvariation_color_old_name' => isset($product_variation_color_atts->filename) ? $product_variation_color_atts->filename: null,
					'productvariation_color_id'       => isset($product_variation_color_atts->id) ? $product_variation_color_atts->id : null,
					'productvariation_color_url'      => $product_variation_color,
                ];
            }
            $formattedStyles[] = [
                'id'         => $item->id,
                'count'      => $item->order,
                'style_name' => $item->name,
                'color_ids'  => $formattedColorStyles,
                'colorPanel' => $formattedColorStyles,
            ];
        });
        unset($product['styles']);
        $product['styles']     = $formattedStyles;
        $product['deletedRow'] = [];
        $product['has_styles'] = ($product->has_styles == 1) ? TRUE : FALSE;

        $allsizes = array_values(array_sort($allsizes, function($val) {
            return $val['order'];
        }));

        if ($product) {
            return response()->json(['success' => true, 'object' => $product, 'colors' => $allcolors, 'sizes' => $allsizes]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product, Request $request) {
        try {
            $sizeID = $request->size_ids;
            $colorID = $request->color_ids;
            $request['size_ids'] = array_pluck($sizeID, 'value');
            $request['color_ids'] = array_pluck($colorID, 'value');
            $product = $this->productRepo->update($product, $request);
            if ( ! $product->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($product->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Product Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {

            $productName = $this->productRepo->delete($request);

            $product = Product::all();

            return response()->json(['success' => true, 'deleted' => $productName , 'object' => $product ]);

        } catch (ValidationException $e) {

            return $this->redirectFormRequest($e);

        }
    }

}
