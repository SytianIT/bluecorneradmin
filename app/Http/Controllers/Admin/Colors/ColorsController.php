<?php

namespace App\Http\Controllers\Admin\Colors;

use App\Http\Controllers\Controller;

use BC\Repositories\Color\Color;
use BC\Repositories\Color\ColorRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class ColorsController extends Controller
{

    public function __construct(ColorRepository $colorsRepo) {
        $this->colorsRepo = $colorsRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $colors = Color::all();

        return response()->json(['success' => true, 'object' => $colors]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Color $colors)
    {
        try {
            $colors = $this->colorsRepo->store($colors, $request);

            if ( ! $colors->save() ) {
                return response()->json(['message' => $request->name.' color did not save successfully!', 'success' => FALSE]);
            }
            return response()->json(['message' => $request->name.'  color Added Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Color $colors)
    {
        return response()->json(['success' => true, 'object' => $colors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Color $colors, Request $request)
    {
        try {
            $colors = $this->colorsRepo->update($colors, $request);

            if (!$colors->save()) {
				return response()->json(['message' => $colors->name.' color did not save succesfully!', 'success' => FALSE]);
			}

            return response()->json(['message' => $colors->name.' Update Successfully!', 'success' => TRUE]);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Color $colors)
    {
        $this->colorsRepo->delete($colors);
        $allColors = Color::all();

        return response()->json(['message' => $colors->name.' Successfully deleted!', 'success' => TRUE, 'object'=> $allColors ]);
    }
}
