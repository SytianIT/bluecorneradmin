<?php

namespace App\Http\Controllers\Admin\ProductCategories;

use App\Http\Controllers\Controller;
use BC\Repositories\ProductCategory\ProductCategory;
use BC\Repositories\ProductCategory\ProductCategoryRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;

class ProductCategoriesController extends Controller
{

    public function __construct(ProductCategoryRepository $productCategoryRepo)
    {
        $this->productCategoryRepo = $productCategoryRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ProductCategory::all();
        if ($categories) {
            $arrayImage = [];
            foreach ( $categories as $key => $args ):
                $draw = [
                    'id' => $args->id,
                    'thumbnail' => $args->img_logo_path,
                    'name' => $args->name,
                    'parent_id' => $args->parent_id,
                    'description' => $args->description
                ];
                $arrayImage[] = $draw;
            endforeach;
            return response()->json(['success' => true, 'object' => $arrayImage ]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, ProductCategory $productCategory)
    {
		$prodCategory = new ProductCategory();
        //$categories = $prodCategory->get()->toTree();
		$categories = $prodCategory->getNestedList();
        if ( $categories ){
            return response()->json(['success' => true, 'object' => $categories]);
        }
    }

    /**
     * Show the form for storing a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ProductCategory $productCategory)
    {
        try {
            $product = $this->productCategoryRepo->store($productCategory, $request);
            if ( !$product->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($product->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Product Category Successfully added']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProductCategory $productCategory, $category_id)
    {
        $productCategory = ProductCategory::find($category_id);
        $AllProductCategories = $productCategory->getNestedList();
        $productCategorySub = $productCategory->parent_id; // palitan nalang ng parent_id from $productCategory

        $category_mobile_thumb = isset($productCategory) && $productCategory->exists ? asset($productCategory->img_logo_path) : asset(\BC\Repositories\Uploadable\Uploadable::IMG_PLACEHOLDER);
        $category_mobile_thumb_atts = $productCategory->getImgLogoPathAttribute($productCategory);

        $format_category_mobile_thumb[] = [
            'name'     => '',
            'old_name' => isset($category_mobile_thumb_atts->filename) ? $category_mobile_thumb_atts->filename: null,
            'id'       => isset($category_mobile_thumb_atts->id) ? $category_mobile_thumb_atts->id: null,
            'src'      => $category_mobile_thumb,
        ];
        $productCategory['thumb_mobile'] = $format_category_mobile_thumb;

        if ($productCategory) {
            return response()->json([
                'success' => true,
                'object' => $productCategory,
                'all_categories' => $AllProductCategories,
                'get_siblings' => $productCategorySub,
            ]);
        }
        return response()->json(['success' => false, 'object' => false]);
    }

    /**
     * Update the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        try {
            $product = $this->productCategoryRepo->update($request);
            if ( ! $product->save() ) {
                return redirect()->back()
                            ->withInput()
                            ->withErrors($product->getErrors())
                            ->withError("Please check required fields.");
            }
            return response()->json(['success' => true, 'object' => 'Product Category Successfully Updated']);
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $category_id)
    {
        try {
            $productCategoryName = $this->productCategoryRepo->delete($request, $category_id);

            $ProductCategory = ProductCategory::all();
            if ($ProductCategory) {
                $arrayImage = [];
                foreach ( $ProductCategory as $key => $args ):
                    $draw = [
                        'id' => $args->id,
                        'thumbnail' => $args->img_logo_path,
                        'name' => $args->name,
                        'parent_id' => $args->parent_id,
                        'description' => $args->description
                    ];
                    $arrayImage[] = $draw;
                endforeach;
                return response()->json(['success' => true, 'deleted' => $productCategoryName , 'object' => $arrayImage ]);
            }
        } catch (ValidationException $e) {
            return $this->redirectFormRequest($e);
        }
    }
}
