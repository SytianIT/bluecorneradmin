<?php

namespace App\Console\Commands;

use BC\Repositories\Contest\Contest;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SetContestActive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'active:contest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Contest status Successfully Updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$now = Carbon::now();
		$onqueues = Contest::onqueue()
			->where('date_from', '>=', $now->toDateString())
			->where('date_to', '<=', $now->toDateString())
			->get();
        if( $onqueues ) {
            foreach($onqueues as $contest)
            {
				$contest->status = 'active';
				$contest->save();
            }
        }
        $this->info('Contest Cron Successfully Running');
    }
}
