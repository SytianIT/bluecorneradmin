<?php

namespace App\Console\Commands;

use BC\Repositories\Items\Item;
use BC\Repositories\Contest\Contest;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SetContestExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:contest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Contest status Successfully Updated';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // 
        $contests = Contest::where('date_to', '<=', Carbon::now())->get();
        if ($contests) {
            foreach($contests as $contest)
            {
                //Update each contest to expired
                $contest->status = 'expired';
                $contest->save();
            }
        }
        $this->info('Contest Cron Successfully Running');
    }
}
