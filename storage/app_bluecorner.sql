-- phpMyAdmin SQL Dump
-- version 4.6.6deb1+deb.cihar.com~trusty.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 09, 2019 at 11:09 AM
-- Server version: 5.5.57-MariaDB-1ubuntu0.14.04.1
-- PHP Version: 7.1.20-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app_bluecorner`
--

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE `audits` (
  `id` int(10) UNSIGNED NOT NULL,
  `auditable_id` int(11) DEFAULT NULL,
  `auditable_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `other_data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `auditable_id`, `auditable_type`, `action`, `description`, `user_id`, `other_data`, `created_at`, `updated_at`) VALUES
(1, 1, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Round Neck\'', 1, NULL, '2019-03-07 02:13:26', '2019-03-07 02:13:26'),
(2, 1, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'XS\'', 1, NULL, '2019-03-07 02:13:47', '2019-03-07 02:13:47'),
(3, 2, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'SMALL\'', 1, NULL, '2019-03-07 02:13:55', '2019-03-07 02:13:55'),
(4, 3, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'MEDIUM\'', 1, NULL, '2019-03-07 02:14:08', '2019-03-07 02:14:08'),
(5, 4, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'LARGE\'', 1, NULL, '2019-03-07 02:14:24', '2019-03-07 02:14:24'),
(6, 4, 'BC\\Repositories\\Sizes\\Sizes', 'update', 'Sizes \'LARGE\'', 1, NULL, '2019-03-07 02:14:44', '2019-03-07 02:14:44'),
(7, 5, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'XL\'', 1, NULL, '2019-03-07 02:15:14', '2019-03-07 02:15:14'),
(8, 6, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'XXL\'', 1, NULL, '2019-03-07 02:15:28', '2019-03-07 02:15:28'),
(9, 7, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'3XL\'', 1, NULL, '2019-03-07 02:15:41', '2019-03-07 02:15:41'),
(10, 8, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'4XL\'', 1, NULL, '2019-03-07 02:15:50', '2019-03-07 02:15:50'),
(11, 9, 'BC\\Repositories\\Sizes\\Sizes', 'create', 'New Sizes \'5XL\'', 1, NULL, '2019-03-07 02:15:57', '2019-03-07 02:15:57'),
(12, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:16:19', '2019-03-07 02:16:19'),
(13, 1, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'AQUA BLUE\'', 1, NULL, '2019-03-07 02:18:55', '2019-03-07 02:18:55'),
(14, 2, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'AZALEA\'', 1, NULL, '2019-03-07 02:19:24', '2019-03-07 02:19:24'),
(15, 3, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'BLACK\'', 1, NULL, '2019-03-07 02:19:40', '2019-03-07 02:19:40'),
(16, 4, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'CHESTNUT\'', 1, NULL, '2019-03-07 02:19:56', '2019-03-07 02:19:56'),
(17, 5, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'CHOCO BROWN\'', 1, NULL, '2019-03-07 02:20:35', '2019-03-07 02:20:35'),
(18, 6, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'COLUMBIA BLUE\'', 1, NULL, '2019-03-07 02:22:18', '2019-03-07 02:22:18'),
(19, 7, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'CORAL\'', 1, NULL, '2019-03-07 02:22:38', '2019-03-07 02:22:38'),
(20, 8, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'CREAM\'', 1, NULL, '2019-03-07 02:23:41', '2019-03-07 02:23:41'),
(21, 9, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'FOREST GREEN\'', 1, NULL, '2019-03-07 02:24:02', '2019-03-07 02:24:02'),
(22, 10, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'FRESHMEN GREEN\'', 1, NULL, '2019-03-07 02:24:23', '2019-03-07 02:24:23'),
(23, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:26:25', '2019-03-07 02:26:25'),
(24, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:27:12', '2019-03-07 02:27:12'),
(25, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:32:35', '2019-03-07 02:32:35'),
(26, 1, 'BC\\Repositories\\Settings\\Setting', 'update', 'Setting \'Blue Corner\'', 1, NULL, '2019-03-07 02:34:43', '2019-03-07 02:34:43'),
(27, 1, 'BC\\Repositories\\Settings\\Setting', 'update', 'Setting \'Blue Corner\'', 1, NULL, '2019-03-07 02:35:36', '2019-03-07 02:35:36'),
(28, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:39:21', '2019-03-07 02:39:21'),
(29, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:48:18', '2019-03-07 02:48:18'),
(30, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:49:06', '2019-03-07 02:49:06'),
(31, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:53:29', '2019-03-07 02:53:29'),
(32, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:53:50', '2019-03-07 02:53:50'),
(33, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:54:09', '2019-03-07 02:54:09'),
(34, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:55:08', '2019-03-07 02:55:08'),
(35, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(36, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 03:18:33', '2019-03-07 03:18:33'),
(37, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 03:42:37', '2019-03-07 03:42:37'),
(38, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 03:42:46', '2019-03-07 03:42:46'),
(39, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 03:51:22', '2019-03-07 03:51:22'),
(40, 2, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'V-Neck\'', 1, NULL, '2019-03-07 04:16:10', '2019-03-07 04:16:10'),
(41, 2, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck\'', 1, NULL, '2019-03-07 04:17:15', '2019-03-07 04:17:15'),
(42, 2, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck\'', 1, NULL, '2019-03-07 04:21:39', '2019-03-07 04:21:39'),
(43, 2, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck\'', 1, NULL, '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(44, 11, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'LIME GREEN\'', 1, NULL, '2019-03-07 04:24:37', '2019-03-07 04:24:37'),
(45, 12, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'MINT GREEN\'', 1, NULL, '2019-03-07 04:24:53', '2019-03-07 04:24:53'),
(46, 13, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'PINK\'', 1, NULL, '2019-03-07 04:25:17', '2019-03-07 04:25:17'),
(47, 14, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'PURPLE\'', 1, NULL, '2019-03-07 04:25:57', '2019-03-07 04:25:57'),
(48, 15, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'TANGERINE\'', 1, NULL, '2019-03-07 04:26:25', '2019-03-07 04:26:25'),
(49, 2, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck\'', 1, NULL, '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(50, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 17:11:12', '2019-03-07 17:11:12'),
(51, 3, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'V-Neck Comfort Wear\'', 1, NULL, '2019-03-07 18:55:55', '2019-03-07 18:55:55'),
(52, 16, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'HEADER WHITE\'', 1, NULL, '2019-03-07 18:57:51', '2019-03-07 18:57:51'),
(53, 17, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'LEMON CHROME\'', 1, NULL, '2019-03-07 18:58:12', '2019-03-07 18:58:12'),
(54, 18, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'SWEETCORN\'', 1, NULL, '2019-03-07 18:58:29', '2019-03-07 18:58:29'),
(55, 19, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'NAVY BLUE\'', 1, NULL, '2019-03-07 18:58:48', '2019-03-07 18:58:48'),
(56, 20, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'RED\'', 1, NULL, '2019-03-07 18:59:04', '2019-03-07 18:59:04'),
(57, 21, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'RED WINE\'', 1, NULL, '2019-03-07 18:59:33', '2019-03-07 18:59:33'),
(58, 22, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'ROYAL BLUE\'', 1, NULL, '2019-03-07 18:59:56', '2019-03-07 18:59:56'),
(59, 23, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'VIOLET\'', 1, NULL, '2019-03-07 19:00:20', '2019-03-07 19:00:20'),
(60, 24, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'WHITE\'', 1, NULL, '2019-03-07 19:00:43', '2019-03-07 19:00:43'),
(61, 25, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'LIGHT BLUE\'', 1, NULL, '2019-03-07 19:01:09', '2019-03-07 19:01:09'),
(62, 3, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck Comfort Wear\'', 1, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(63, 3, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck Comfort Wear\'', 1, NULL, '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(64, 4, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Fashion Polo (White Stripes)\'', 1, NULL, '2019-03-07 19:28:40', '2019-03-07 19:28:40'),
(65, 4, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo (White Stripes)\'', 1, NULL, '2019-03-07 19:31:52', '2019-03-07 19:31:52'),
(66, 4, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo (White Stripes)\'', 1, NULL, '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(67, 1, 'BC\\Repositories\\Settings\\Setting', 'update', 'Setting \'Blue Corner\'', 1, NULL, '2019-03-07 19:44:21', '2019-03-07 19:44:21'),
(68, 1, 'BC\\Repositories\\Contest\\Contest', 'create', 'New Contest \'My Contest\'', 1, NULL, '2019-03-07 19:45:35', '2019-03-07 19:45:35'),
(69, 4, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo (White Stripes)\'', 1, NULL, '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(70, 5, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Single Jersey\'', 1, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(71, 5, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Single Jersey\'', 1, NULL, '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(72, 1, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-07 20:57:31', '2019-03-07 20:57:31'),
(73, 6, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Round Neck Shirt\'', 1, NULL, '2019-03-07 21:05:08', '2019-03-07 21:05:08'),
(74, 6, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck Shirt\'', 1, NULL, '2019-03-07 21:14:28', '2019-03-07 21:14:28'),
(75, 26, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'BLUE JEWEL\'', 1, NULL, '2019-03-07 21:15:09', '2019-03-07 21:15:09'),
(76, 27, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'ORANGE\'', 1, NULL, '2019-03-07 21:15:41', '2019-03-07 21:15:41'),
(77, 28, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'YELLOW GOLD\'', 1, NULL, '2019-03-07 21:16:30', '2019-03-07 21:16:30'),
(78, 6, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck Shirt\'', 1, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(79, 7, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'V-Neck Shirt\'', 1, NULL, '2019-03-07 21:28:10', '2019-03-07 21:28:10'),
(80, 6, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck Shirt\'', 1, NULL, '2019-03-07 21:30:18', '2019-03-07 21:30:18'),
(81, 7, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'V-Neck Shirt\'', 1, NULL, '2019-03-07 21:32:37', '2019-03-07 21:32:37'),
(82, 8, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Polo Shirt\'', 1, NULL, '2019-03-07 21:38:48', '2019-03-07 21:38:48'),
(83, 29, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'ALPHINE\'', 1, NULL, '2019-03-07 22:08:03', '2019-03-07 22:08:03'),
(84, 30, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'APPLE GREEN\'', 1, NULL, '2019-03-07 22:08:36', '2019-03-07 22:08:36'),
(85, 8, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Polo Shirt\'', 1, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(86, 9, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 22:15:41', '2019-03-07 22:15:41'),
(87, 9, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 22:17:01', '2019-03-07 22:17:01'),
(88, 9, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(89, 9, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 22:26:05', '2019-03-07 22:26:05'),
(90, 10, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Round Neck Shirt\'', 1, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(91, 11, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'V-Neck Shirt\'', 1, NULL, '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(92, 12, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Polo Shirt\'', 1, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(93, 13, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(94, 13, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Fashion Polo Shirt\'', 1, NULL, '2019-03-07 23:03:02', '2019-03-07 23:03:02'),
(95, 14, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Sando Kids 6 in 1 White (Boys)\'', 1, NULL, '2019-03-07 23:19:04', '2019-03-07 23:19:04'),
(96, 1, 'BC\\Repositories\\Contest\\Contest', 'update', 'Contest \'My Contest\'', 1, NULL, '2019-03-08 02:08:46', '2019-03-08 02:08:46'),
(97, 15, 'BC\\Repositories\\Product\\Product', 'create', 'New Product \'Round Neck\'', 1, NULL, '2019-03-08 02:22:01', '2019-03-08 02:22:01'),
(98, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 02:22:14', '2019-03-08 02:22:14'),
(99, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 02:26:25', '2019-03-08 02:26:25'),
(100, 31, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'Maroon\'', 1, NULL, '2019-03-08 02:28:39', '2019-03-08 02:28:39'),
(101, 31, 'BC\\Repositories\\Color\\Color', 'update', 'Color \'Maroon\'', 1, NULL, '2019-03-08 02:28:46', '2019-03-08 02:28:46'),
(102, 31, 'BC\\Repositories\\Color\\Color', 'update', 'Color \'MAROON\'', 1, NULL, '2019-03-08 02:28:56', '2019-03-08 02:28:56'),
(103, 32, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'MOSS GREEN\'', 1, NULL, '2019-03-08 02:31:31', '2019-03-08 02:31:31'),
(104, 33, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'MUROISE\'', 1, NULL, '2019-03-08 02:32:16', '2019-03-08 02:32:16'),
(105, 34, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'NAVY BLUE\'', 1, NULL, '2019-03-08 02:32:32', '2019-03-08 02:32:32'),
(106, 35, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'AVOCADO\'', 1, NULL, '2019-03-08 02:32:46', '2019-03-08 02:32:46'),
(107, 34, 'BC\\Repositories\\Color\\Color', 'delete', 'Color \'NAVY BLUE\'', 1, NULL, '2019-03-08 02:32:56', '2019-03-08 02:32:56'),
(108, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 02:33:18', '2019-03-08 02:33:18'),
(109, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(110, 36, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'PEACH\'', 1, NULL, '2019-03-08 02:38:57', '2019-03-08 02:38:57'),
(111, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 02:39:16', '2019-03-08 02:39:16'),
(112, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(113, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 03:22:14', '2019-03-08 03:22:14'),
(114, 37, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'Safari\'', 1, NULL, '2019-03-08 03:23:44', '2019-03-08 03:23:44'),
(115, 37, 'BC\\Repositories\\Color\\Color', 'update', 'Color \'SAFARI\'', 1, NULL, '2019-03-08 03:23:50', '2019-03-08 03:23:50'),
(116, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 03:24:16', '2019-03-08 03:24:16'),
(117, 38, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'SKY BLUE\'', 1, NULL, '2019-03-08 03:24:50', '2019-03-08 03:24:50'),
(118, 39, 'BC\\Repositories\\Color\\Color', 'create', 'New Color \'Sunkist\'', 1, NULL, '2019-03-08 03:26:07', '2019-03-08 03:26:07'),
(119, 39, 'BC\\Repositories\\Color\\Color', 'update', 'Color \'SUNKIST\'', 1, NULL, '2019-03-08 03:26:18', '2019-03-08 03:26:18'),
(120, 15, 'BC\\Repositories\\Product\\Product', 'update', 'Product \'Round Neck\'', 1, NULL, '2019-03-08 03:27:12', '2019-03-08 03:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `value`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'AQUA BLUE', '#007DD9', 1, NULL, '2019-03-07 02:18:55', '2019-03-07 02:18:55'),
(2, 'AZALEA', '#fec7e6', 2, NULL, '2019-03-07 02:19:24', '2019-03-07 02:19:24'),
(3, 'BLACK', '#000', 3, NULL, '2019-03-07 02:19:40', '2019-03-07 02:19:40'),
(4, 'CHESTNUT', '#9b8879', 4, NULL, '2019-03-07 02:19:56', '2019-03-07 02:19:56'),
(5, 'CHOCO BROWN', '#825538', 5, NULL, '2019-03-07 02:20:35', '2019-03-07 02:20:35'),
(6, 'COLUMBIA BLUE', '#d1ebf8', 6, NULL, '2019-03-07 02:22:18', '2019-03-07 02:22:18'),
(7, 'CORAL', '#ff524c', 7, NULL, '2019-03-07 02:22:38', '2019-03-07 02:22:38'),
(8, 'CREAM', '#fcf2e6', 8, NULL, '2019-03-07 02:23:41', '2019-03-07 02:23:41'),
(9, 'FOREST GREEN', '#01be88', 9, NULL, '2019-03-07 02:24:02', '2019-03-07 02:24:02'),
(10, 'FRESHMEN GREEN', '#00ae6f', 10, NULL, '2019-03-07 02:24:22', '2019-03-07 02:24:22'),
(11, 'LIME GREEN', '#caf999', 11, NULL, '2019-03-07 04:24:37', '2019-03-07 04:24:37'),
(12, 'MINT GREEN', '#d8efe5', 12, NULL, '2019-03-07 04:24:53', '2019-03-07 04:24:53'),
(13, 'PINK', '#fef1f8', 13, NULL, '2019-03-07 04:25:17', '2019-03-07 04:25:17'),
(14, 'PURPLE', '#553fa4', 14, NULL, '2019-03-07 04:25:57', '2019-03-07 04:25:57'),
(15, 'TANGERINE', '#ff7800', 15, NULL, '2019-03-07 04:26:25', '2019-03-07 04:26:25'),
(16, 'HEADER WHITE', '#dbdbdd', 16, NULL, '2019-03-07 18:57:51', '2019-03-07 18:57:51'),
(17, 'LEMON CHROME', '#faec57', 17, NULL, '2019-03-07 18:58:12', '2019-03-07 18:58:12'),
(18, 'SWEETCORN', '#f9e6ac', 18, NULL, '2019-03-07 18:58:29', '2019-03-07 18:58:29'),
(19, 'NAVY BLUE', '#141f31', 19, NULL, '2019-03-07 18:58:48', '2019-03-07 18:58:48'),
(20, 'RED', '#b31e22', 20, NULL, '2019-03-07 18:59:04', '2019-03-07 18:59:04'),
(21, 'RED WINE', '#612237', 21, NULL, '2019-03-07 18:59:33', '2019-03-07 18:59:33'),
(22, 'ROYAL BLUE', '#0a3593', 22, NULL, '2019-03-07 18:59:56', '2019-03-07 18:59:56'),
(23, 'VIOLET', '#3f1d59', 23, NULL, '2019-03-07 19:00:20', '2019-03-07 19:00:20'),
(24, 'WHITE', '#FFFFFF', 24, NULL, '2019-03-07 19:00:43', '2019-03-07 19:00:43'),
(25, 'LIGHT BLUE', '#66d4ff', 25, NULL, '2019-03-07 19:01:09', '2019-03-07 19:01:09'),
(26, 'BLUE JEWEL', '#006ac0', 26, NULL, '2019-03-07 21:15:09', '2019-03-07 21:15:09'),
(27, 'ORANGE', '#f87410', 27, NULL, '2019-03-07 21:15:41', '2019-03-07 21:15:41'),
(28, 'YELLOW GOLD', '#ffd300', 28, NULL, '2019-03-07 21:16:30', '2019-03-07 21:16:30'),
(29, 'ALPHINE', '#85abf2', 29, NULL, '2019-03-07 22:08:03', '2019-03-07 22:08:03'),
(30, 'APPLE GREEN', '#c8faa1', 30, NULL, '2019-03-07 22:08:36', '2019-03-07 22:08:36'),
(31, 'MAROON', '#800000', 31, NULL, '2019-03-08 02:28:39', '2019-03-08 02:28:56'),
(32, 'MOSS GREEN', '#23372F', 32, NULL, '2019-03-08 02:31:31', '2019-03-08 02:31:31'),
(33, 'MUROISE', '#FE769A', 33, NULL, '2019-03-08 02:32:16', '2019-03-08 02:32:16'),
(34, 'NAVY BLUE', '#293754', 34, '2019-03-08 02:32:55', '2019-03-08 02:32:32', '2019-03-08 02:32:55'),
(35, 'AVOCADO', '#95A763', 35, NULL, '2019-03-08 02:32:46', '2019-03-08 02:32:46'),
(36, 'PEACH', '#FFA67F', 36, NULL, '2019-03-08 02:38:57', '2019-03-08 02:38:57'),
(37, 'SAFARI', '#5A6554', 37, NULL, '2019-03-08 03:23:44', '2019-03-08 03:23:50'),
(38, 'SKY BLUE', '#A8C6EC', 38, NULL, '2019-03-08 03:24:50', '2019-03-08 03:24:50'),
(39, 'SUNKIST', '#FFAB4B', 39, NULL, '2019-03-08 03:26:07', '2019-03-08 03:26:18');

-- --------------------------------------------------------

--
-- Table structure for table `contests`
--

CREATE TABLE `contests` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contests`
--

INSERT INTO `contests` (`id`, `title`, `slug`, `description`, `date_from`, `date_to`, `status`, `published`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'My Contest', 'my-contest', '<p>Test Contest</p>', '2019-03-08', '2019-03-08', 'active', 1, NULL, '2019-03-07 19:45:35', '2019-03-07 19:45:35');

-- --------------------------------------------------------

--
-- Table structure for table `contest_items`
--

CREATE TABLE `contest_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `type` text COLLATE utf8mb4_unicode_ci,
  `question` text COLLATE utf8mb4_unicode_ci,
  `choices` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contest_items`
--

INSERT INTO `contest_items` (`id`, `contest_id`, `type`, `question`, `choices`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'text', 'Question for paragraph', 'null', 0, NULL, '2019-03-07 19:46:46', '2019-03-07 19:46:46'),
(2, 1, 'text_field', 'Question for text area', 'null', 1, NULL, '2019-03-07 19:46:46', '2019-03-07 19:46:46'),
(3, 1, 'radio', 'Question for radio buttons', '[{\"subitem\":\"radio option 1\",\"subParent\":2,\"radio\":true},{\"subitem\":\"radio option 2\",\"subParent\":2,\"radio\":true}]', 2, NULL, '2019-03-07 19:46:46', '2019-03-07 19:46:46'),
(4, 1, 'checkbox', 'Question for checkbox', '[{\"subitem\":\"checkbox option 1\",\"subParent\":3,\"checkbox\":true},{\"subitem\":\"checkbox option 2\",\"subParent\":3,\"checkbox\":true}]', 3, NULL, '2019-03-07 19:46:46', '2019-03-07 19:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4275, '2014_10_12_000000_create_users_table', 1),
(4276, '2014_10_12_100000_create_password_resets_table', 1),
(4277, '2018_12_13_093447_create_sizes_table', 1),
(4278, '2019_01_03_024605_create_products_table', 1),
(4279, '2019_01_03_070555_create_product_categories_table', 1),
(4280, '2019_01_03_094202_create_colors_table', 1),
(4281, '2019_01_07_031327_create_product_sizes_table', 1),
(4282, '2019_01_07_031730_create_product_variations_table', 1),
(4283, '2019_01_07_031751_create_styles_table', 1),
(4284, '2019_01_07_060800_create_contests_table', 1),
(4285, '2019_01_07_063840_create_roles_table', 1),
(4286, '2019_01_07_064243_create_user_roles_table', 1),
(4287, '2019_01_14_033316_create_contest_items_table', 1),
(4288, '2019_01_17_064054_create_uploadables_table', 1),
(4289, '2019_01_21_025050_create_role_permissions_table', 1),
(4290, '2019_01_21_025119_create_permissions_table', 1),
(4291, '2019_01_23_053205_create_settings_table', 1),
(4292, '2019_03_05_112126_create_audits_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `has_styles` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `name`, `description`, `has_styles`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'Round Neck', '<h3><strong>COTTON JERSEY</strong></h3><p><br></p><ul><li>Seamless rib collar</li><li>Double-needle sleeve and bottom hem</li><li>Adults XS-3XL</li><li>38 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 02:13:26', '2019-03-07 02:13:26'),
(2, 2, 'V-Neck', '<h3>COTTON JERSEY</h3><ul><li>V-Neck rib collar with</li><li>Double-needle sleeve and bottom hem</li><li>Adult XS- 3xl</li><li>24 colors</li></ul>', 0, NULL, '2019-03-07 04:16:10', '2019-03-07 04:16:10'),
(3, 2, 'V-Neck Comfort Wear', '<p><br></p><h3>COTTON JERSEY</h3><ul><li>Seamless rib collar</li><li>Shoulder to shoulder taping</li><li>Double-needle sleeve and bottom hem</li><li>Adult XS- 2xl</li><li>15 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 18:55:55', '2019-03-07 18:55:55'),
(4, 2, 'Fashion Polo (White Stripes)', '<h3>HONEYCOMB</h3><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket.</li><li>Adult XS- 2xl</li><li>5 designs</li><li>8 colors</li></ul>', 1, NULL, '2019-03-07 19:28:40', '2019-03-07 19:31:52'),
(5, 2, 'Single Jersey', '<h3>HONEYCOMB</h3><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket</li><li>Adult XS- 2xl</li><li>36 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(6, 3, 'Round Neck Shirt', '<h3>COTTON JERSEY</h3><ul><li>Feminine construction details</li><li>Seamless rib collar with</li><li>ADouble-needle sleeve and bottom hem</li><li>Adult XS- 4xl</li><li>20 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 21:05:08', '2019-03-07 21:05:08'),
(7, 3, 'V-Neck Shirt', '<h3>COTTON JERSEY</h3><ul><li>Feminine construction details</li><li>V-Neck rib collar</li><li>Double-needle sleeve and bottom hem</li><li>Adult XS- 4xl</li><li>20 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 21:28:10', '2019-03-07 21:28:10'),
(8, 3, 'Polo Shirt', '<h3>(HC / COMFORTWEAR) HONEYCOMB</h3><p><br></p><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket.</li><li>Adult XS- 2xl</li><li>35 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 21:38:48', '2019-03-07 21:38:48'),
(9, 3, 'Fashion Polo Shirt', '<h3>HONEYCOMB</h3><p><br></p><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket.</li><li>Adult XS- 2xl</li><li>5 designs</li><li>8 colors</li></ul><p><br></p>', 1, NULL, '2019-03-07 22:15:41', '2019-03-07 22:15:41'),
(10, 4, 'Round Neck Shirt', '<h3>COTTON JERSEY</h3><p><br></p><ul><li>Seamless rib collar</li><li>Double-needle sleeve and bottom hem</li><li>S-2XL</li><li>23 colors</li></ul>', 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(11, 4, 'V-Neck Shirt', '<h3>COTTON JERSEY</h3><p><br></p><ul><li>V-Neck rib collar</li><li>Double-needle sleeve and bottom hem</li><li>S-2XL</li><li>White only</li></ul><p><br></p>', 0, NULL, '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(12, 4, 'Polo Shirt', '<h3>(HC / COMFORTWEAR) HONEYCOMB</h3><p><br></p><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket.</li><li>Adult S – 2XL</li><li>36 colors</li></ul>', 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(13, 4, 'Fashion Polo Shirt', '<h3>HONEYCOMB</h3><p><br></p><ul><li>1×1 rib flat knitted collar</li><li>Double-needle sleeve and bottom hem</li><li>Necktape. 2-button placket reinforced with fusible interlining.</li><li>Adult XS- 2xl</li><li>5 designs</li><li>8 colors</li></ul><p><br></p>', 0, NULL, '2019-03-07 22:58:01', '2019-03-07 22:58:01'),
(14, 4, 'Sando Kids 6 in 1 White (Boys)', '<h3>COTTON JERSEY</h3><ul><li>Athletic cut</li><li>Ultimate comfort</li><li>Self-jersey trim on neck and armholes with double needle stitching</li><li>Double needle bottom hem</li><li>Size 4 to Size 22</li></ul>', 0, NULL, '2019-03-07 23:19:04', '2019-03-07 23:19:04'),
(15, 5, 'Round Neck', '<h3><strong>COTTON JERSEY</strong></h3><p><br></p><ul><li><span style=\"color: rgb(33, 37, 41);\">Seamless rib collar.</span></li><li><span style=\"color: rgb(33, 37, 41);\">Double-needle sleeve and bottom hem</span></li><li><span style=\"color: rgb(33, 37, 41);\">Adult XS- 3xl</span></li><li><span style=\"color: rgb(33, 37, 41);\">42 colors</span></li></ul>', 0, NULL, '2019-03-08 02:22:01', '2019-03-08 02:26:25');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `description`, `_lft`, `_rgt`, `parent_id`, `order`, `featured`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Blue corner', NULL, 1, 8, NULL, NULL, 1, NULL, '2019-03-07 01:07:46', '2019-03-07 01:07:46'),
(2, 'Mens', NULL, 2, 3, 1, NULL, 0, NULL, '2019-03-07 01:09:09', '2019-03-07 01:09:09'),
(3, 'Womens', NULL, 4, 5, 1, NULL, 0, NULL, '2019-03-07 01:09:24', '2019-03-07 01:09:24'),
(4, 'Kids', NULL, 6, 7, 1, NULL, 0, NULL, '2019-03-07 01:09:32', '2019-03-07 01:09:32'),
(5, 'AIIZ Ultra Cotton', NULL, 9, 10, NULL, NULL, 1, NULL, '2019-03-07 21:48:25', '2019-03-07 21:48:25');

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_sizes`
--

INSERT INTO `product_sizes` (`id`, `product_id`, `size_id`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 8, NULL, NULL, NULL),
(2, 1, 9, NULL, NULL, NULL),
(3, 1, 7, NULL, NULL, NULL),
(4, 1, 6, NULL, NULL, NULL),
(5, 1, 5, NULL, NULL, NULL),
(6, 1, 3, NULL, NULL, NULL),
(7, 1, 2, NULL, NULL, NULL),
(8, 1, 4, NULL, NULL, NULL),
(9, 1, 1, NULL, NULL, NULL),
(10, 2, 1, NULL, NULL, NULL),
(11, 2, 2, NULL, NULL, NULL),
(12, 2, 3, NULL, NULL, NULL),
(13, 2, 4, NULL, NULL, NULL),
(14, 2, 5, NULL, NULL, NULL),
(15, 2, 6, NULL, NULL, NULL),
(16, 2, 7, NULL, NULL, NULL),
(17, 2, 8, NULL, NULL, NULL),
(18, 2, 9, NULL, NULL, NULL),
(19, 3, 1, NULL, NULL, NULL),
(20, 3, 2, NULL, NULL, NULL),
(21, 3, 3, NULL, NULL, NULL),
(22, 3, 4, NULL, NULL, NULL),
(23, 3, 5, NULL, NULL, NULL),
(24, 3, 6, NULL, NULL, NULL),
(25, 3, 7, NULL, NULL, NULL),
(26, 3, 8, NULL, NULL, NULL),
(27, 3, 9, NULL, NULL, NULL),
(28, 4, 1, NULL, NULL, NULL),
(29, 4, 2, NULL, NULL, NULL),
(30, 4, 3, NULL, NULL, NULL),
(31, 4, 4, NULL, NULL, NULL),
(32, 4, 5, NULL, NULL, NULL),
(33, 4, 6, NULL, NULL, NULL),
(34, 4, 7, NULL, NULL, NULL),
(35, 4, 8, NULL, NULL, NULL),
(36, 4, 9, NULL, NULL, NULL),
(37, 5, 8, NULL, NULL, NULL),
(38, 5, 7, NULL, NULL, NULL),
(39, 5, 6, NULL, NULL, NULL),
(40, 5, 9, NULL, NULL, NULL),
(41, 5, 5, NULL, NULL, NULL),
(42, 5, 3, NULL, NULL, NULL),
(43, 5, 2, NULL, NULL, NULL),
(44, 5, 1, NULL, NULL, NULL),
(45, 5, 4, NULL, NULL, NULL),
(46, 6, 1, NULL, NULL, NULL),
(47, 6, 2, NULL, NULL, NULL),
(48, 6, 3, NULL, NULL, NULL),
(49, 6, 4, NULL, NULL, NULL),
(50, 6, 5, NULL, NULL, NULL),
(51, 6, 6, NULL, NULL, NULL),
(52, 6, 7, NULL, NULL, NULL),
(53, 6, 8, NULL, NULL, NULL),
(54, 6, 9, NULL, NULL, NULL),
(55, 7, 1, NULL, NULL, NULL),
(56, 7, 2, NULL, NULL, NULL),
(57, 7, 3, NULL, NULL, NULL),
(58, 7, 4, NULL, NULL, NULL),
(59, 7, 5, NULL, NULL, NULL),
(60, 7, 6, NULL, NULL, NULL),
(61, 7, 7, NULL, NULL, NULL),
(62, 7, 8, NULL, NULL, NULL),
(63, 7, 9, NULL, NULL, NULL),
(64, 8, 1, NULL, NULL, NULL),
(65, 8, 5, NULL, NULL, NULL),
(66, 9, 1, NULL, NULL, NULL),
(67, 9, 5, NULL, NULL, NULL),
(68, 10, 2, NULL, NULL, NULL),
(69, 10, 5, NULL, NULL, NULL),
(70, 11, 2, NULL, NULL, NULL),
(71, 11, 5, NULL, NULL, NULL),
(72, 12, 2, NULL, NULL, NULL),
(73, 12, 5, NULL, NULL, NULL),
(74, 13, 1, NULL, NULL, NULL),
(75, 13, 5, NULL, NULL, NULL),
(76, 14, 1, NULL, NULL, NULL),
(77, 14, 2, NULL, NULL, NULL),
(78, 15, 1, NULL, NULL, NULL),
(79, 15, 2, NULL, NULL, NULL),
(80, 15, 3, NULL, NULL, NULL),
(81, 15, 4, NULL, NULL, NULL),
(82, 15, 5, NULL, NULL, NULL),
(83, 15, 6, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` int(10) UNSIGNED NOT NULL,
  `style_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variations`
--

INSERT INTO `product_variations` (`id`, `style_id`, `product_id`, `color_id`, `active`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 8, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(2, NULL, 1, 7, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(3, NULL, 1, 9, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(4, NULL, 1, 6, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(5, NULL, 1, 5, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(6, NULL, 1, 10, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(7, NULL, 1, 4, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(8, NULL, 1, 3, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(9, NULL, 1, 2, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(10, NULL, 1, 1, 0, '2019-03-07 02:55:08', '2019-03-07 02:26:25', '2019-03-07 02:55:08'),
(11, NULL, 1, 7, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(12, NULL, 1, 1, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(13, NULL, 1, 2, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(14, NULL, 1, 4, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(15, NULL, 1, 3, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(16, NULL, 1, 5, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(17, NULL, 1, 6, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(18, NULL, 1, 9, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(19, NULL, 1, 8, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(20, NULL, 1, 10, 0, NULL, '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(21, NULL, 2, 1, 0, NULL, '2019-03-07 04:16:11', '2019-03-07 04:16:11'),
(22, NULL, 2, 2, 0, NULL, '2019-03-07 04:16:11', '2019-03-07 04:16:11'),
(23, NULL, 2, 5, 0, NULL, '2019-03-07 04:16:11', '2019-03-07 04:16:11'),
(24, NULL, 2, 9, 0, '2019-03-07 04:24:13', '2019-03-07 04:16:11', '2019-03-07 04:24:13'),
(25, NULL, 2, 3, 0, NULL, '2019-03-07 04:16:11', '2019-03-07 04:16:11'),
(26, NULL, 2, 10, 0, NULL, '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(27, NULL, 2, 11, 0, NULL, '2019-03-07 04:28:59', '2019-03-07 04:28:59'),
(28, NULL, 2, 12, 0, NULL, '2019-03-07 04:28:59', '2019-03-07 04:28:59'),
(29, NULL, 2, 13, 0, NULL, '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(30, NULL, 2, 14, 0, NULL, '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(31, NULL, 2, 15, 0, NULL, '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(32, NULL, 3, 3, 0, NULL, '2019-03-07 18:55:56', '2019-03-07 18:55:56'),
(33, NULL, 3, 10, 0, NULL, '2019-03-07 18:55:56', '2019-03-07 18:55:56'),
(34, NULL, 3, 1, 0, '2019-03-07 19:13:32', '2019-03-07 18:55:56', '2019-03-07 19:13:32'),
(35, NULL, 3, 16, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(36, NULL, 3, 18, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(37, NULL, 3, 19, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(38, NULL, 3, 20, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(39, NULL, 3, 21, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(40, NULL, 3, 22, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(41, NULL, 3, 24, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(42, NULL, 3, 23, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(43, NULL, 3, 25, 0, NULL, '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(44, 1, 4, 16, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(45, 1, 4, 11, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(46, 1, 4, 19, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(47, 1, 4, 20, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(48, 1, 4, 24, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(49, 2, 4, 3, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(50, 2, 4, 24, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(51, 2, 4, 10, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(52, 2, 4, 17, 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(53, 2, 4, 20, 0, NULL, '2019-03-07 19:31:52', '2019-03-07 19:31:52'),
(54, NULL, 5, 3, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(55, NULL, 5, 25, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(56, NULL, 5, 11, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(57, NULL, 5, 19, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(58, NULL, 5, 20, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(59, NULL, 5, 22, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(60, NULL, 5, 24, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(61, NULL, 5, 13, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(62, NULL, 5, 14, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(63, NULL, 5, 23, 0, NULL, '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(64, NULL, 6, 3, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(65, NULL, 6, 26, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(66, NULL, 6, 10, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(67, NULL, 6, 16, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(68, NULL, 6, 25, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(69, NULL, 6, 22, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(70, NULL, 6, 21, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(71, NULL, 6, 28, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(72, NULL, 6, 11, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(73, NULL, 6, 20, 0, NULL, '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(74, NULL, 7, 1, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(75, NULL, 7, 2, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(76, NULL, 7, 3, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(77, NULL, 7, 10, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(78, NULL, 7, 11, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(79, NULL, 7, 12, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(80, NULL, 7, 28, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(81, NULL, 7, 20, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(82, NULL, 7, 21, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(83, NULL, 7, 22, 0, NULL, '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(84, NULL, 8, 29, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(85, NULL, 8, 30, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(86, NULL, 8, 2, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(87, NULL, 8, 3, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(88, NULL, 8, 6, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(89, NULL, 8, 8, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(90, NULL, 8, 10, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(91, NULL, 8, 16, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(92, NULL, 8, 20, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(93, NULL, 8, 21, 0, NULL, '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(94, 3, 9, 3, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(95, 3, 9, 26, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(96, 3, 9, 5, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(97, 3, 9, 16, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(98, 3, 9, 11, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(99, 4, 9, 2, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(100, 4, 9, 3, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(101, 4, 9, 10, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(102, 4, 9, 17, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(103, 4, 9, 20, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(104, 5, 9, 29, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(105, 5, 9, 3, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(106, 5, 9, 22, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(107, 5, 9, 23, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(108, 5, 9, 24, 0, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(109, NULL, 10, 1, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(110, NULL, 10, 2, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(111, NULL, 10, 3, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(112, NULL, 10, 5, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(113, NULL, 10, 10, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(114, NULL, 10, 20, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(115, NULL, 10, 21, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(116, NULL, 10, 24, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(117, NULL, 10, 28, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(118, NULL, 10, 11, 0, NULL, '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(119, NULL, 11, 24, 0, NULL, '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(120, NULL, 12, 2, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(121, NULL, 12, 26, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(122, NULL, 12, 6, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(123, NULL, 12, 17, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(124, NULL, 12, 11, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(125, NULL, 12, 19, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(126, NULL, 12, 27, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(127, NULL, 12, 13, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(128, NULL, 12, 21, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(129, NULL, 12, 22, 0, NULL, '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(130, NULL, 13, 24, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(131, NULL, 13, 23, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(132, NULL, 13, 20, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(133, NULL, 13, 11, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(134, NULL, 13, 22, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(135, NULL, 13, 29, 0, NULL, '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(136, NULL, 14, 24, 0, NULL, '2019-03-07 23:19:05', '2019-03-07 23:19:05'),
(137, NULL, 15, 31, 0, NULL, '2019-03-08 02:33:18', '2019-03-08 02:33:18'),
(138, NULL, 15, 32, 0, NULL, '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(139, NULL, 15, 33, 0, NULL, '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(140, NULL, 15, 19, 0, NULL, '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(141, NULL, 15, 35, 0, NULL, '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(142, NULL, 15, 36, 0, NULL, '2019-03-08 02:39:16', '2019-03-08 02:39:16'),
(143, NULL, 15, 13, 0, NULL, '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(144, NULL, 15, 20, 0, NULL, '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(145, NULL, 15, 22, 0, NULL, '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(146, NULL, 15, 37, 0, NULL, '2019-03-08 03:24:16', '2019-03-08 03:24:16'),
(147, NULL, 15, 39, 0, NULL, '2019-03-08 03:27:11', '2019-03-08 03:27:11'),
(148, NULL, 15, 18, 0, NULL, '2019-03-08 03:27:11', '2019-03-08 03:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_contact_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homepage_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homepage_sub_title` longtext COLLATE utf8mb4_unicode_ci,
  `product_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_sub_title` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `site_email`, `site_contact_no`, `homepage_title`, `homepage_sub_title`, `product_title`, `product_sub_title`, `created_at`, `updated_at`) VALUES
(1, 'Blue Corner', 'bluecorner@email.com', NULL, 'Blue Corner', '<p>Test</p>', NULL, NULL, '2019-03-07 02:34:43', '2019-03-07 02:35:36');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `description`, `width`, `length`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'XS', NULL, '17', '26', '1', NULL, '2019-03-07 02:13:47', '2019-03-07 02:13:47'),
(2, 'SMALL', NULL, '18', '27', '2', NULL, '2019-03-07 02:13:55', '2019-03-07 02:13:55'),
(3, 'MEDIUM', NULL, '19', '28', '3', NULL, '2019-03-07 02:14:08', '2019-03-07 02:14:08'),
(4, 'LARGE', NULL, '20 1/2', '29', '4', NULL, '2019-03-07 02:14:24', '2019-03-07 02:14:44'),
(5, 'XL', NULL, '21 1/2', '29 1/2', '5', NULL, '2019-03-07 02:15:14', '2019-03-07 02:15:14'),
(6, 'XXL', NULL, '23', '30', '6', NULL, '2019-03-07 02:15:28', '2019-03-07 02:15:28'),
(7, '3XL', NULL, '23', '31', '7', NULL, '2019-03-07 02:15:41', '2019-03-07 02:15:41'),
(8, '4XL', NULL, '24', '31', '8', NULL, '2019-03-07 02:15:50', '2019-03-07 02:15:50'),
(9, '5XL', NULL, '25', '32', '9', NULL, '2019-03-07 02:15:57', '2019-03-07 02:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

CREATE TABLE `styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `product_id`, `name`, `order`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 4, 'STYLE NO. 1', 0, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(2, 4, 'STYLE NO. 2', 1, NULL, '2019-03-07 19:31:51', '2019-03-07 19:31:51'),
(3, 9, 'STYLE NO. 1', 0, NULL, '2019-03-07 22:15:41', '2019-03-07 22:15:41'),
(4, 9, 'STYLE NO. 2', 1, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15'),
(5, 9, 'STYLE NO. 3', 2, NULL, '2019-03-07 22:20:15', '2019-03-07 22:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `uploadables`
--

CREATE TABLE `uploadables` (
  `id` int(10) UNSIGNED NOT NULL,
  `uploadable_id` int(11) DEFAULT NULL,
  `uploadable_type` text COLLATE utf8mb4_unicode_ci,
  `key` text COLLATE utf8mb4_unicode_ci,
  `filename` text COLLATE utf8mb4_unicode_ci,
  `path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploadables`
--

INSERT INTO `uploadables` (`id`, `uploadable_id`, `uploadable_type`, `key`, `filename`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, 'BC\\Repositories\\ProductCategory\\ProductCategory', 'logo', 'blue-corner-logo-logo.png', 'uploads/category/blue-corner-logo-logo.png', '2019-03-07 01:07:46', '2019-03-07 01:07:46'),
(2, 2, 'BC\\Repositories\\ProductCategory\\ProductCategory', 'logo', 'mens-logo-logo.png', 'uploads/category/mens-logo-logo.png', '2019-03-07 01:22:56', '2019-03-07 01:32:11'),
(3, 3, 'BC\\Repositories\\ProductCategory\\ProductCategory', 'logo', 'womens-logo-logo.png', 'uploads/category/womens-logo-logo.png', '2019-03-07 01:31:24', '2019-03-07 01:31:24'),
(4, 4, 'BC\\Repositories\\ProductCategory\\ProductCategory', 'logo', 'kids-logo-logo.png', 'uploads/category/kids-logo-logo.png', '2019-03-07 01:32:56', '2019-03-07 01:32:56'),
(6, 1, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'round-neck-round-neck-banner-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/round-neck-round-neck-banner-product_mobile_banner_image.jpg', '2019-03-07 02:13:26', '2019-03-07 02:13:26'),
(7, 1, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'round-neck-round-neck-banner-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/round-neck-round-neck-banner-tablet-product_tablet_banner_image.jpg', '2019-03-07 02:13:26', '2019-03-07 02:13:26'),
(8, 1, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-cream-img-1401-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-cream-img-1401-2-product_variation_image.jpg', '2019-03-07 02:27:11', '2019-03-07 02:27:11'),
(10, 2, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-coral-img-1448-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-coral-img-1448-2-product_variation_image.jpg', '2019-03-07 02:27:11', '2019-03-07 02:32:35'),
(12, 3, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-forest-green-img-1413-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-forest-green-img-1413-2-product_variation_image.jpg', '2019-03-07 02:27:11', '2019-03-07 02:32:35'),
(14, 4, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-columbia-blue-img-1389-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-columbia-blue-img-1389-2-product_variation_image.jpg', '2019-03-07 02:27:11', '2019-03-07 02:32:35'),
(16, 5, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-choco-brown-img-1446-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-choco-brown-img-1446-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(18, 6, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-freshman-green-img-1463-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-freshman-green-img-1463-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(20, 7, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-chestnut-img-1467-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-chestnut-img-1467-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(22, 8, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-black-img-1396-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-black-img-1396-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(24, 9, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-azalea-img-1444-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-azalea-img-1444-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(26, 10, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-aqua-blue-img-1434-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-aqua-blue-img-1434-2-product_variation_image.jpg', '2019-03-07 02:27:12', '2019-03-07 02:32:35'),
(28, 1, 'BC\\Repositories\\Settings\\Setting', 'featured_mobile', 'blue-corner-0-featured_mobile.png', 'uploads/setting/blue-corner-0-featured_mobile.png', '2019-03-07 02:34:43', '2019-03-07 02:34:43'),
(29, 1, 'BC\\Repositories\\Settings\\Setting', 'featured_tablet', 'blue-corner-0-featured_tablet.png', 'uploads/setting/blue-corner-0-featured_tablet.png', '2019-03-07 02:34:44', '2019-03-07 02:34:44'),
(47, 11, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-coral-img-1448-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-coral-img-1448-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(48, 12, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-aqua-blue-img-1434-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-aqua-blue-img-1434-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(49, 13, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-azalea-img-1444-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-azalea-img-1444-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(50, 14, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-chestnut-img-1467-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-chestnut-img-1467-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(51, 15, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-black-img-1396-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-black-img-1396-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(52, 16, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-choco-brown-img-1446-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-choco-brown-img-1446-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(53, 17, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-columbia-blue-img-1389-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-columbia-blue-img-1389-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(54, 18, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-forest-green-img-1413-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-forest-green-img-1413-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(55, 19, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-cream-img-1401-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-cream-img-1401-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(56, 20, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-freshman-green-img-1463-2-product_variation_image.jpg', 'uploads/product/variation/round-neck-freshman-green-img-1463-2-product_variation_image.jpg', '2019-03-07 02:57:22', '2019-03-07 02:57:22'),
(58, 2, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'v-neck-v-neck-454x384-product_featured_image.jpg', 'uploads/product/featured-image/v-neck-v-neck-454x384-product_featured_image.jpg', '2019-03-07 04:17:15', '2019-03-07 04:17:15'),
(59, 2, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'v-neck-v-neck-banner-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/v-neck-v-neck-banner-product_mobile_banner_image.jpg', '2019-03-07 04:21:39', '2019-03-07 04:21:39'),
(60, 2, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'v-neck-v-neck-banner-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/v-neck-v-neck-banner-tablet-product_tablet_banner_image.jpg', '2019-03-07 04:21:39', '2019-03-07 04:21:39'),
(61, 21, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-aqua-blue-img-1559-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-aqua-blue-img-1559-1-product_variation_image.jpg', '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(62, 22, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-azalea-img-1588-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-azalea-img-1588-1-product_variation_image.jpg', '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(63, 23, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-choco-brown-img-1584-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-choco-brown-img-1584-1-product_variation_image.jpg', '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(64, 25, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-black-img-1595-product_variation_image.jpg', 'uploads/product/variation/v-neck-black-img-1595-product_variation_image.jpg', '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(65, 26, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-freshman-green-img-1573-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-freshman-green-img-1573-1-product_variation_image.jpg', '2019-03-07 04:24:13', '2019-03-07 04:24:13'),
(66, 27, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-lime-green-img-1558-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-lime-green-img-1558-1-product_variation_image.jpg', '2019-03-07 04:28:59', '2019-03-07 04:28:59'),
(67, 28, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-mint-green-img-1568-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-mint-green-img-1568-1-product_variation_image.jpg', '2019-03-07 04:28:59', '2019-03-07 04:28:59'),
(68, 29, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-pink-img-1581-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-pink-img-1581-1-product_variation_image.jpg', '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(69, 30, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-purlple-img-1545-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-purlple-img-1545-1-product_variation_image.jpg', '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(70, 31, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-tangerine-img-1565-product_variation_image.jpg', 'uploads/product/variation/v-neck-tangerine-img-1565-product_variation_image.jpg', '2019-03-07 04:29:00', '2019-03-07 04:29:00'),
(71, 1, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'round-neck-product9-454x384-product_featured_image.jpg', 'uploads/product/featured-image/round-neck-product9-454x384-product_featured_image.jpg', '2019-03-07 17:11:12', '2019-03-07 20:57:31'),
(72, 3, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'v-neck-comfort-wear-3415-00093-1-e1474339516700-454x384-product_featured_image.jpg', 'uploads/product/featured-image/v-neck-comfort-wear-3415-00093-1-e1474339516700-454x384-product_featured_image.jpg', '2019-03-07 18:55:56', '2019-03-07 18:55:56'),
(73, 3, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'v-neck-comfort-wear-v-neck-comfort-wear-banner-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/v-neck-comfort-wear-v-neck-comfort-wear-banner-product_mobile_banner_image.jpg', '2019-03-07 18:55:56', '2019-03-07 18:55:56'),
(74, 3, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'v-neck-comfort-wear-v-neck-comfort-wear-banner-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/v-neck-comfort-wear-v-neck-comfort-wear-banner-tablet-product_tablet_banner_image.jpg', '2019-03-07 18:55:56', '2019-03-07 18:55:56'),
(75, 32, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-black-img-1665-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-black-img-1665-1-product_variation_image.jpg', '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(76, 33, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-irish-green-img-1657-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-irish-green-img-1657-1-product_variation_image.jpg', '2019-03-07 19:07:53', '2019-03-07 19:07:53'),
(77, 35, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-heather-white-img-1677-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-heather-white-img-1677-1-product_variation_image.jpg', '2019-03-07 19:13:31', '2019-03-07 19:13:31'),
(78, 36, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-maize-img-1676-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-maize-img-1676-1-product_variation_image.jpg', '2019-03-07 19:13:31', '2019-03-07 19:13:31'),
(79, 37, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-navy-blue-img-1663-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-navy-blue-img-1663-1-product_variation_image.jpg', '2019-03-07 19:13:31', '2019-03-07 19:13:31'),
(80, 38, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-red-img-1661-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-red-img-1661-1-product_variation_image.jpg', '2019-03-07 19:13:31', '2019-03-07 19:13:31'),
(81, 39, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-red-wine-img-1668-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-red-wine-img-1668-1-product_variation_image.jpg', '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(82, 40, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-royal-blue-img-1674-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-royal-blue-img-1674-1-product_variation_image.jpg', '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(83, 41, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-white-img-1680-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-white-img-1680-1-product_variation_image.jpg', '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(84, 42, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-violet-img-1669-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-violet-img-1669-1-product_variation_image.jpg', '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(85, 43, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-comfort-wear-lt-blue-img-1659-product_variation_image.jpg', 'uploads/product/variation/v-neck-comfort-wear-lt-blue-img-1659-product_variation_image.jpg', '2019-03-07 19:13:32', '2019-03-07 19:13:32'),
(86, 4, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'fashion-polo-white-stripes-product6-454x384-product_featured_image.jpg', 'uploads/product/featured-image/fashion-polo-white-stripes-product6-454x384-product_featured_image.jpg', '2019-03-07 19:28:41', '2019-03-07 19:28:41'),
(87, 4, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'fashion-polo-white-stripes-fashion-polo-with-stripes-banner-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/fashion-polo-white-stripes-fashion-polo-with-stripes-banner-product_mobile_banner_image.jpg', '2019-03-07 19:28:41', '2019-03-07 19:28:41'),
(88, 4, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'fashion-polo-white-stripes-fashion-polo-with-stripes-banner-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/fashion-polo-white-stripes-fashion-polo-with-stripes-banner-tablet-product_tablet_banner_image.jpg', '2019-03-07 19:28:41', '2019-03-07 19:28:41'),
(89, 44, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-heather-white-img-1793-3-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-heather-white-img-1793-3-product_variation_image.jpg', '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(90, 45, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-lime-green-img-1790-3-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-lime-green-img-1790-3-product_variation_image.jpg', '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(91, 46, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-navy-blue-img-1782-3-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-navy-blue-img-1782-3-product_variation_image.jpg', '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(92, 47, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-red-img-1786-3-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-red-img-1786-3-product_variation_image.jpg', '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(93, 48, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-white-img-1788-2-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-white-img-1788-2-product_variation_image.jpg', '2019-03-07 19:34:55', '2019-03-07 19:34:55'),
(94, 1, 'BC\\Repositories\\Settings\\Setting', 'featured_mobile', 'blue-corner-1-featured_mobile.png', 'uploads/setting/blue-corner-1-featured_mobile.png', '2019-03-07 19:44:21', '2019-03-07 19:44:21'),
(95, 1, 'BC\\Repositories\\Settings\\Setting', 'featured_tablet', 'blue-corner-1-featured_tablet.png', 'uploads/setting/blue-corner-1-featured_tablet.png', '2019-03-07 19:44:21', '2019-03-07 19:44:21'),
(96, 1, 'BC\\Repositories\\Contest\\Contest', 'contest_banner_mobile', 'my-contest-contest_banner_mobile-contest_banner_mobile.jpg', 'uploads/contest/mobile/my-contest-contest_banner_mobile-contest_banner_mobile.jpg', '2019-03-07 19:45:35', '2019-03-07 19:45:35'),
(97, 1, 'BC\\Repositories\\Contest\\Contest', 'contest_banner_tablet', 'my-contest-contest_banner_tablet-contest_banner_tablet.jpg', 'uploads/contest/tablet/my-contest-contest_banner_tablet-contest_banner_tablet.jpg', '2019-03-07 19:45:35', '2019-03-07 19:45:35'),
(98, 1, 'BC\\Repositories\\Contest\\Contest', 'contest_thumb_mobile', 'my-contest-contest_thumb_mobile-contest_thumb_mobile.png', 'uploads/contest/mobile/my-contest-contest_thumb_mobile-contest_thumb_mobile.png', '2019-03-07 19:45:35', '2019-03-07 19:45:35'),
(99, 1, 'BC\\Repositories\\Contest\\Contest', 'contest_thumb_tablet', 'my-contest-contest_thumb_tablet-contest_thumb_tablet.png', 'uploads/contest/tablet/my-contest-contest_thumb_tablet-contest_thumb_tablet.png', '2019-03-07 19:45:35', '2019-03-07 19:45:35'),
(100, 49, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-black-img-1828-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-black-img-1828-1-product_variation_image.jpg', '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(101, 50, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-white-img-1848-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-white-img-1848-product_variation_image.jpg', '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(102, 51, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-freshman-green-img-1838-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-freshman-green-img-1838-1-product_variation_image.jpg', '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(103, 52, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-yellow-img-1846-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-yellow-img-1846-product_variation_image.jpg', '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(104, 53, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-white-stripes-red-img-1850-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-white-stripes-red-img-1850-product_variation_image.jpg', '2019-03-07 19:46:02', '2019-03-07 19:46:02'),
(105, 5, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'single-jersey-single-jersey-1-product_featured_image.jpg', 'uploads/product/featured-image/single-jersey-single-jersey-1-product_featured_image.jpg', '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(106, 5, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'single-jersey-single-jersey-banner-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/single-jersey-single-jersey-banner-product_mobile_banner_image.jpg', '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(107, 5, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'single-jersey-single-jersey-banner-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/single-jersey-single-jersey-banner-tablet-product_tablet_banner_image.jpg', '2019-03-07 19:56:32', '2019-03-07 19:56:32'),
(108, 54, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-black-img-1645-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-black-img-1645-1-product_variation_image.jpg', '2019-03-07 20:00:10', '2019-03-07 20:00:10'),
(109, 55, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-light-blue-img-1635-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-light-blue-img-1635-1-product_variation_image.jpg', '2019-03-07 20:00:10', '2019-03-07 20:00:10'),
(110, 56, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-lime-green-img-1641-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-lime-green-img-1641-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(111, 57, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-navy-blue-img-1631-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-navy-blue-img-1631-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(112, 58, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-scarlet-img-1647-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-scarlet-img-1647-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(113, 59, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-royal-blue-img-1629-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-royal-blue-img-1629-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(114, 60, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-white-img-1634-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-white-img-1634-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(115, 61, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-pink-img-1639-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-pink-img-1639-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(116, 62, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-purple-img-1654-1-product_variation_image.jpg', 'uploads/product/variation/single-jersey-purple-img-1654-1-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(117, 63, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'single-jersey-violet-img-1654-product_variation_image.jpg', 'uploads/product/variation/single-jersey-violet-img-1654-product_variation_image.jpg', '2019-03-07 20:00:11', '2019-03-07 20:00:11'),
(118, 6, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'round-neck-shirt-round-neck-woens-454x384-product_featured_image.jpg', 'uploads/product/featured-image/round-neck-shirt-round-neck-woens-454x384-product_featured_image.jpg', '2019-03-07 21:05:08', '2019-03-07 21:05:08'),
(119, 6, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'round-neck-shirt-round-neck-women-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/round-neck-shirt-round-neck-women-mobile-product_mobile_banner_image.jpg', '2019-03-07 21:14:28', '2019-03-07 21:14:28'),
(120, 6, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'round-neck-shirt-round-neck-woemn-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/round-neck-shirt-round-neck-woemn-product_tablet_banner_image.jpg', '2019-03-07 21:14:28', '2019-03-07 21:14:28'),
(121, 64, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-black-img-1984-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-black-img-1984-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(122, 65, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-blue-jewel-img-1986-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-blue-jewel-img-1986-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(123, 66, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-freshman-green-img-1966-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-freshman-green-img-1966-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(124, 67, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-heather-white-img-1970-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-heather-white-img-1970-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(125, 68, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-light-blue-img-1977-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-light-blue-img-1977-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(126, 69, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-royal-blue-img-1974-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-royal-blue-img-1974-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(127, 70, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-red-wine-img-1952-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-red-wine-img-1952-1-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(128, 71, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-yellow-gold-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-yellow-gold-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(129, 72, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-lime-green-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-lime-green-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(130, 73, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-red-img-1950-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-red-img-1950-product_variation_image.jpg', '2019-03-07 21:23:42', '2019-03-07 21:23:42'),
(131, 7, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'v-neck-shirt-v-neck-womens-454x384-product_featured_image.jpg', 'uploads/product/featured-image/v-neck-shirt-v-neck-womens-454x384-product_featured_image.jpg', '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(132, 7, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'v-neck-shirt-v-neck-women-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/v-neck-shirt-v-neck-women-product_mobile_banner_image.jpg', '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(133, 7, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'v-neck-shirt-v-neck-women-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/v-neck-shirt-v-neck-women-tablet-product_tablet_banner_image.jpg', '2019-03-07 21:28:11', '2019-03-07 21:28:11'),
(134, 74, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-aqua-blue-img-2013-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-aqua-blue-img-2013-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(135, 75, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-azalea-img-1990-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-azalea-img-1990-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(136, 76, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-black-img-2015-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-black-img-2015-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(137, 77, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-freshman-green-img-1988-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-freshman-green-img-1988-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(138, 78, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-lime-green-img-2005-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-lime-green-img-2005-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(139, 79, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-mint-green-img-1994-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-mint-green-img-1994-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(140, 80, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-vibrant-yellow-img-2028-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-vibrant-yellow-img-2028-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(141, 81, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-red-img-2022-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-red-img-2022-1-product_variation_image.jpg', '2019-03-07 21:32:36', '2019-03-07 21:32:36'),
(142, 82, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-red-wine-img-1996-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-red-wine-img-1996-1-product_variation_image.jpg', '2019-03-07 21:32:37', '2019-03-07 21:32:37'),
(143, 83, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-royal-blue-img-1997-1-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-royal-blue-img-1997-1-product_variation_image.jpg', '2019-03-07 21:32:37', '2019-03-07 21:32:37'),
(144, 8, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'polo-shirt-aiiz-single-jersy-polo-shirt-others-454x384-product_featured_image.jpg', 'uploads/product/featured-image/polo-shirt-aiiz-single-jersy-polo-shirt-others-454x384-product_featured_image.jpg', '2019-03-07 21:38:48', '2019-03-07 21:38:48'),
(145, 8, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'polo-shirt-polo-shirt-women-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/polo-shirt-polo-shirt-women-mobile-product_mobile_banner_image.jpg', '2019-03-07 21:38:48', '2019-03-07 21:38:48'),
(146, 8, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'polo-shirt-polo-shirt-women-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/polo-shirt-polo-shirt-women-tablet-product_tablet_banner_image.jpg', '2019-03-07 21:38:49', '2019-03-07 21:38:49'),
(147, 5, 'BC\\Repositories\\ProductCategory\\ProductCategory', 'logo', 'aiiz-ultra-cotton-logo-logo.png', 'uploads/category/aiiz-ultra-cotton-logo-logo.png', '2019-03-07 21:48:25', '2019-03-07 21:48:25'),
(148, 84, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-alphine-img-1500-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-alphine-img-1500-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(149, 85, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-apple-green-img-1527-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-apple-green-img-1527-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(150, 86, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-azalea-img-1498-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-azalea-img-1498-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(151, 87, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-black-img-1504-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-black-img-1504-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(152, 88, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-columbia-blue-img-1544-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-columbia-blue-img-1544-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(153, 89, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-cream-img-1512-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-cream-img-1512-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(154, 90, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-freshman-green-img-1485-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-freshman-green-img-1485-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(155, 91, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-heather-white-img-1501-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-heather-white-img-1501-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(156, 92, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-red-img-1491-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-red-img-1491-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(157, 93, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-red-wine-img-1530-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-red-wine-img-1530-1-product_variation_image.jpg', '2019-03-07 22:13:07', '2019-03-07 22:13:07'),
(158, 9, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'fashion-polo-shirt-untitled-1-copy-1-product_featured_image.jpg', 'uploads/product/featured-image/fashion-polo-shirt-untitled-1-copy-1-product_featured_image.jpg', '2019-03-07 22:15:41', '2019-03-07 22:15:41'),
(159, 9, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'fashion-polo-shirt-fashion-polo-shirt-women-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/fashion-polo-shirt-fashion-polo-shirt-women-mobile-product_mobile_banner_image.jpg', '2019-03-07 22:17:01', '2019-03-07 22:17:01'),
(160, 9, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'fashion-polo-shirt-fashion-polo-shirt-women-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/fashion-polo-shirt-fashion-polo-shirt-women-tablet-product_tablet_banner_image.jpg', '2019-03-07 22:17:01', '2019-03-07 22:17:01'),
(161, 94, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-black-img-1934-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-black-img-1934-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(162, 95, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-blue-jewel-img-1940-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-blue-jewel-img-1940-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(163, 96, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-choco-brown-img-1927-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-choco-brown-img-1927-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(164, 97, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-heather-white-img-1931-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-heather-white-img-1931-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(165, 98, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-heather-white-img-1931-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-heather-white-img-1931-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(166, 99, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-azalea-img-1947-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-azalea-img-1947-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(167, 100, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-black-img-1902-2-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-black-img-1902-2-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(168, 101, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-freshman-green-img-1895-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-freshman-green-img-1895-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(169, 102, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-lemon-chrome-img-1943-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-lemon-chrome-img-1943-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(170, 103, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-red-img-1868-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-red-img-1868-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(171, 104, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-alphine-img-1869-2-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-alphine-img-1869-2-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(172, 105, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-black-img-1894-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-black-img-1894-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(173, 106, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-royal-blue-img-1865-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-royal-blue-img-1865-1-product_variation_image.jpg', '2019-03-07 22:26:04', '2019-03-07 22:26:04'),
(174, 107, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-violet-img-1871-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-violet-img-1871-1-product_variation_image.jpg', '2019-03-07 22:26:05', '2019-03-07 22:26:05'),
(175, 108, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-white-img-1861-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-white-img-1861-1-product_variation_image.jpg', '2019-03-07 22:26:05', '2019-03-07 22:26:05'),
(176, 109, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-aqua-blue-img-2046-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-aqua-blue-img-2046-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(177, 110, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-azalea-img-2068-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-azalea-img-2068-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(178, 111, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-black-img-2039-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-black-img-2039-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(179, 112, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-choco-brown-img-2065-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-choco-brown-img-2065-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(180, 113, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-freshman-green-img-2058-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-freshman-green-img-2058-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(181, 114, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-red-img-2064-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-red-img-2064-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(182, 115, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-red-wine-img-2076-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-red-wine-img-2076-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(183, 116, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-white-img-2044-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-white-img-2044-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(184, 117, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-yellow-gold-img-2042-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-yellow-gold-img-2042-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(185, 118, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-shirt-lime-green-img-2071-1-product_variation_image.jpg', 'uploads/product/variation/round-neck-shirt-lime-green-img-2071-1-product_variation_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(186, 10, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'round-neck-shirt-kid-product_featured_image.jpg', 'uploads/product/featured-image/round-neck-shirt-kid-product_featured_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(187, 10, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'round-neck-shirt-round-neck-shirt-kid-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/round-neck-shirt-round-neck-shirt-kid-mobile-product_mobile_banner_image.jpg', '2019-03-07 22:39:31', '2019-03-07 22:39:31'),
(188, 10, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'round-neck-shirt-round-neck-shirt-kid-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/round-neck-shirt-round-neck-shirt-kid-tablet-product_tablet_banner_image.jpg', '2019-03-07 22:39:32', '2019-03-07 22:39:32'),
(189, 119, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'v-neck-shirt-untitled-2-copy-product_variation_image.jpg', 'uploads/product/variation/v-neck-shirt-untitled-2-copy-product_variation_image.jpg', '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(190, 11, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'v-neck-shirt-untitled-2-copy-product_featured_image.jpg', 'uploads/product/featured-image/v-neck-shirt-untitled-2-copy-product_featured_image.jpg', '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(191, 11, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'v-neck-shirt-v-neck-shirt-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/v-neck-shirt-v-neck-shirt-mobile-product_mobile_banner_image.jpg', '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(192, 11, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'v-neck-shirt-v-neck-shirt-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/v-neck-shirt-v-neck-shirt-tablet-product_tablet_banner_image.jpg', '2019-03-07 22:44:49', '2019-03-07 22:44:49'),
(193, 120, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-azalea-img-2181-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-azalea-img-2181-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(194, 121, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-blue-jewel-img-2170-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-blue-jewel-img-2170-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(195, 122, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-columbia-blue-img-2175-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-columbia-blue-img-2175-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(196, 123, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-lemon-chrome-img-2179-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-lemon-chrome-img-2179-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(197, 124, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-lime-green-img-2147-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-lime-green-img-2147-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(198, 125, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-navy-blue-img-2158-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-navy-blue-img-2158-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(199, 126, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-orange-img-2172-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-orange-img-2172-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(200, 127, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-pink-img-2177-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-pink-img-2177-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(201, 128, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-red-wine-img-2148-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-red-wine-img-2148-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51');
INSERT INTO `uploadables` (`id`, `uploadable_id`, `uploadable_type`, `key`, `filename`, `path`, `created_at`, `updated_at`) VALUES
(202, 129, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'polo-shirt-violet-img-2152-1-product_variation_image.jpg', 'uploads/product/variation/polo-shirt-violet-img-2152-1-product_variation_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(203, 12, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'polo-shirt-polo-shirt-454x384-product_featured_image.jpg', 'uploads/product/featured-image/polo-shirt-polo-shirt-454x384-product_featured_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(204, 12, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'polo-shirt-polo-shirt-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/polo-shirt-polo-shirt-mobile-product_mobile_banner_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(205, 12, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'polo-shirt-polo-shirt-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/polo-shirt-polo-shirt-tablet-product_tablet_banner_image.jpg', '2019-03-07 22:52:51', '2019-03-07 22:52:51'),
(206, 130, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-white-img-2167-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-white-img-2167-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(207, 131, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-violet-img-2163-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-violet-img-2163-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(208, 132, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-red-img-2157-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-red-img-2157-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(209, 133, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-lime-green-img-2169-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-lime-green-img-2169-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(210, 134, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-blue-jewel-img-2161-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-blue-jewel-img-2161-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(211, 135, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'fashion-polo-shirt-aplhine-img-2164-1-product_variation_image.jpg', 'uploads/product/variation/fashion-polo-shirt-aplhine-img-2164-1-product_variation_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(212, 13, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'fashion-polo-shirt-untitled-1-copy-2-product_featured_image.jpg', 'uploads/product/featured-image/fashion-polo-shirt-untitled-1-copy-2-product_featured_image.jpg', '2019-03-07 22:58:02', '2019-03-07 22:58:02'),
(213, 13, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'fashion-polo-shirt-fashion-polo-shirt-kid-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/fashion-polo-shirt-fashion-polo-shirt-kid-mobile-product_mobile_banner_image.jpg', '2019-03-07 23:03:02', '2019-03-07 23:03:02'),
(214, 13, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'fashion-polo-shirt-fashion-polo-shirt-kid-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/fashion-polo-shirt-fashion-polo-shirt-kid-tablet-product_tablet_banner_image.jpg', '2019-03-07 23:03:03', '2019-03-07 23:03:03'),
(215, 136, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'sando-kids-6-in-1-white-boys-untitled-333-copy-product_variation_image.jpg', 'uploads/product/variation/sando-kids-6-in-1-white-boys-untitled-333-copy-product_variation_image.jpg', '2019-03-07 23:19:05', '2019-03-07 23:19:05'),
(216, 14, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'sando-kids-6-in-1-white-boys-untitled-333-copy-product_featured_image.jpg', 'uploads/product/featured-image/sando-kids-6-in-1-white-boys-untitled-333-copy-product_featured_image.jpg', '2019-03-07 23:19:05', '2019-03-07 23:19:05'),
(217, 14, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'sando-kids-6-in-1-white-boys-sando-kids-mobile-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/sando-kids-6-in-1-white-boys-sando-kids-mobile-product_mobile_banner_image.jpg', '2019-03-07 23:19:05', '2019-03-07 23:19:05'),
(218, 14, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'sando-kids-6-in-1-white-boys-sando-kids-tablet-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/sando-kids-6-in-1-white-boys-sando-kids-tablet-product_tablet_banner_image.jpg', '2019-03-07 23:19:05', '2019-03-07 23:19:05'),
(219, 15, 'BC\\Repositories\\Product\\Product', 'product_featured_image', 'round-neck-round-neck-product_featured_image.png', 'uploads/product/featured-image/round-neck-round-neck-product_featured_image.png', '2019-03-08 02:26:26', '2019-03-08 02:26:26'),
(220, 15, 'BC\\Repositories\\Product\\Product', 'product_mobile_banner_image', 'round-neck-round-neck-round-neck-banner-product-mobile-banner-image-product_mobile_banner_image.jpg', 'uploads/product/banner/mobile/round-neck-round-neck-round-neck-banner-product-mobile-banner-image-product_mobile_banner_image.jpg', '2019-03-08 02:26:26', '2019-03-08 02:26:26'),
(221, 15, 'BC\\Repositories\\Product\\Product', 'product_tablet_banner_image', 'round-neck-round-neck-round-neck-banner-tablet-product-tablet-banner-image-product_tablet_banner_image.jpg', 'uploads/product/banner/tablet/round-neck-round-neck-round-neck-banner-tablet-product-tablet-banner-image-product_tablet_banner_image.jpg', '2019-03-08 02:26:26', '2019-03-08 02:26:26'),
(222, 137, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-maroon-product_variation_image.png', 'uploads/product/variation/round-neck-maroon-product_variation_image.png', '2019-03-08 02:33:18', '2019-03-08 02:33:18'),
(223, 138, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-moss-green-product_variation_image.png', 'uploads/product/variation/round-neck-moss-green-product_variation_image.png', '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(224, 139, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-muroise-product_variation_image.png', 'uploads/product/variation/round-neck-muroise-product_variation_image.png', '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(225, 140, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-navy-blue-product_variation_image.png', 'uploads/product/variation/round-neck-navy-blue-product_variation_image.png', '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(226, 141, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-avocado-product_variation_image.png', 'uploads/product/variation/round-neck-avocado-product_variation_image.png', '2019-03-08 02:36:38', '2019-03-08 02:36:38'),
(227, 142, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-peach-product_variation_image.png', 'uploads/product/variation/round-neck-peach-product_variation_image.png', '2019-03-08 02:39:16', '2019-03-08 02:39:16'),
(228, 143, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-pink-product_variation_image.png', 'uploads/product/variation/round-neck-pink-product_variation_image.png', '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(229, 144, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-red-product_variation_image.png', 'uploads/product/variation/round-neck-red-product_variation_image.png', '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(230, 145, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-royal-blue-product_variation_image.png', 'uploads/product/variation/round-neck-royal-blue-product_variation_image.png', '2019-03-08 03:21:30', '2019-03-08 03:21:30'),
(231, 146, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-safari-product_variation_image.png', 'uploads/product/variation/round-neck-safari-product_variation_image.png', '2019-03-08 03:24:16', '2019-03-08 03:24:16'),
(232, 147, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-sunkist-product_variation_image.png', 'uploads/product/variation/round-neck-sunkist-product_variation_image.png', '2019-03-08 03:27:11', '2019-03-08 03:27:11'),
(233, 148, 'BC\\Repositories\\ProductVariation\\ProductVariation', 'product_variation_image', 'round-neck-sweet-corn-product_variation_image.png', 'uploads/product/variation/round-neck-sweet-corn-product_variation_image.png', '2019-03-08 03:27:11', '2019-03-08 03:27:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `active`, `last_login`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Super', 'Admin', 'admin@email.com', '$2y$10$0sPJhiM5HTvUgCzppwwpteWcMS6EnRM6zg/HxaooLYBMoOKnNGgHe', 1, NULL, '2019-03-07 01:03:56', '2019-03-07 01:03:56', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audits`
--
ALTER TABLE `audits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contests`
--
ALTER TABLE `contests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contest_items`
--
ALTER TABLE `contest_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sizes_name_unique` (`name`);

--
-- Indexes for table `styles`
--
ALTER TABLE `styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploadables`
--
ALTER TABLE `uploadables`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audits`
--
ALTER TABLE `audits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `contests`
--
ALTER TABLE `contests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contest_items`
--
ALTER TABLE `contest_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4293;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `styles`
--
ALTER TABLE `styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `uploadables`
--
ALTER TABLE `uploadables`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
