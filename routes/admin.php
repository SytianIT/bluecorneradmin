<?php

Auth::routes();
Route::get('/', function () {
    return redirect()->to('login');
})->name('login');

Route::get('/admin/{app?}/{app1?}/{app2?}/{app3?}/{app4?}', function () {
    return view('auth.index');
})->middleware('auth');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('logout', 'Auth\LoginController@logout');
// Start of all API's
Route::group(['prefix' => 'api'], function() {
    // Check authenticated user
    Route::group(['namespace' => 'Auth\Api'], function() {
        Route::get('check-login', 'AuthController@check');
    });

    // users controller
    Route::group(['prefix' => 'users/','as' => 'users.'], function () {
        Route::group(['namespace' => 'Admin\Users'], function() {
            Route::get('/', 'UsersController@index')->name('index');
            Route::get('create', 'UsersController@create')->name('create');
            Route::post('store', 'UsersController@store')->name('store');
            Route::get('edit/{userid}','UsersController@edit')->name('edit');
            Route::post('update/{userid}', 'UsersController@update')->name('update');
            Route::get('delete/{userid}', 'UsersController@destroy')->name('delete');
        });
    });


    // Audit controller
    Route::group(['prefix' => 'audits/','as' => 'audits.'], function () {
        Route::group(['namespace' => 'Admin\Audit'], function() {
            Route::get('/', 'AuditController@index')->name('index');
        });
    });

    // Permission controller
    Route::group(['prefix' => 'permissions/','as' => 'permissions.'], function () {
        Route::group(['namespace' => 'Admin\Permissions'], function () {
            Route::get('/', 'PermissionController@index')->name('index');
            Route::get('create', 'PermissionController@create')->name('create');
            Route::post('store', 'PermissionController@store')->name('store');
            Route::get('edit/{Permission}','PermissionController@edit')->name('edit');
            Route::post('update/{Permission}', 'PermissionController@update')->name('update');
            Route::get('delete/{Permission}', 'PermissionController@destroy')->name('delete');
        });
    });

    // Role controller
    Route::group(['prefix' => 'roles/','as' => 'roles.'], function () {
        Route::group(['namespace' => 'Admin\Roles'], function () {
            Route::get('/', 'RolesController@index')->name('index');
            Route::get('create', 'RolesController@create')->name('create');
            Route::post('store', 'RolesController@store')->name('store');
            Route::get('edit/{roles}','RolesController@edit')->name('edit');
            Route::post('update/{roles}', 'RolesController@update')->name('update');
            Route::get('delete/{roles}', 'RolesController@destroy')->name('delete');
        });
    });
    // contest controller
    Route::group(['prefix' => 'contests/','as' => 'contests.'], function () {
        Route::group(['namespace' => 'Admin\Contests'], function() {
            Route::get('/', 'ContestsController@index')->name('index');
            Route::get('create', 'ContestsController@create')->name('create');
            Route::post('store', 'ContestsController@store')->name('store');
            Route::get('edit/{contsid}','ContestsController@edit')->name('edit');
            Route::post('update/{contsid}', 'ContestsController@update')->name('update');
            Route::get('delete/{contsid}', 'ContestsController@destroy')->name('delete');
            Route::get('move-to-archive/{contest}', 'ContestsController@moveToArchive')->name('movetoarchive');
        });
        // Items
        Route::group(['prefix' => '{contsid}/items/','as' => 'items.'], function() {
            Route::group(['namespace' => 'Admin\Items'], function() {
                Route::get('/', 'ItemsController@index')->name('index');
                Route::post('store', 'ItemsController@store')->name('store');
                Route::get('edit', 'ItemsController@edit')->name('edit');
                Route::post('update', 'ItemsController@update')->name('update');
                Route::get('delete/{item}', 'ItemsController@destroy')->name('delete');
            });
        });
        //Answers
        // Route::group(['prefix' => '{contsid}/answer','as' => 'audits.'], function () {
        //  Route::group(['namespace' => 'Admin\Answer'], function() {
        //      Route::get('/', 'AnswerController@index')->name('index');
        //  });
        // });
    });

    Route::group(['prefix' => 'archive/','as' => 'archive.'], function () {
        Route::group(['prefix' => 'feedback','as' => 'feedback.','namespace' => 'Admin\Contests'], function() {
            Route::get('/', 'ContestsController@archiveIndex')->name('index');
        });
        Route::group(['prefix' => 'feedback','as' => 'feedback.'], function() {
            Route::group(['prefix' => 'entries','as' => 'entries.','namespace' => 'Admin\Answer'], function() {
                Route::get('/', 'AnswerController@archiveIndex')->name('index');
            });
        });
    });

	//Feedback Entry
    Route::group(['prefix' => 'feedback','as' => 'feedback.'], function () {
        Route::group(['namespace' => 'Admin\Answer'], function() {
            Route::get('{contest}/entries', 'AnswerController@filteredIndex')->name('filtered');
        });
        Route::group(['prefix' => 'entries','as' => 'entries.','namespace' => 'Admin\Answer'], function() {
            Route::get('/', 'AnswerController@index')->name('index');
        });
	});
    Route::group(['prefix' => 'feedback','as' => 'feedback.'], function () {
        Route::group(['prefix' => 'entry','as' => 'entry.','namespace' => 'Admin\Answer'], function() {
            Route::get('{contestEntry}/show', 'AnswerController@show')->name('view');
            Route::get('delete/{contestEntry}', 'AnswerController@delete')->name('delete');
        });
    });

    // products controller
    Route::group(['prefix' => 'products/','as' => 'products.'], function () {
        Route::group(['namespace' => 'Admin\Products'], function() {
            Route::get('/', 'ProductsController@index')->name('index');
            Route::get('create', 'ProductsController@create')->name('create');
            Route::post('store', 'ProductsController@store')->name('store');
            Route::get('edit/{product}','ProductsController@edit')->name('edit');
            Route::post('update/{product}', 'ProductsController@update')->name('update');
            Route::post('delete', 'ProductsController@destroy')->name('delete');
        });
    });
    // category controller
    Route::group(['prefix' => 'product-category/','as' => 'product-category.'], function () {
        Route::group(['namespace' => 'Admin\ProductCategories'], function() {
            Route::get('/', 'ProductCategoriesController@index')->name('index');
            Route::get('create', 'ProductCategoriesController@create')->name('create');
            Route::post('store', 'ProductCategoriesController@store')->name('store');
            Route::get('edit/{catid}','ProductCategoriesController@edit')->name('edit');
            Route::post('update/{catid}', 'ProductCategoriesController@update')->name('update');
            Route::get('delete/{catid}', 'ProductCategoriesController@destroy')->name('delete');
        });
    });
    // color controller
    Route::group(['prefix' => 'colors/','as' => 'colors.'], function () {
        Route::group(['namespace' => 'Admin\Colors'], function () {
            Route::get('/', 'ColorsController@index')->name('index');
            Route::post('store', 'ColorsController@store')->name('store');
            Route::get('edit/{colors}','ColorsController@edit')->name('edit');
            Route::post('edit/{colors}', 'ColorsController@update')->name('update');
            Route::get('delete/{colors}', 'ColorsController@delete')->name('delete');
        });
    });
    // size controller
    Route::group(['prefix' => 'sizes/','as' => 'sizes.'], function () {
        Route::group(['namespace' => 'Admin\Sizes'], function () {
            Route::get('/', 'SizesController@index')->name('index');
            Route::post('create', 'SizesController@store')->name('store');
            Route::get('edit/{sizes}','SizesController@edit')->name('edit');
            Route::post('edit/{sizes}', 'SizesController@update')->name('update');
            Route::get('delete/{sizes}', 'SizesController@delete')->name('delete');
        });
        Route::group(['namespace' => 'Admin\SizesCategory'], function () {
            Route::get('sizecategories', 'SizesCategoryController@getSizeCategories')->name('sizecategories');
            Route::get('sizepercat/{sizecat}', 'SizesCategoryController@getSizePerCategories')->name('sizepercat');
        });
    });
    // size controller
    Route::group(['prefix' => 'sizes-category/','as' => 'sizes-category.'], function () {
        Route::group(['namespace' => 'Admin\SizesCategory'], function () {
            Route::post('create', 'SizesCategoryController@store')->name('store');
            Route::get('/', 'SizesCategoryController@index')->name('index');
            Route::get('edit/{sizecatid}','SizesCategoryController@edit')->name('edit');
            Route::post('edit/{sizecatid}', 'SizesCategoryController@update')->name('update');
            Route::get('delete/{sizecatid}', 'SizesCategoryController@delete')->name('delete');
        });
    });
    // settings controller
    Route::group(['prefix' => 'settings/','as' => 'settings.'], function () {
        Route::group(['namespace' => 'Admin\Settings'], function () {
            Route::get('/', 'SettingsController@index')->name('index');
            Route::post('update', 'SettingsController@store')->name('store');
            Route::get('delete/{settingid}', 'SettingsController@remove')->name('remove');
        });
    });
});
