<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('fetch-initial-data', 'InitialController@fetchInitial');
Route::get('fetch-settings', 'SettingsController@fetchSettings');
Route::get('fetch-products', 'ProductController@fetchProducts');
Route::get('fetch-product-variations', 'ProductController@fetchProductVariations');
Route::get('fetch-categories', 'CategoriesController@fetchCategories');
Route::get('fetch-contests', 'ContestController@fetchContest');

Route::get('submit-contests', 'ContestController@submitContest');
