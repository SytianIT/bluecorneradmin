<?php

namespace BC;

use Illuminate\Database\Eloquent\Model;
use BC\ContextInterface;

class Context implements ContextInterface {

	protected $model;

	public function set(Model $model)
	{
		$this->model = $model;
	}

	public function getInstance()
	{
		return $this->model;
	}

	public function has()
	{
		if ($this->model) return true;

		return false;
	}

	public function id()
	{
		return $this->model ? $this->model->id : null;
	}

	public function column()
	{
		return $this->model ? $this->model->getForeignKey() : null;
	}

	public function table()
	{
		return $this->model->getTable();
	}

	public function reset()
	{
		$this->model = null;
	}

}