<?php

namespace BC\Validation;

use Illuminate\Database\Eloquent\Model;

interface ValidationInterface {
	
	public function validate(Model $model);

	public function getErrors();

}