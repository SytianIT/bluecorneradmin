<?php

namespace BC;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

class UuidGenerator {

    public static function make($name = null)
    {
        try {
            
            // Generate a version 5 (name-based and hashed with SHA1) UUID object
            return Uuid::uuid5(Uuid::NAMESPACE_DNS, $name ? strtolower($name).time() : time());

        } catch (UnsatisfiedDependencyException $e) {
            
            // Some dependency was not met. Either the method cannot be called on a
            // 32-bit system, or it can, but it relies on Moontoast\Math to be present.
            echo 'Caught exception: ' . $e->getMessage() . "\n";

        }
    }
    
}
