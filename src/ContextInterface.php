<?php

namespace BC;

use Illuminate\Database\Eloquent\Model;

interface ContextInterface {

	public function set(Model $model);

	public function getInstance();

	public function has();

	public function id();

	public function column();

	public function table();

}