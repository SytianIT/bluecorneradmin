<?php

namespace BC\Repositories\Style;

use App\Model\BaseModel;
use BC\Repositories\ProductVariation\ProductVariation;
use BC\Repositories\Product\Product;
use BC\Repositories\Uploadable\UploadableRepository;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Style extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        'product_id' => 'required',
        'name'       => 'required',
    ];

    protected $fillable = [
        'product_id','name'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function productVariations() {
        return $this->hasMany(ProductVariation::class, 'style_id');
    }

    public static function boot() {
        parent::boot();
        $uploadRepo = resolve('BC\Repositories\Uploadable\UploadableRepository');
        static::deleting(function($style) use ($uploadRepo){
            $toDelete = $style->productVariations()->pluck('id')->toArray();
            $toDelete = collect($toDelete);
			$toDelete->each(function($item) use($uploadRepo) {
                ProductVariation::where('id', $item)->get()->each(function($prodVar) use ($uploadRepo) {
					$productvariation_image_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_image_path) : false;
					$uploadRepo->removeFileUploads($productvariation_image_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);

					$productvariation_color_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_color_path) : false;
					$uploadRepo->removeFileUploads($productvariation_color_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);
					$prodVar->delete();
				});
			});
        });
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Style '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Style '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Style '{$this->name}'";
    }
}
?>
