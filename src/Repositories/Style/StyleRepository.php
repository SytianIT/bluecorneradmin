<?php

namespace BC\Repositories\Style;

use BC\Repositories\BaseRepository;

use BC\Repositories\ProductVariation\ProductVariationRepository;
use BC\Repositories\Product\Product;
use BC\Repositories\Style\Style;
use BC\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class StyleRepository extends BaseRepository {

	protected $model;

	public function __construct( Style $style, ProductVariationRepository $pvRepo )
	{
		$this->model = $style;
		$this->pvRepo = $pvRepo;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store( $styles = [], Product $product , $requestName )
	{
		$styles = collect($styles);
		$stylesArr = collect();

		foreach($styles as $style) {
			$styleMod = new Style;
			$styleMod->product_id = $product->id;
			$styleMod->name =  $style['style_name'];
			$styleMod->order = $style['count'];
			$styleMod->save();
			$stylesArr->push($styleMod->load('product'));
		}

		$pv =  $this->pvRepo->store($styles, $stylesArr,true, $requestName);
	}

	public function update( $styles = [], Product $product, $request )
	{
		$styles = collect($styles);
		$stylesArr = collect();
		$updatedIds = [];
		$existingVariationsIds = $product->styles->pluck('id')->toArray();
		foreach($styles as $key => $style) {
			$updatedIds[] = $style['id'];
			$styleMod = isset($style['id']) && $style['id'] != '' ? $this->model->find($style['id']) : new Style();
			$styleMod->product_id = $product->id;
			$styleMod->name =  $style['style_name'];
			$styleMod->order = $key;
			$styleMod->save();
			$key++;
			$stylesArr->push($styleMod->load('product'));
		}
		$pv =  $this->pvRepo->update($styles, $stylesArr,true,$request);

		$toDelete = array_diff($existingVariationsIds, $updatedIds);
		$this->delete($toDelete);
	}

	public function delete($styles = [])
	{
		foreach( $styles as $key => $value ) {
			Style::where('id', $value)->get()->each(function($style) {
				$style->delete();
			});
		}
	}

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Style $style = null)
    {
        $rules = [
            'product_id'   => 'required|unique:style,product_id',
            'name'  => 'required',
        ];

        if( $style ) {
            if ($request->product_id == $style->product_id) {
                $rules['product_id'] = 'required';
            }
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
        }
    }
}
