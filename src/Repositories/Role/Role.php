<?php

namespace BC\Repositories\Role;

// use BC\Repositories\ProductCategory\ProductCategory;
use App\Model\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Role extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

    public function users() {
        return $this->belongsToMany(User::class, 'user_roles', 'role_id', 'user_id');
    }

	protected $rules = [
        'title'  => 'required',
    ];

    protected $fillable = [
        'title', 'description',
    ];
    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Role'{$this->title}'";
    }

    public function auditUpdate()
    {
        return "Role'{$this->title}'";
    }

    public function auditDelete()
    {
        return "Role'{$this->title}'";
    }
}
?>

