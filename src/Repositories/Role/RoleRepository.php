<?php

namespace BC\Repositories\Role;

use BC\Repositories\BaseRepository;

use BC\Repositories\Role\Role;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class RoleRepository extends BaseRepository {

	protected $model;

	public function __construct(Role $role)
	{
		$this->model = $role;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(Request $request, Role $role = null)
	{
		$role = $this->model;
		
		$this->validateData($request);
		
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$role->fill($request->input());
		$role->key = str_slug($request->title);
		$role->save();

		return $role;
	}

	public function update(Request $request)
	{
		$this->validateData($request); 
		
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$role = $this->model->find($request->id);
		$role->fill($request->input());
		$role->key = str_slug($request->title);
		$role->save();

		return $role;
	}

	public function delete(Request $request, $role_id)
    {

        $role = $this->model->find($role_id);
        $role->delete();

        return $role->title;
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Role $role = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'title' => 'required'
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }
}
