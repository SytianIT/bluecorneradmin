<?php

namespace BC\Repositories\Settings;

use App\Model\BaseModel;
use BC\Repositories\Uploadable\Uploadable;
use Watson\Validating\ValidatingTrait;

class Setting extends BaseModel
{

    const IMG_BANNER_MOBILE = 'featured_mobile';
    const IMG_BANNER_TABLET = 'featured_tablet';

    use ValidatingTrait;

	public $table = "settings";

    protected $rules = [
        'site_title'  => 'required',
        'site_email'  => 'required',
        // 'homepage_title' => 'required',
        // 'product_title' => 'required',
    ];

    protected $fillable = [
        'site_title', 'site_email',
        'site_contact_no', 'homepage_title',
        'homepage_sub_title', 'product_title', 'product_sub_title'
    ];

     protected $appends = [
        'featured_mobile_path',
        'featured_tablet_path',
    ];

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function getFeaturedMobileAttribute() // featured_mobile
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER_MOBILE ? true : false;
        })->first();

        return $uploads;
    }

    public function getFeaturedMobilePathAttribute() // featured_mobile_path
    {
        return $this->featured_mobile ? $this->featured_mobile->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getFeaturedTabletAttribute() // featured_tablet
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER_TABLET ? true : false;
        })->first();

        return $uploads;
    }

    public function getFeaturedTabletPathAttribute() // featured_tablet_path
    {
        return $this->featured_tablet ? $this->featured_tablet->path : Uploadable::IMG_PLACEHOLDER;
    }

    // Collection getter for Tablet sliders
	public function getTabletSlidersAttribute()
	{
		return $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_BANNER_TABLET ? true : false;
		});
    }

	// Collection getter for Mobile sliders
	public function getMobileSlidersAttribute()
	{
		return $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_BANNER_MOBILE ? true : false;
		});
    }

	public static function getSettings($columns = '*', $with = [])
	{
		return self::select($columns)->with($with)->first();
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Setting '{$this->site_title}'";
    }

    public function auditUpdate()
    {
        return "Setting '{$this->site_title}'";
    }

    public function auditDelete()
    {
        return "Setting '{$this->site_title}'";
    }
}
?>
