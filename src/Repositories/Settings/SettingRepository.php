<?php

namespace BC\Repositories\Settings;

use BC\Repositories\BaseRepository;

use BC\Repositories\Settings\Setting;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SettingRepository extends BaseRepository {

	protected $model;

	public function __construct(Setting $settings, UploadableRepository $uploadRepo)
	{
		$this->model = $settings;
		$this->uploadRepo = $uploadRepo;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function update(Request $request)
	{
		$this->validateData($request);
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$settings = isset($request->id) && $request->id != '' ? $this->model->find($request->id) : new Setting();
		$settings->fill($request->input());
		$settings->save();
		$settings->audit('update');

        if ( !empty($request['thumb']) ):
            $this->uploadRepo->uploadImageMultipleIntervention($settings, $request['thumb'], $request, 'image', [
                'get_new_name' => str_slug($request->site_title),
                'key' => Setting::IMG_BANNER_MOBILE,
                'field_key' => 'featured_mobile',
                'path' => 'uploads/setting',
                'filename' => 'featured_mobile',
				'width' => '400',
				'height' => '257',
            ], false);
        endif;

        if ( !empty($request['tablet']) ):
			$this->uploadRepo->uploadImageMultipleIntervention($settings, $request['tablet'], $request, 'image', [
				'get_new_name' => str_slug($request->site_title),
                'key' => Setting::IMG_BANNER_TABLET,
                'field_key' => 'featured_tablet',
                'path' => 'uploads/setting',
                'filename' => 'featured_tablet',
				'width' => '1280',
				'height' => '400',
			], false);
        endif;

		return $settings;
	}

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, ProductCategory $productCategory = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'site_title' => 'required',
                'site_email' => 'required',
                // 'homepage_title' => 'required',
                // 'product_title' => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

	public function cleanSettings($settings)
	{
		$settings->tablet_sliders = $this->encodedUploadables($settings->tablet_sliders);
		$settings->mobile_sliders = $this->encodedUploadables($settings->mobile_sliders);

		$settings = $this->cleanObject($settings);
		return $settings;
    }
}
