<?php

namespace BC\Repositories\Color;

use App\Model\BaseModel;
use BC\Repositories\Product\Product;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Color extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        'name'  => 'required',
        'value' => 'required',
    ];

    protected $fillable = [
        'name', 'value',
    ];

    public function products() {
        return $this->belongsToMany(Product::class,'product_variations','color_id','product_id');
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Color '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Color '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Color '{$this->name}'";
    }
}
?>

