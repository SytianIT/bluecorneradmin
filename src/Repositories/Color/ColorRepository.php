<?php

namespace BC\Repositories\Color;

use BC\Repositories\BaseRepository;

use BC\Repositories\Color\Color;
use BC\Repositories\ProductVariation\ProductVariation;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ColorRepository extends BaseRepository {

	protected $model;

	public function __construct(Color $color)
	{
		$this->model = $color;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(Color $color = null, Request $request)
	{
		$color = $this->model;
		$color->fill($request->input());
		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new ValidationException($this->errors);
		}
		$order = Color::max('order');
		$order = ($order == null) ? 1 : $order + 1;
		$color->order = $order;
		$color->orderable = $request->orderable ?: false;
		$color->save();
		$color->audit('create');
		return $color;
	}

	public function update(Color $color, Request $request)
	{
		$color->fill($request->input());
		$this->validateData($request);
		$color->orderable = $request->orderable ?: false;
		$color->save();
		$color->audit('update');

		$products = ProductVariation::where('color_id', $color->id)->get();
		foreach ($products as $product) {
			$product->sync_count = $product->sync_count + 1;
			$product->save();
		}

		return $color;
	}

	public function delete(Color $color)
	{
		$color->delete();
		$color->audit('delete');
		return $color;
	}

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Color $color = null)
    {
        $rules = [
            'name'   => 'required',
            'value'  => 'required',
        ];

        if( $color ) {
            if ($request->name == $color->name) {
                $rules['name'] = 'required';
            }
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
        }
    }
}
