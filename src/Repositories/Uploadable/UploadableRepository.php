<?php

namespace BC\Repositories\Uploadable;


use BC\Repositories\BaseRepository;
use BC\Repositories\Traits\Uploadable as UploadableTrait;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\File;

class UploadableRepository extends BaseRepository
{
    use UploadableTrait;

    protected $model;

    public function __construct(
        Uploadable $uploadable
    )
    {
        $this->model = $uploadable;
    }

    public function makeUploadFromUrl($model, $key, $path, $filename)
    {
        $upload = new Uploadable();
        $upload->key = $key;
        $upload->filename = $filename;
        $upload->uploadable_type = get_class($model);
        $upload->uploadable_id = $model->id;
        $upload->path = $path;
        $upload->save();

        return $upload;
    }

    public function createUpload($model, $request, $fileType, $options = [])
    {
        return $this->uploadImage($model, $request, $options, $fileType);
    }

    public function uploadImage($model, $request, $options, $fileType)
    {
        $messageBag = new MessageBag();

        $multiple = array_get($options, 'multiple');

        $key = array_get($options, 'key'); // Important part of option
        $fieldKey = array_get($options, 'field_key');
        $path = array_get($options, 'path');
        $filename = array_get($options, 'filename');
        $width = array_get($options, 'width');
        $height = array_get($options, 'height');

        if (!$key) {
            $this->errors = $messageBag->add('date', 'Key for upload image is not defined.')->merge($this->errors);
            return;
        }

        $modelUpload = null;

        if ($multiple) {
            if ($request->filled('upload_id')) {
                $modelUpload = $model->uploads()->find($request->upload_id);
            }
        } else {
            if ($model->uploads->count() > 0) {
                $modelUpload = $model->uploads->filter(function ($ups) use ($key) {
                    return $ups->key == $key ? true : false;
                })->first();
            }
        }

        $upload = $modelUpload ?: new Uploadable();

        $upload->key = $key;

        if ($upload->exists) {
            $file = $request->file($fieldKey ? $fieldKey : 'image') ?: $request->{$fieldKey};

            if ($file instanceof UploadedFile || $request->hasFile($fieldKey ? $fieldKey : 'file')) {
                $this->deleteUpload($upload);
            }
        }

        if ($fileType == 'image') {
            $file = $request->file($fieldKey ? $fieldKey : 'image') ?: $request->{$fieldKey};

            if ($file instanceof UploadedFile) {
                $data = $this->baseUpload($file, $path ?: 'uploads/etc', $width ?: NULL, $height ?: NULL, true);
                $upload->path = $data['path'];
                $upload->filename = $data['fileName'];
            }
        } else if ($fileType == 'file') {
            if ($request->hasFile($fieldKey ? $fieldKey : 'file')) {
                $data = $this->fileUpload($request->file($fieldKey ? $fieldKey : 'file'), $path ?: 'uploads/etc');
                $upload->path = $data['path'];
                $upload->filename = $data['fileName'];
            }
        }

        if (count($this->errors) > 0) {
            throw new \ATM\Validation\ValidationException($this->errors);
        }

        $upload->uploadable_type = get_class($model);
        $upload->uploadable_id = $model->id;
        $upload->order = $request->filled('order') ? $request->order : null;
        $upload->save();

        return $upload;
    }

    /**
     * @param $file - Can be filepath, base64 and etc. see image intervention documentation for reference http://image.intervention.io/api/make
     * @param $request
     * @param bool $saveAsUploadObject - either return as Uploadable object or uploaded path
     */
    public function uploadImageIntervention($model, $file, $request, $fileType, $options = [], $saveAsUploadObject = true)
    {

        $messageBag = new MessageBag();
        $image = Image::make($file);

        if ($image instanceof \Intervention\Image\Image) {

            /* felix added */
                $multiple = array_get($options, 'multiple');
                $key = array_get($options, 'key'); // Important part of option
                $fieldKey = array_get($options, 'field_key');
                $newName = array_get($options, 'get_new_name');
            /* end */

            /* default code here */
                $path = array_get($options, 'path');
                $filename = array_get($options, 'filename');
                $width = array_get($options, 'width');
                $height = array_get($options, 'height');
				$isColor = array_get($options, 'color');
            /* end */

            /* felix added */
                if (!$key) {
                    $this->errors = $messageBag->add('date', 'Key for upload image is not defined.')->merge($this->errors);
                    return;
                }

                $modelUpload = null;

                if ($multiple) {
                    if ($request->filled('upload_id')) {
                        $modelUpload = $model->uploads()->find($request->upload_id);
                    }
                } else {
                    // dd($model);
                    if ($model->uploads->count() > 0) {
                        $modelUpload = $model->uploads->filter(function ($ups) use ($key) {
                            return $ups->key == $key ? true : false;
                        })->first();
                    }
                }

                $upload = $modelUpload ?: new Uploadable();

                $upload->key = $key;

                if ($upload->exists) {

                    $file = $request->file($fieldKey ? $fieldKey : 'image') ?: $request->{$fieldKey};
                    if ($file instanceof UploadedFile || $request->hasFile($fieldKey ? $fieldKey : 'file')) {
                        $this->deleteUpload($upload);
                    }
                }

                if ($fileType == 'image') {

                    // $data = $this->interventionUploadImage($image, $path ?: 'uploads/etc', null, $width ?: NULL, $height ?: NULL, false, str_random(15)); DEFAULT CODE
                    $data = $this->interventionUploadImage($image, $path ?: 'uploads/etc', null, $width ?: NULL, $height ?: NULL, false, $newName.'-'.$key);
                    $upload->path = $data['path'];
                    $upload->filename = $data['fileName'];

                } else if ($fileType == 'file') {

                    // wala pa

                }

                if (count($this->errors) > 0) {
                    throw new \ATM\Validation\ValidationException($this->errors);
                }

                $upload->uploadable_type = get_class($model);
                $upload->uploadable_id = $model->id;
                $upload->sync_count = $upload->exists ? $upload->sync_count + 1 : 0;
                $upload->save();
            /* end */

            return $upload;

            // default code here
            //$data = $this->interventionUploadImage($image, $path ?: 'uploads/etc', null, $width ?: NULL, $height ?: NULL, false, str_random(15));
            //return $data;
        }

        return false;
    }

    public function deleteThisUploadable($uploadable)
    {
        $this->deleteUpload($uploadable);
    }

    /* felix for multiple uploads */
    public function uploadImageMultipleIntervention( $model, $file, $request, $fileType, $options = [], $saveAsUploadObject = true ) {

        $multiple = array_get($options, 'multiple');
        $key = array_get($options, 'key'); // Important part of option
        $fieldKey = array_get($options, 'field_key');
        $newName = array_get($options, 'get_new_name');

        $path = array_get($options, 'path');
        $filename = array_get($options, 'filename');
        $width = array_get($options, 'width');
        $height = array_get($options, 'height');

        $messageBag = new MessageBag();

        if (!$key) {
            $this->errors = $messageBag->add('date', 'Key for upload image is not defined.')->merge($this->errors);
            return;
        }

        if ( $file ) {

            $upload = $model;

            if ($fileType == 'image') {

                foreach ( $file as $count => $getImage) :
                    $image = Image::make($getImage['src']);
                    if ($image instanceof \Intervention\Image\Image) {
                        $upload = isset($getImage['upload_id']) && $getImage['upload_id'] != '' ? $model->uploads()->find($getImage['upload_id']) : new Uploadable();
                        $upload->key = $key;
                        $data = $this->interventionUploadImage($image, $path ?: 'uploads/etc', null, $width ?: NULL, $height ?: NULL, false, strtolower($newName).'-'.$count.'-'.$key);
                        $upload->path = $data['path'];
                        $upload->filename = $data['fileName'];
                        $upload->sync_count = $upload->exists ? $upload->sync_count + 1 : 0;
                        $upload->uploadable_type = get_class($model);
                        $upload->uploadable_id = $model->id;
                        $upload->save();
                    }
                endforeach;

            }

            if (count($this->errors) > 0) {
                throw new \ATM\Validation\ValidationException($this->errors);
            }

            return $upload;
        }
    }

    /* felix for removing file upload in the server */
    public function removeFileUploads($path, $type) {

        $messageBag = new MessageBag();
        $UploadType = array_get($type, 'type'); // Important part of option

        if (!$UploadType) {
            $this->errors = $messageBag->add('date', 'Type for upload image is not defined.')->merge($this->errors);
            return;
        }

        if (count($this->errors) > 0) {
            throw new \ATM\Validation\ValidationException($this->errors);
        }

        if ( $UploadType ):
            if ( basename($path) == basename(Uploadable::IMG_PLACEHOLDER) ): else: if(File::exists($path)) { File::delete($path); } endif;
        endif;
    }
}
