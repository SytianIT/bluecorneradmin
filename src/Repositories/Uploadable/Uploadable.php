<?php

namespace BC\Repositories\Uploadable;

use App\Model\BaseModel;
use BC\Repositories\Uploadable\UploadableRepository;
use Illuminate\Support\Facades\Log;

class Uploadable extends BaseModel
{
    protected $table = 'uploadables';

    const IMG_PLACEHOLDER = 'images/placeholder.png';

    public function uploadable()
    {
        return $this->morphTo();
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Image '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Image '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Image '{$this->name}'";
    }
}
