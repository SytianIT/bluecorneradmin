<?php

namespace BC\Repositories\Traits;

use BC\Repositories\Uploadable\Uploadable as UploadableModel;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Uploadable
{
    /**
     * Function to call to upload basic image with parameters of optimizing image
     * @param  UploadedFile|null $image
     * @param  String            $path
     * @param  Int            $width
     * @param  Int            $height
     * @return String
     * $employee->image = $this->baseUpload($request->file('image'), 'uploads/employees/profile', 450, 450);
     */
    public function baseUpload(UploadedFile $image, $path, $width = null, $height = null, $saveOriginal = true, $filename = null)
    {
        $messageBag = new MessageBag();

        if ($image && $image->isValid()) {
            $validator = Validator::make(
                ['image' => $image],
                ['image' => 'required|image|max:200000']
            );

            if ($validator->fails()) {
                $this->errors = $messageBag->add('thumb', 'Provide a valid image')->merge($this->errors);

                return null;
            }

            $img = Image::make($image->getRealPath());

            // Create first directory if not exists
            if(!File::exists(public_path($path))){
                File::makeDirectory($path, 0775);
            }

            // Save first the original image
            if($saveOriginal){
                $originalFileName = $this->makeFileNameWithCheckExistence($image, $path, false, 0, null, null, $filename);
                $uploadPath = public_path($path.'/'.$originalFileName);
                $img->save($uploadPath, 100);
            }

            // Save with different option of crop and rotate
            $fileName = $this->makeFileNameWithCheckExistence($image, $path, false, 0, $width, $height, $filename);
            $uploadPath = public_path($path.'/'.$fileName);
            if($width && $height) {
                $img->fit($width, $height)->save($uploadPath, 100);
            } else if ($width) {
                $img->resize($width, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadPath, 100);
            } else if ($height) {
                $img->resize(null, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($uploadPath, 100);
            } else {
                $img->save($uploadPath, 100);
            }

            $path = $path.'/'.$fileName;
            return compact('path', 'fileName');
        }

        return null;
    }

    public function interventionUploadImage($img, $path, $image = null, $width = null, $height = null, $saveOriginal = true, $filename = null)
    {
        // Create first directory if not exists
        if(!File::exists(public_path($path))){
            File::makeDirectory($path, 0775);
        }

        // Save first the original image
        if($image && $saveOriginal){
            $originalFileName = $this->makeFileNameWithCheckExistence($image, $path, false, 0, null, null, $filename);
            $uploadPath = public_path($path.'/'.$originalFileName);
            $img->save($uploadPath, 100);
        }

        // Use for not provided UploadedFile
        $mime = $img->mime();
        if ($mime == 'image/jpeg')
            $extension = '.jpg';
        elseif ($mime == 'image/png')
            $extension = '.png';
        elseif ($mime == 'image/gif')
            $extension = '.gif';
        else
            $extension = '';

        // Save with different option of crop and rotate
        $fileName = ($image && $saveOriginal) ? $this->makeFileNameWithCheckExistence($image, $path, false, 0, $width, $height, $filename) : ($filename . $extension);
        $uploadPath = public_path($path.'/'.$fileName);
        if($width && $height) {
            $img->fit($width, $height)->save($uploadPath, 100);
        } else if ($width) {
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($uploadPath, 100);
        } else if ($height) {
            $img->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->save($uploadPath, 100);
        } else {
            $img->save($uploadPath, 100);
        }

        $path = $path.'/'.$fileName;

        return compact('path', 'fileName');
    }

    public function fileUpload(UploadedFile $file, $path)
    {
        $messageBag = new MessageBag();

        if ($file && $file->isValid()) {
            $validator = Validator::make(
                ['file' => $file],
                ['file' => 'required|mimes:docx,pdf,ppt,pptx,doc,word']
            );

            if ($validator->fails()) {
                $this->errors = $messageBag->add('file', 'Provide a valid file')->merge($this->errors);

                return null;
            }

            // Create first directory if not exists
            if(!File::exists(public_path($path))){
                File::makeDirectory($path, 0775);
            }

            $fileName = $this->makeFileNameWithCheckExistence($file, $path, false, 0, null, null);
            $uploadPath = public_path($path.'/'.$fileName);
            $file->move(public_path($path), $fileName);

            $path = $path.'/'.$fileName;
            return compact('path', 'fileName');
        }
    }

    /**
     * Method use to check the file name existence in path provided, if exist will add incremental number after the name specified
     * @param  Image  $image
     * @param  String  $path
     * @param  boolean $repeat
     * @param  integer $counter
     * @param  integer  $width
     * @param  integer  $height
     * @return String
     */
    public function makeFileNameWithCheckExistence($image, $path, $repeat = false, $counter = 0, $width, $height, $filename = null)
    {
        $originalName = $filename ?: $image->getClientOriginalName();
        $fileExtension = '.'.$image->getClientOriginalExtension();

        $slugName = str_slug(substr($originalName, 0, strpos($originalName, $fileExtension)), '-');
        $rawName = $repeat ? $slugName.'-'.$counter : $slugName;
        if($width && $height){
            $rawName .= '-'.$width.'x'.$height;
        } else if($width){
            $rawName .= '-w_'.$width;
        } else if($height){
            $rawName .= '-h_'.$height;
        }

        $demandPath = public_path($path.'/'.$rawName.$fileExtension);
        $fileName = $rawName.$fileExtension;
        if(File::exists($demandPath)){
            $fileName = $this->makeFileNameWithCheckExistence($image, $path, true, $counter + 1, $width, $height);
        }

        return $fileName;
    }

    /**
     * Upload method intended for file and image upload
     * @param UploadedFile $file
     * @param $path
     * @param array $imageOpt
     * @return String
     */
    public function uploadAttachment(UploadedFile $file, $path, $imageOpt = [])
    {
        if ($file && $file->isValid()) {
            $validator = Validator::make(
                ['file' => $file],
                ['file' => 'mimes:jpeg,jpg,png,doc,docx,ppt,pptx,pdf|max:20000']
            );

            if ($validator->fails()) {
                return null;
            }

            $fileName = $this->makeFileNameWithCheckExistence($file, $path, false, 0);

            $file->move($path, $fileName);

            return $path.'/'.$fileName;
        }

        return null;
    }

    public function deleteUpload(UploadableModel $uploadable)
    {
        $paths = explode('/', $uploadable->path);
        array_pop($paths);
        $path = public_path(implode('/', $paths));

        $filename = $uploadable->filename;
        $fileType = array_last(explode('.', $filename));
        $exploded = explode('-', $filename);
        array_pop($exploded);
        $originalName = implode('-', $exploded);

        $originalPath = $path . '/' . $originalName . '.' . $fileType;
        if(File::exists($originalPath)){
            File::delete($originalPath);
        }

        $modifiedPath = public_path($uploadable->path);
        if(File::exists($modifiedPath)){
            File::delete($modifiedPath);
        }

        return true;
    }

    public function resizeImage($image, $width = null, $height = null)
    {
        if ($width && $height) {
            return $image->resize($width, $height);
        } else if ($width) {
            return $image->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        } else if ($height) {
            return $image->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        return $image;
    }
}