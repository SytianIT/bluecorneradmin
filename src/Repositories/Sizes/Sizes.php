<?php

namespace BC\Repositories\Sizes;

use App\Model\BaseModel;
use BC\Repositories\Product\Product;
use BC\Repositories\SizesCategory\SizesCategory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Sizes extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        // 'name'   => 'required|unique',
        'name'   => 'required',
        'width'  => 'required',
        'length' => 'required',
    ];

    protected $fillable = [
        'name','description','width','length'
    ];

    public function products() {
        return $this->belongsToMany(Product::class, 'product_sizes', 'size_id', 'product_id');
    }

    public function sizeCategory() {
        return $this->belongsTo(SizesCategory::class, 'size_cat_id');
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Sizes '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Sizes '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Sizes '{$this->name}'";
    }
}
?>
