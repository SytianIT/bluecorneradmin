<?php

namespace BC\Repositories\Sizes;

use BC\Repositories\BaseRepository;

use BC\Repositories\Sizes\Sizes;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SizesRepository extends BaseRepository {

	protected $model;

	public function __construct(Sizes $sizes)
	{
		$this->model = $sizes;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(Sizes $sizes = null, Request $request)
	{
		$sizes = $this->model;
		$sizes->fill($request->input());
		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new ValidationException($this->errors);
		}
		$order = Sizes::max('order');
		$order = ($order == null) ? 1 : $order + 1;
        $sizes->order = $order;
        $sizes->size_cat_id = $request->sizescat['value'];
		$sizes->save();
		$sizes->audit('create');
		return $sizes;
	}

	public function update(Sizes $sizes, Request $request)
	{
		$sizes->fill($request->input());
        $this->validateData($request);
        $sizes->size_cat_id = $request->sizescat['value'];
		$sizes->save();
		$sizes->audit('update');
		return $sizes;
    }

    public function updateOrder(Sizes $sizes, Request $request) {
        if( $request ) {
            $orderedSizes = $request->orderedSizes;
            foreach( $orderedSizes as $item ) {
                $sizes = isset($item['id']) ? $this->model->find($item['id']) : new Sizes();
                $sizes->order = $item['order'];
                $sizes->save();
            }
        }
    }

	public function delete(Sizes $sizes)
	{
		$sizes->delete();
		$sizes->audit('delete');
		return $sizes;
	}

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Sizes $sizes = null)
    {
        $rules = [
            // 'name'   => 'required|unique:sizes,name',
            'name'   => 'required',
            'width'  => 'required',
            'length' => 'required',
        ];

        if( $sizes ) {
            if ($request->name == $sizes->name) {
                $rules['name'] = 'required';
            }
        }

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $this->errors = $validator->errors();
        }
    }
}
