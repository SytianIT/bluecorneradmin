<?php

namespace BC\Repositories\SizesCategory;

use BC\Repositories\BaseRepository;

use BC\Repositories\SizesCategory\SizesCategory;
use BC\Repositories\Sizes\Sizes;
use BC\Repositories\Sizes\SizesRepository;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Intervention\Image\Size;

class SizesCategoryRepository extends BaseRepository {

	protected $model;

	public function __construct(SizesCategory $sizesCategory, SizesRepository $sizesRepo)
	{
		$this->model = $sizesCategory;
		$this->sizesRepo = $sizesRepo;
	}

	public function search(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = $this->model->with($with);

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('featured')) {
			$query = $query->where('featured', 1);
		}

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(SizesCategory $sizesCategory = null, Request $request)
	{
		$this->validateData($request);
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}
		$sizesCategory = $this->model;
		$sizesCategory->fill($request->input());
		$sizesCategory->save();

        return $sizesCategory;
	}

	public function update(Request $request)
	{
        $size = new Sizes();
		$this->validateData($request);
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
        }
        $this->sizesRepo->updateOrder($size, $request);
		$sizesCategory = $this->model->find($request->id);
		$sizesCategory->fill($request->input());
		$sizesCategory->save();

		return $sizesCategory;
	}

	public function delete(SizesCategory $sizecatid)
    {
        $sizecatid->delete();
        return $sizecatid->name;
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, ProductCategory $productCategory = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'name' => 'required'
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    /**
	 * Methods to clean object to return for APIE
	 */
	public function cleanCategory($collection)
	{
		foreach ($collection as $item) {
			$ftImage = $this->encodedUploadables($item->img_logo);
			$item->featured_image = $ftImage;

			if ($item->childs->count() > 0) {
				$item->child_categories = $this->cleanCategory($item->childs);
			}
		}

		return $collection;
    }
}
