<?php

namespace BC\Repositories\SizesCategory;

use App\Model\BaseModel;
use BC\Repositories\Product\Product;
use BC\Repositories\Sizes\Sizes;
use Watson\Validating\ValidatingTrait;

class SizesCategory extends BaseModel
{

	use ValidatingTrait;

	protected $rules = [
		'name'  => 'required',
	];

	protected $fillable = [
		'name', 'description'
	];

	/**
	 * Get the products that owns the categories.
	 */
	public function size()
	{
		return $this->hasMany(Sizes::class,'size_cat_id');
    }

    public function products()
	{
		return $this->hasMany(Products::class,'size_cat_id');
	}

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Sizes Category '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Sizes Category '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Sizes Category '{$this->name}'";
    }
}
?>
