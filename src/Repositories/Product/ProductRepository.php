<?php

namespace BC\Repositories\Product;

use BC\Repositories\BaseRepository;

use BC\Repositories\Color\Color;
use BC\Repositories\ProductVariation\ProductVariation;
use BC\Repositories\ProductVariation\ProductVariationRepository;
use BC\Repositories\Product\Product;
use BC\Repositories\Sizes\Sizes;
use BC\Repositories\Style\Style;
use BC\Repositories\Style\StyleRepository;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductRepository extends BaseRepository {

	protected $model;

	public function __construct(Product $product, Style $style, UploadableRepository $uploadRepo, StyleRepository $styleRepo, ProductVariation $productVar, ProductVariationRepository $productVarRepo)
	{
		$this->model = $product;
		$this->style = $style;
		$this->uploadRepo = $uploadRepo;
		$this->productVar = $productVar;
		$this->styleRepo   = $styleRepo;
		$this->productVarRepo = $productVarRepo;
	}

	/**
	 * Method for general fetching of products
	 * @param Request $request
	 * @param array $with
	 * @param bool $paginate
	 */
	public function search(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = $this->model->with($with)->orderBy('id');

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('category_id')) {
			$query = $query->where('category_id', $request->category_id);
		}

		if ($request->filled('featured')) {
			$query = $query->where('featured', 1);
		}

		if ($request->filled('product_id')) {
			$query = $query->where('id', $request->product_id);
		}

        if ($request->filled('api_fetch')) {
            $query = $query->withTrashed();
        }

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

	/**
	 * Method for general fetching of productVariations
	 * @param Request $request
	 * @param array $with
	 * @param bool $paginate
	 */
	public function searchProductVariation(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = ProductVariation::with($with)->orderBy('id');

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('product_id')) {
			$query = $query->where('product_id', $request->product_id);
		}

		if ($request->filled('api_fetch')) {
			$query = $query->withTrashed();
		}

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
		    // dd($query->skip(110)->limit(110)->get()->first());
			//return $query->skip(660)->limit(110)->get();
			return $query->get();
		}
	}

	/**
	 * Method for general fetching of product sizes
	 * @param Request $request
	 * @param array $with
	 * @param bool $paginate
	 */
	public function searchProductSizes(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = Sizes::with($with)->orderBy('id');

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('product_id')) {
			$query = $query->where('product_id', $request->product_id);
		}

		if ($request->filled('api_fetch')) {
			$query = $query->withTrashed();
		}

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			//return $query->skip(500)->limit(200)->get();
			return $query->get();
		}
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(Product $product = null, Request $request )
	{
		$product = $this->model;
		$product->fill($request->input());
		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$product->has_styles = $request->has_styles ? 1 : 0;
		$product->status =  $request->status ? 1 : 0;
		$product->size_cat_id =  $request->sizeCat['value'];
		$product->save();
        $product->audit('create');
        $sizes = array_values(array_sort($request->size_ids, function($val){
            return $val['order'];
        }));
        $product->sizes()->sync($sizes);

		if( $request->has_styles == null ) {
			$colorPanel = $request->colorPanel;
			$this->productVarRepo->store($colorPanel, $product, $request->has_styles, $request);
		} elseif( $request->has_styles != null ) {
			$this->styleRepo->store( $request->styles, $product, $request );
		}

		if ( !empty($request['product_featured_image_info'][0]['url']) ):
            $this->uploadRepo->uploadImageIntervention($product, $request['product_featured_image_info'][0]['url'], $request, 'image', [
                'get_new_name' => str_slug($request->name.'-'.$request['product_featured_image_info'][0]['name']),
                'key' => Product::IMG_PRODUCT_FEATURED_IMAGE,
                'field_key' => 'product_featured_image',
                'path' => 'uploads/product/featured-image',
                'filename' => 'product_featured_image',
				'width' => '454',
				'height' => '384',
            ], false);
		endif;

		if ( !empty($request['product_mobile_banner_info'][0]['url']) ) :
			$this->uploadRepo->uploadImageIntervention($product, $request['product_mobile_banner_info'][0]['url'], $request, 'image', [
				'get_new_name' => str_slug($request->name.'-'.$request['product_mobile_banner_info'][0]['name']),
                'key' => Product::IMG_PRODUCT_MOBILE_BANNER_IMAGE,
                'field_key' => 'product_mobile_banner_image',
                'path' => 'uploads/product/banner/mobile',
				'filename' => 'product_mobile_banner_image',
				'width' => '375',
				'height' => '209',
			], false);
		endif;

		/*if ( !empty($request['product_tablet_banner_info'][0]['url']) ) :
			$this->uploadRepo->uploadImageIntervention($product, $request['product_tablet_banner_info'][0]['url'], $request, 'image', [
				'get_new_name' => str_slug($request->name.'-'.$request['product_tablet_banner_info'][0]['name']),
                'key' => Product::IMG_PRODUCT_TABLET_BANNER_IMAGE,
                'field_key' => 'product_tablet_banner_image',
                'path' => 'uploads/product/banner/tablet',
				'filename' => 'product_tablet_banner_image',
			], false);
		endif;*/

		return $product;
	}

	public function update(Product $product, Request $request)
	{
		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
        }

        $sizes = array_values(array_sort($request->size_ids, function($val){
            return $val['order'];
        }));

		$product->sizes()->sync($sizes);
		$product->fill($request->input());
		$product->has_styles = $request->has_styles ? 1 : 0;
		$product->sync_count = $product->sync_count + 1;
		$product->status =  $request->status ? 1 : 0;
		if( $request->has_styles == null ) {
			$colorPanel = $request->colorPanel;
			$this->productVarRepo->update($colorPanel, $product, $request->has_styles, $request);
			$product->styles()->delete();
		} elseif( $request->has_styles != null ) {
			$this->styleRepo->update( $request->styles, $product, $request );
			$toDelete = $product->productVariations()->where('style_id', NULL)->pluck('id')->toArray();
			$toDelete = collect($toDelete);
			$toDelete->each(function($item, $key) {
				ProductVariation::where('id', $item)->get()->each(function($prodVar) {
					$productvariation_image_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_image_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_image_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);

					$productvariation_color_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_color_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_color_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);
					$prodVar->delete();
				});
			});
		}
		$product->save();
		$product->audit('update');

		if ( !empty($request['product_featured_image_info'][0]['url']) ) {
			$name = substr($request['product_featured_image_info'][0]['old_name'], 0, strrpos($request['product_featured_image_info'][0]['old_name'], "."));
			$filename = ( $request['product_featured_image_info'][0]['name'] == '' ) ? str_replace(array('product_featured_image'), '',$name) : $request->name.'-'.$request['product_featured_image_info'][0]['name'];
			if( !in_array($request['product_featured_image_info'][0]['name'], ['', null]) ) {
				$product_featured_image = isset($product) && $product->exists ? public_path($product->product_featured_image_path) : false;
				$this->uploadRepo->removeFileUploads($product_featured_image, [
					'type' => 'SINGLE_REMOVE_UPLOAD',
				]);
			}
			$this->uploadRepo->uploadImageIntervention($product, $request['product_featured_image_info'][0]['url'], $request, 'image', [
				'get_new_name' => str_slug($filename),
                'key' => Product::IMG_PRODUCT_FEATURED_IMAGE,
                'field_key' => 'product_featured_image',
                'path' => 'uploads/product/featured-image',
				'filename' => 'product_featured_image',
				'width' => '454',
				'height' => '384',
			], false);
		} else {
			$product_featured_image = isset($product) && $product->exists ? public_path($product->product_featured_image_path) : false;
			$this->uploadRepo->removeFileUploads($product_featured_image, [
				'type' => 'SINGLE_REMOVE_UPLOAD',
			]);
			$product->uploads()->where('id', '=', $request['product_featured_image_info'][0]['id'])->delete();
		}

		if ( !empty($request['product_mobile_banner_info'][0]['url']) ) {
			$name = substr($request['product_mobile_banner_info'][0]['old_name'], 0, strrpos($request['product_mobile_banner_info'][0]['old_name'], "."));
			$filename = ( $request['product_mobile_banner_info'][0]['name'] == '' ) ? str_replace(array('product_mobile_banner_image'), '',$name) : $request->name.'-'.$request['product_mobile_banner_info'][0]['name'];
			if( !in_array($request['product_mobile_banner_info'][0]['name'], ['', null]) ) {
				$product_mobile_banner_image = isset($product) && $product->exists ? public_path($product->product_mobile_banner_image_path) : false;
				$this->uploadRepo->removeFileUploads($product_mobile_banner_image, [
					'type' => 'SINGLE_REMOVE_UPLOAD',
				]);
			}
			$this->uploadRepo->uploadImageIntervention($product, $request['product_mobile_banner_info'][0]['url'], $request, 'image', [
				'get_new_name' => str_slug($filename),
                'key' => Product::IMG_PRODUCT_MOBILE_BANNER_IMAGE,
                'field_key' => 'product_mobile_banner_image',
                'path' => 'uploads/product/banner/mobile',
				'filename' => 'product_mobile_banner_image',
				'width' => '375',
				'height' => '209',
			], false);
		} else {
			$product_mobile_banner_image = isset($product) && $product->exists ? public_path($product->product_mobile_banner_image_path) : false;
			$this->uploadRepo->removeFileUploads($product_mobile_banner_image, [
				'type' => 'SINGLE_REMOVE_UPLOAD',
			]);
			$product->uploads()->where('id', '=', $request['product_mobile_banner_info'][0]['id'])->delete();
		}

		/*if ( !empty($request['product_tablet_banner_info'][0]['url']) ) {
			$name = substr($request['product_tablet_banner_info'][0]['old_name'], 0, strrpos($request['product_tablet_banner_info'][0]['old_name'], "."));
			$filename = ( $request['product_tablet_banner_info'][0]['name'] == '' ) ? str_replace(array('product_tablet_banner_image'), '',$name) : $request->name.'-'.$request['product_tablet_banner_info'][0]['name'];
			if( !in_array($request['product_tablet_banner_info'][0]['name'], ['', null]) ) {
				$product_tablet_banner_image = isset($product) && $product->exists ? public_path($product->product_tablet_banner_image_path) : false;
				$this->uploadRepo->removeFileUploads($product_tablet_banner_image, [
					'type' => 'SINGLE_REMOVE_UPLOAD',
				]);
			}
			$this->uploadRepo->uploadImageIntervention($product, $request['product_tablet_banner_info'][0]['url'], $request, 'image', [
				'get_new_name' => str_slug($filename),
                'key' => Product::IMG_PRODUCT_TABLET_BANNER_IMAGE,
                'field_key' => 'product_tablet_banner_image',
                'path' => 'uploads/product/banner/tablet',
				'filename' => 'product_tablet_banner_image',
			], false);
		} else {
			$product_tablet_banner_image = isset($product) && $product->exists ? public_path($product->product_tablet_banner_image_path) : false;
			$this->uploadRepo->removeFileUploads($product_tablet_banner_image, [
				'type' => 'SINGLE_REMOVE_UPLOAD',
			]);
			$product->uploads()->where('id', '=', $request['product_tablet_banner_info'][0]['id'])->delete();
		}*/

		return $product;
	}

	public function delete(Request $request)
    {
		$product_id = $request->id;
		$product = $this->model->find($product_id);

        $product->sync_count = $product->sync_count + 1;
		$styleToBeDeleted = $product->styles()->pluck('id')->toArray();
		foreach( $styleToBeDeleted as $value ) {
			Style::where('id', $value)->get()->each(function($style) {
				$style->delete();
			});
		}

		$product->sizes()->detach();
		$product->colors()->detach();

		$product_featured_image = isset($product) && $product->exists ? public_path($product->product_featured_image_path) : false;
        $this->uploadRepo->removeFileUploads($product_featured_image, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
		]);

		$product_mobile_banner_image = isset($product) && $product->exists ? public_path($product->product_mobile_banner_image_path) : false;
        $this->uploadRepo->removeFileUploads($product_mobile_banner_image, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
		]);

		$product_tablet_banner_image = isset($product) && $product->exists ? public_path($product->product_tablet_banner_image_path) : false;
        $this->uploadRepo->removeFileUploads($product_tablet_banner_image, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $product->save();
        $product->delete();
		$product->audit('delete');
        return $product->name;
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Product $product = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'name' => 'required',
                'category_id' => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

	/**
	 * Methods to clean object to return for APIE
	 */
	public function cleanProduct($collection, $pathOnly = false)
	{
		foreach ($collection as $item) {
			$item->featured_image = $this->encodedUploadables($item->product_featured_image, $pathOnly);
			$item->tablet_banner = $this->encodedUploadables($item->product_tablet_banner_image, $pathOnly);
			$item->mobile_banner = $this->encodedUploadables($item->product_mobile_banner_image, $pathOnly);

			/*if ($item->productVariations->count() > 0) {
				$item->grouped_product_variations = $this->cleanVariation($item->productVariations);
			}*/
		}

		return $collection;
	}

	/**
	 * Method to clean product variation to return for APIE
	 */
	public function cleanVariation($collection, $pathOnly = false)
	{
		foreach ($collection as $item) {
			$item->variation_image = $this->encodedUploadables($item->product_variation_image, $pathOnly);
			$item->color_image = $this->encodedUploadables($item->product_variation_color, $pathOnly);
		}

		return $collection; //->groupBy('style_id')
	}
}
