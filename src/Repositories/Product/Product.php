<?php

namespace BC\Repositories\Product;

use App\Model\BaseModel;
use BC\Repositories\Color\Color;
use BC\Repositories\ProductCategory\ProductCategory;
use BC\Repositories\ProductVariation\ProductVariation;
use BC\Repositories\SizesCategory\SizesCategory;
use BC\Repositories\Sizes\Sizes;
use BC\Repositories\Style\Style;
use BC\Repositories\Traits\Nestable;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Watson\Validating\ValidatingTrait;

class Product extends BaseModel
{

    const IMG_PRODUCT_FEATURED_IMAGE = 'product_featured_image';
    const IMG_PRODUCT_MOBILE_BANNER_IMAGE = 'product_mobile_banner_image';
    const IMG_PRODUCT_TABLET_BANNER_IMAGE = 'product_tablet_banner_image';

    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        'name'        => 'required',
        'category_id' => 'required',
    ];

    protected $fillable = [
        'category_id', 'name', 'description', 'has_styles'
    ];

    protected $appends = [
        'product_featured_image_path',
        'product_mobile_banner_image_path',
        'product_tablet_banner_image_path',
    ];

    // Relational Methods

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function category() {
        return $this->belongsTo(ProductCategory::class);
    }

    public function colors() {
        return $this->belongsToMany(Color::class,'product_variations','product_id','color_id');
    }

    public function productVariations() {
        return $this->hasMany(ProductVariation::class, 'product_id');
    }

    public function sizes() {
        return $this->belongsToMany(Sizes::class,'product_sizes','product_id','size_id');
    }

    public function sizeCategory() {
        return $this->belongsTo(SizesCategory::class, 'size_cat_id');
    }

    public function styles() {
        return $this->hasMany(Style::class)->orderBy('order','asc');
    }

    /*
     * Helper Methods
     */

    public function isActive()
    {
        return $this->active ? true : false;
    }

    /*
     * Mutators
     */

    public function getProductFeaturedImageAttribute() // product_featured_image **JR COMMER: not necessary yung Product sa prefix, kasi belong naman talaga to sa model na product
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_PRODUCT_FEATURED_IMAGE ? true : false;
        })->first();

        return $uploads;
    }

    public function getProductFeaturedImagePathAttribute() // product_featured_image_path
    {
        return $this->product_featured_image ? $this->product_featured_image->path : '';
    }

    public function getProductMobileBannerImageAttribute() // product_mobile_image
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_PRODUCT_MOBILE_BANNER_IMAGE ? true : false;
        })->first();

        return $uploads;
    }

    public function getProductMobileBannerImagePathAttribute() // product_mobile_banner_image_path
    {
        return $this->product_mobile_banner_image ? $this->product_mobile_banner_image->path : '';
    }

    public function getProductTabletBannerImageAttribute() // product_mobile_image
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_PRODUCT_TABLET_BANNER_IMAGE ? true : false;
        })->first();

        return $uploads;
    }

    public function getProductTabletBannerImagePathAttribute() // product_mobile_banner_image_path
    {
        return $this->product_tablet_banner_image ? $this->product_tablet_banner_image->path : '';
    }

    /*
     * Static Methods
     */

    public static function boot() {
        parent::boot();
        static::deleting(function($product) {
            $product->styles()->delete();
        });
    }

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Product '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Product '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Product '{$this->name}'";
    }
}
?>
