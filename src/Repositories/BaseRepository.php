<?php

namespace BC\Repositories;

use BC\Validation\ValidationException;
use BC\Validation\ValidationInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

abstract class BaseRepository implements ValidationInterface {

	const PER_PAGE = 50;
	const DISPLAY_ALL = 'all';

	/**
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * @var \Illuminate\Support\MessageBag|array
	 */
	protected $errors = [];

	/**
	 * @var boolean	Whether validation exception will be thrown
	 */
	protected $throwValidationException = true;

	public function with($with = array())
	{
		return $this->model->with($with);
	}

	public function find($id, $with = array())
	{
		return $this->model->with($with)->find($id) ?: $this->abort();
	}

	public function all($with = array())
	{
		return $this->model->with($with)->get();
	}

	public function getAllBy($key, $value, $with = array())
	{
		$collection = $this->model->with($with)->where($key, $value)->get();

		return $collection;
	}

	public function getFirstBy($key, $value, $with = array())
	{
		return $this->model->with($with)->where($key, $value)->first();
	}

	/**
	 * paginated results
	 *
	 * @param int $page
	 * @param int $limit
	 * @return StdClass
	 */
	public function getByPage($page = 1, $limit = 10)
	{
	  $results = new \StdClass;
	  $results->page = $page;
	  $results->limit = $limit;
	  $results->totalItems = 0;
	  $results->items = array();

	  $items = $this->model->skip($limit * ($page - 1))
	  				->take($limit)
	  				->get();

	  $results->totalItems = $this->model->count();
	  $results->items = $items->all();

	  return $results;
	}

	public function paginateCollection($results, $currentPage, $perPage, $opt = null)
    {
        $temp = $results;
        $currentIndex = (($currentPage == 1 || $currentPage == 0) ? 0 : $currentPage - 1) * $perPage;
        $slice = $temp->slice($currentIndex, $perPage);

        $opt = ($opt == null) ? ['path' => ''] : $opt;

        return new LengthAwarePaginator($slice, count($results), $perPage, $currentPage, $opt);
    }

	public function validate(Model $model, $ruleset = 'saving')
	{
		if (! $model->isValid($ruleset)) {
			if ( ! $this->throwValidationException) {
				$this->errors = $model->getErrors();
				return false;
			}
			throw new ValidationException($model->getErrors());
		}

		return true;
	}

	public function validationPasses(Model $model)
	{
		if ( ! $model->isValid())
		{
			if ( ! $this->throwValidationException) {
				$this->errors = $model->getErrors();
				return false;
			}
			throw new ValidationException($model->getErrors());
		}

		return true;
	}

	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * Abort excution, e.g. a model not found
	 */
	protected function abort()
	{
		abort(404);
	}

	public function throwValidationException(array $errors = array())
	{
		$errors = new \Illuminate\Support\MessageBag($errors);

		throw new \BC\Validation\ValidationException($errors);
	}

	public function relQuery($query, $relation, $column, $value, $operator = null) {

		$query = $query->whereHas($relation, function($q) use($column, $value, $operator) {
			if( $operator ) {
				$q->where($column, $operator, $value);
			}
			else {
				$q->where($column, $value);
			}
		});

		return $query;

	}

	public function addNewLog($action, $ip, $username)
	{
		$this->model->addNewLog($action, $ip, $username);
	}

	/**
	 * Return the associated model
	 *
	 * @return Illuminate\Database\Eloquent\Model
	 */
	public function getModel()
	{
		return $this->model;
	}

    /**
     * Use to sort multidimensional arrays;
     */
    public function sortingArrays($array, $keys)
    {
        return array_multisort(
            array_column($array, 'no_combined_enabled'),  SORT_ASC,
            array_column($array, 'name'), SORT_ASC,
            $array
        );
	}

	/**
	 * For cleaning object to return in APIE
	 */
	public function cleanObject($object)
	{
		unset($object['id']);
		return $object;
	}

	public function encodedUploadables($collection, $pathOnly = false)
	{
		if ($collection instanceof Collection) {
			return $collection->map(function($slider) use ($pathOnly) {
				if ($pathOnly) {
					if (file_exists($slider->path)) {
						$imageInv = Image::make(asset($slider->path));
						if ($imageInv) {
							$base64 = $imageInv->encode('data-url')->encoded;
							//$base64 = str_replace("data:image/jpeg;base64,", "", $base64);
							$slider->encoded = $base64;
						} else {
							$slider->encoded = asset($slider->path);
						}
					}
				} else {
					$slider->encoded = asset($slider->path);
			}

				return $slider;
			})->toArray(); //->pluck('encoded')
		} else if ($collection) {
			if ($pathOnly) {
				if (file_exists($collection->path)) {
					$imageInv = Image::make(asset($collection->path));
					if ($imageInv) {
						$base64 = $imageInv->encode('data-url')->encoded;
						//$base64 = str_replace("data:image/jpeg;base64,", "", $base64);
						$collection->encoded = $base64;
					} else {
						$collection->encoded = asset($collection->path);
					}
				}
			} else {
				$collection->encoded = asset($collection->path);
			}

			return $collection; //->encoded
		}
	}
}
