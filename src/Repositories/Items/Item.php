<?php

namespace BC\Repositories\Items;

use App\Model\BaseModel;
use BC\Repositories\Contest\Contest;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Item extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

    protected $table = "contest_items";

    protected $fillable = [
        'category_id', 'type', 'question',
    ];

    protected $casts = [
        'choices' => 'array'
    ];

    protected $rules = [];

    public function contest() {
        return $this->belongsTo(Contest::class);
    }
    
    // Helper methods
	public function getType()
	{
		$type = 'n/a';
		if (in_array($this->type, ['text', 'text_field'])) {
			$type = 'text';
		} else {
			$type = $this->type;
		}

		return $type;
	}
}

?>
