<?php

namespace BC\Repositories\Items;

use BC\Repositories\BaseRepository;

use BC\Repositories\Items\Item;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

class ItemRepository extends BaseRepository {

	protected $model;

	public function __construct(Item $item)
	{
		$this->model = $item;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{

		if ($request['data']) {
			$items = $this->model;
			$allArrays = [];
			$now = Carbon::now()->toDateTimeString();
			foreach ($request['data'] as $key => $args ):
				$draw = [
                    'contest_id' => $args['contest_id'],
                    'type' => $args['type'],
                    'question' => $args['question'],
                    'choices' => json_encode($args['choices']),
					'order' => $args['sub_key'],
					'created_at' => $now,
					'updated_at' => $now,
                ];
				$allArrays[] = $draw;
			endforeach;
			Item::insert( $allArrays );
			return $items;
		}
	}

	public function update(Request $request)
	{
		// dd( $request );
		if ( $request['data'] ) {
			foreach ($request['data'] as $key => $args ):
				$items = isset($args['id']) && $args['id'] != '' ? $this->model->find($args['id']) : new Item();
				$items->contest_id = $args['contest_id'];
                $items->type = $args['type'];
                $items->question = $args['question'];
                $items->choices = $args['choices'];
                // $items->order = $args['sub_key'];
                $items->order = $key;
				$items->save();
				$key++;
			endforeach;
			return $items;
		}
	}

	public function delete(Request $request, $itemId)
    {
        $item = $this->model->find($itemId);
        $item->delete();
        return $item->name;
    }
}
