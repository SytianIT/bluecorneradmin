<?php

namespace BC\Repositories\ProductVariation;

use BC\Repositories\BaseRepository;

use BC\Repositories\ProductVariation\ProductVariation;
use BC\Repositories\Product\Product;
use BC\Repositories\Style\Style;
use BC\Repositories\Uploadable\Uploadable;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductVariationRepository extends BaseRepository {

	protected $model;

	public function __construct(ProductVariation $productVariation, UploadableRepository $uploadRepo)
	{
		$this->model = $productVariation;
		$this->uploadRepo = $uploadRepo;
	}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store($styles, $collection, $hasStyle, $request)
	{
		$formattedStyles = [];
		$now = Carbon::now()->toDateTimeString();
		$styles = collect($styles);
		if( $hasStyle == false  ) {
			$styles->each(function($item, $key) use($collection, $request) {
				$productVar = new ProductVariation;
				$productVar->product_id = $collection->id;
				$productVar->color_id = $item['value'];
				$productVar->order = ( isset($item['count'])) ? $item['count'] : $item['order'];
				$productVar->save();
				if ( !empty($item['productvariation_image_url']) ):
					$this->uploadRepo->uploadImageIntervention($productVar, $item['productvariation_image_url'], $request, 'image', [
						'get_new_name' => str_slug($request->name.'-'.$item['productvariation_image_name']),
						'key'          => ProductVariation::IMG_PRODUCT_VARIATION_IMAGE,
						'field_key'    => 'product_variation_image',
						'path'         => 'uploads/product/variation',
						'filename'     => 'product_variation_image',
						'width'		   => '454',
						'height'       => '384'
					], false);
				endif;
				if ( !empty($item['productvariation_color_url']) ):
					$this->uploadRepo->uploadImageIntervention($productVar, $item['productvariation_color_url'], $request, 'image', [
						'get_new_name' => str_slug($request->name.'-'.$item['productvariation_color_name']),
						'key'          => ProductVariation::IMG_PRODUCT_VARIATION_COLOR,
						'field_key'    => 'product_variation_color',
						'path'         => 'uploads/product/variation',
						'filename'     => 'product_variation_color',
						'width'		   => '454',
						'height'       => '384'
					], false);
				endif;
			});
		} else {
			$styles->each(function($item, $key) use($collection, $request) {
				if( is_array($item['colorPanel']) ) {
					foreach( $item['colorPanel'] as $value ) {
						$thisStyle = $collection[$key];
						if (isset($item['id'])) {
							$thisStyle = $collection->first(function($obj) use ($item) {
								return $obj->id == $item['id'] ?: false;
							});
						}
						$productVar = new ProductVariation;
						$productVar->style_id = $thisStyle->id;
						$productVar->product_id = $thisStyle->product->id;
                        $productVar->color_id = $value['value'];
						$productVar->save();

						if ( !empty($value['productvariation_image_url']) ):
							$this->uploadRepo->uploadImageIntervention($productVar, $value['productvariation_image_url'], $request, 'image', [
								'get_new_name' => str_slug($request->name.'-'.$value['productvariation_image_name']),
								'key'          => ProductVariation::IMG_PRODUCT_VARIATION_IMAGE,
								'field_key'    => 'product_variation_image',
								'path'         => 'uploads/product/variation',
								'filename'     => 'product_variation_image',
								'width'		   => '454',
								'height'       => '384'
							], false);
						endif;
						if ( !empty($value['productvariation_color_url']) ):
							$this->uploadRepo->uploadImageIntervention($productVar, $value['productvariation_color_url'], $request, 'image', [
								'get_new_name' => str_slug($request->name.'-'.$value['productvariation_color_name']),
								'key'          => ProductVariation::IMG_PRODUCT_VARIATION_COLOR,
								'field_key'    => 'product_variation_color',
								'path'         => 'uploads/product/variation',
								'filename'     => 'product_variation_color',
								'width'		   => '454',
								'height'       => '384'
							], false);
						endif;
					}
				}
			});
		}
	}

	public function update($styles,  $collection, $hasStyle, $request)
	{
        // dd($request->all());
		$styles = collect($styles);
		if( $hasStyle == false ) {
			$existingVariationsIds = $collection->productVariations->pluck('id')->toArray();
			$updatedIds = [];
			$styles->each(function($item, $key) use($collection, $request, &$updatedIds, $existingVariationsIds) {
				$updatedIds[] = $item['id'];
				$productVar = isset($item['id']) ? $this->model->find($item['id']) : new ProductVariation();
				$productVar->product_id = $collection->id;
                $productVar->color_id = $item['value'];
				$productVar->order = ( isset($item['count'])) ? $item['count'] : $item['order'];
				$productVar->sync_count = $productVar->sync_count + 1;
				$productVar->save();
				if ( !empty($item['productvariation_image_url']) ) {
					$name = substr($item['productvariation_image_old_name'], 0, strrpos($item['productvariation_image_old_name'], "."));
					$filename = ( $item['productvariation_image_name'] == '' ) ? str_replace(array('product_variation_image'), '',$name) : $request->name.'-'.$item['productvariation_image_name'];
					if( !in_array($item['productvariation_image_name'], ['', null]) ) {
						$productvariation_image_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_image_path) : false;
						$this->uploadRepo->removeFileUploads($productvariation_image_url, [
							'type' => 'SINGLE_REMOVE_UPLOAD',
						]);
					}
					$this->uploadRepo->uploadImageIntervention($productVar, $item['productvariation_image_url'], $request, 'image', [
						'get_new_name' => str_slug($filename),
						'key'          => ProductVariation::IMG_PRODUCT_VARIATION_IMAGE,
						'field_key'    => 'product_variation_image',
						'path'         => 'uploads/product/variation',
						'filename'     => 'product_variation_image',
						'width'		   => '454',
						'height'       => '384'
					], false);
				} else {
					$productvariation_image_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_image_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_image_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);
					$productVar->uploads()->where('id', '=', $item['productvariation_image_id'])->delete();
				}
				if ( !empty($item['productvariation_color_url']) ) {
					$name = substr($item['productvariation_color_old_name'], 0, strrpos($item['productvariation_color_old_name'], "."));
					$filename = ( $item['productvariation_color_name'] == '' ) ? str_replace(array('product_variation_color'), '',$name) : $request->name.'-'.$item['productvariation_color_name'];

					if( !in_array($item['productvariation_color_name'], ['', null]) ) {
						$productvariation_color_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_color_path) : false;
						$this->uploadRepo->removeFileUploads($productvariation_color_url, [
							'type' => 'SINGLE_REMOVE_UPLOAD',
						]);
					}
					$this->uploadRepo->uploadImageIntervention($productVar, $item['productvariation_color_url'], $request, 'image', [
						'get_new_name' => str_slug($filename),
						'key'          => ProductVariation::IMG_PRODUCT_VARIATION_COLOR,
						'field_key'    => 'product_variation_color',
						'path'         => 'uploads/product/variation',
						'filename'     => 'product_variation_color',
						'width'		   => '454',
						'height'       => '384',
						'color' => true
					], false);
				} else {
					$productvariation_color_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_color_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_color_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);
					$productVar->uploads()->where('id', '=', $item['productvariation_color_id'])->delete();
				}
			});
			$toDelete = array_diff($existingVariationsIds, $updatedIds);
			$toDelete = collect($toDelete);
			$toDelete->each(function($item, $key) {
				ProductVariation::where('id', $item)->get()->each(function($prodVar) {
					$productvariation_image_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_image_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_image_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);

					$productvariation_color_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_color_path) : false;
					$this->uploadRepo->removeFileUploads($productvariation_color_url, [
						'type' => 'SINGLE_REMOVE_UPLOAD',
					]);

					$prodVar->delete();
				});
			});
		} else {
			$updatedIds = [];
			$styles->each(function($item, $key) use($collection, $request, &$updatedIds) {
				$thisStyle = $collection[$key];
				$styleID = $item['id'];
				$productID = $thisStyle->product->id;
				$existingVariationsIds = $thisStyle->productVariations->pluck('id')->toArray();
				if( is_array($item['colorPanel']) ) {
					foreach( $item['colorPanel'] as $value ) {
						$thisStyle = $collection[$key];
						$updatedIds[] = $value['id'];
						$productVar = isset($value['id']) ? $this->model->find($value['id']) : new ProductVariation();
						$productVar->style_id = ($thisStyle->id) ? $thisStyle->id : NULL;
						$productVar->product_id = $thisStyle->product->id;
						$productVar->color_id = $value['value'];
						$productVar->save();
						if ( !empty($value['productvariation_image_url']) ) {
							$name = substr($value['productvariation_image_old_name'], 0, strrpos($value['productvariation_image_old_name'], "."));
							$filename = ( $value['productvariation_image_name'] == '' ) ? str_replace(array('product_variation_image'), '',$name) : $thisStyle->product->name.'-'.$value['productvariation_image_name'];
							if( !in_array($value['productvariation_image_name'], ['', null]) ) {
								$productvariation_image_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_image_path) : false;
								$this->uploadRepo->removeFileUploads($productvariation_image_url, [
									'type' => 'SINGLE_REMOVE_UPLOAD',
								]);
							}
							$this->uploadRepo->uploadImageIntervention($productVar, $value['productvariation_image_url'], $request, 'image', [
								'get_new_name' => str_slug($filename),
								'key'          => ProductVariation::IMG_PRODUCT_VARIATION_IMAGE,
								'field_key'    => 'product_variation_image',
								'path'         => 'uploads/product/variation',
								'filename'     => 'product_variation_image',
								'width'		   => '454',
								'height'       => '384'
							], false);
						} else {
							$productvariation_image_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_image_path) : false;
							$this->uploadRepo->removeFileUploads($productvariation_image_url, [
								'type' => 'SINGLE_REMOVE_UPLOAD',
							]);
							$productVar->uploads()->where('id', '=', $value['productvariation_image_id'])->delete();
						}

						if ( !empty($value['productvariation_color_url']) ) {
							$name = substr($value['productvariation_color_old_name'], 0, strrpos($value['productvariation_color_old_name'], "."));
							$filename = ( $value['productvariation_color_name'] == '' ) ? str_replace(array('product_variation_color'), '',$name) : $thisStyle->product->name.'-'.$value['productvariation_color_name'];
							if( !in_array($value['productvariation_color_name'], ['', null]) ) {
								$productvariation_color_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_color_path) : false;
								$this->uploadRepo->removeFileUploads($productvariation_color_url, [
									'type' => 'SINGLE_REMOVE_UPLOAD',
								]);
							}
							$this->uploadRepo->uploadImageIntervention($productVar, $value['productvariation_color_url'], $request, 'image', [
								'get_new_name' => str_slug($filename),
								'key'          => ProductVariation::IMG_PRODUCT_VARIATION_COLOR,
								'field_key'    => 'product_variation_color',
								'path'         => 'uploads/product/variation',
								'filename'     => 'product_variation_color',
								'width'		   => '454',
								'height'       => '384'
							], false);
						} else {
							$productvariation_color_url = isset($productVar) && $productVar->exists ? public_path($productVar->product_variation_color_path) : false;
							$this->uploadRepo->removeFileUploads($productvariation_color_url, [
								'type' => 'SINGLE_REMOVE_UPLOAD',
							]);
							$productVar->uploads()->where('id', '=', $value['productvariation_color_id'])->delete();
						}
					}
					$toDelete = array_diff($existingVariationsIds, $updatedIds);
					$toDelete = collect($toDelete);
					$toDelete->each(function($item, $key) {
						ProductVariation::where('id', $item)->get()->each(function($prodVar) {
							$productvariation_image_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_image_path) : false;
							$this->uploadRepo->removeFileUploads($productvariation_image_url, [
								'type' => 'SINGLE_REMOVE_UPLOAD',
							]);

							$productvariation_color_url = isset($prodVar) && $prodVar->exists ? public_path($prodVar->product_variation_color_path) : false;
							$this->uploadRepo->removeFileUploads($productvariation_color_url, [
								'type' => 'SINGLE_REMOVE_UPLOAD',
							]);

							$prodVar->delete();
						});
					});
				}
			});
		}
	}

	// public function delete(Style $style)
	// {
	// 	$style->delete();
	// 	return $style;
	// }
}
