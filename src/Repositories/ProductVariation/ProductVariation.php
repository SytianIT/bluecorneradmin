<?php

namespace BC\Repositories\ProductVariation;

use App\Model\BaseModel;
use BC\Repositories\Color\Color;
use BC\Repositories\Product\Product;
use BC\Repositories\Style\Style;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class ProductVariation extends BaseModel
{
    const IMG_PRODUCT_VARIATION_IMAGE = 'product_variation_image';
    const IMG_PRODUCT_VARIATION_COLOR = 'product_variation_color';

    use ValidatingTrait, SoftDeletes;

	protected $rules = [

    ];

    protected $fillable = [
       'style_id','product_id','color_id'
    ];

    protected $appends = [
        'product_variation_image_path',
        'product_variation_color_path',
    ];

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function color() {
        return $this->belongsTo(Color::class);
    }

    public function style() {
        return $this->belongsTo(Style::class);
    }

    public function isActive()
    {
        return $this->active ? true : false;
    }

    public function getProductVariationImageAttribute() // product_variation_image
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_PRODUCT_VARIATION_IMAGE ? true : false;
        })->first();

        return $uploads;
    }

    public function getProductVariationImagePathAttribute() // product_variation_image_path
    {
        return $this->product_variation_image ? $this->product_variation_image->path : '';
    }

    public function getProductVariationColorAttribute() // product_variation_color
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_PRODUCT_VARIATION_COLOR ? true : false;
        })->first();

        return $uploads;
    }

    public function getProductVariationColorPathAttribute() // product_variation_color_path
    {
        return $this->product_variation_color ? $this->product_variation_color->path : '';
    }
}
?>
