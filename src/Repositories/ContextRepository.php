<?php

namespace BC\Repositories;

use BC\ContextInterface;
use BC\ContextScope;
use BC\Repositories\BaseRepository;
use Rutorika\Sortable\SortableTrait;

abstract class ContextRepository extends BaseRepository {

	/**
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;

	/**
	 * @var App\Zensoft\Context
	 */
	protected $context;

	public function setContext(ContextInterface $context) 
	{
		$this->context = $context;
	}

	public function scope($with = [])
	{
		$query = $this->model->with($with);
		if ($this->context->has()) {
			if ($this->model instanceof ContextScope) {
				$query = $query->ofContext($this->context);
			} else {
				$query = $query->where($this->context->column(), $this->context->id());
			}
		}
		
		$classes = class_uses($this->model, false);
		if ($classes && in_array(SortableTrait::class, $classes)) {
			$query = $query->sorted();
		}
		return $query;
	}

	public function find($id, $with = array())
	{
		return $this->scope($with)->find($id);
	}

	public function findOrFail($id, $with = array())
	{
		return $this->scope($with)->find($id) ?: abort(404);
	}

	public function findTrashed($id)
	{
		return $this->scope()->onlyTrashed()->find($id);
	}

    public function findDontAbort($id, $with = array()) 
    {
        return $this->scope($with)->find($id);
    }

	public function findWithTrash($id)
	{
		return $this->scope()->withTrashed()->find($id);
	}

	public function all($with = array())
	{
		return $this->scope($with)->get();
	}

	public function allWithTrashed($with = array())
	{
		return $this->scope($with)->withTrashed()->get();	
	}

	public function allTrashed($with = array()) 
	{
		return $this->scope($with)->onlyTrashed()->get();	
	}

	public function getAllBy($key, $value, $with = array())
	{
		$collection = $this->scope($with)->where($key, $value)->get();

		return $collection;
	}

	public function getFirstBy($key, $value, $with = array())
	{
		return $this->scope()->where($key, $value)->first();
	}

	public function getTrashedFirstBy($key, $value, $with = array())
	{
		return $this->scope()->onlyTrashed()->where($key, $value)->first();
	}

	public function queuedArray($collection, $columns = array()) {

	 	$collection = $collection->all();
	 	$arrCollection = [];

	 	foreach( $collection as $object ) {

		 	$arrColumn = [];

		 	// Columns
	 		if( count($columns) > 0 ) {
	 			foreach( $columns as $key => $colName ) {

 					$arrColumn[$key] = is_callable($colName) ? $colName($object) : $object->$colName;

	 			}

	 		}

	 		array_push($arrCollection, $arrColumn);

	 	}

	 	return $arrCollection;

	}

}