<?php

namespace BC\Repositories\Users;

use BC\Repositories\Role\Role;
use App\User;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Repositories\ContextRepository;
use BC\Validation\ValidationException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserRepository extends ContextRepository {

	protected $model;

	public function __construct(User $user, UploadableRepository $uploadRepo)
	{
		$this->model = $user;
        $this->uploadRepo = $uploadRepo;
	}

	public function store(User $user = null, Request $request)
	{
        $user = $this->model;

        $this->validateUserData($request);
        $this->validatePassword($request->password, $request->password_confirmation);

        if (count($this->errors) > 0) {
            throw new \BC\Validation\ValidationException($this->errors);
        }
        

        $user->fill($request->input());
        $user->password = bcrypt($request->password);
        $user->active = ($request->active == true)? 1 : 0;
        
        if($user->save()){
            $user->roles()->sync($request->role_id);
        }

        if ( !empty($request['thumb']) ):
            $this->uploadRepo->uploadImageIntervention($user, $request['thumb'], $request, 'image', [
                'get_new_name' => str_slug($request->first_name),
                'key' => User::IMG_LOGO,
                'field_key' => 'thumb',
                'path' => 'uploads/user',
                'filename' => 'thumb',
                'width' => 358,
                'height' => 182
            ], false);
        endif;

        return $user;
    }

    public function update(Request $request)
    {

        $this->validateEditUserData($request);
        $this->validateEditPassword($request); //$request->password, $request->confirm_password

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

        $user = $this->model->find($request->id);

        $user->fill($request->input());
        $user->password = bcrypt($request->password);
        $user->active = ($request->active == true )? 1 : 0;

        $user->roles()->sync($request->role_id);
        $user->save();

        if (!empty($request['thumb'])) {
            $this->uploadRepo->uploadImageIntervention($user, $request['thumb'], $request, 'image', [
                'get_new_name' => str_slug($request->first_name),
                'key' => User::IMG_LOGO,
                'field_key' => 'thumb',
                'path' => 'uploads/user',
                'filename' => 'thumb',
                'width' => 358,
                'height' => 182
            ], false);
        }

		return $user;
	}

    public function delete(Request $request, $userId)
    {
        
        $user = $this->model->find($userId);
        $user->uploads()->where('uploadable_id', '=', $userId)->delete();
        $user->delete();

        $user->roles()->detach();

        return $user->name;
    }

	public function validateUserData(Request $request)
    {
        $validator = \Validator::make($request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
                'password_confirmation' => 'required',
                'role_id' => 'required'
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        } 
    }

    public function validatePassword($password, $passwordConfirmation)
    {
        $validator = \Validator::make(
            array(
                'password' => $password,
                'password_confirmation' => $passwordConfirmation,
            ),
            array(
                'password' => 'required|min:6|confirmed'
            )
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    public function validateEditPassword($request) //$password, $passwordConfirmation
    {
        $rules = [];

        if ($request->filled('password')) {
            $rules = array(
                'password' => 'confirmed'
            );
        }

        $validator = \Validator::make(
            $request->all(),
            $rules
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    public function validateEditUserData(Request $request)
    {
        $validator = \Validator::make($request->all(),
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                // 'confirm_password' => 'required'
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        } 
    }
    
}
