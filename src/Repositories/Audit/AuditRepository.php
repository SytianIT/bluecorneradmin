<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 11/4/2017
 * Time: 1:30 PM
 */

namespace BC\Repositories\Audit;

use BC\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuditRepository extends BaseRepository
{
    protected $model;

    public function __construct(
        Audit $audit
    )
    {
        $this->model = $audit;
    }

    public function search($request, $with = [])
    {
        $with = [
            'user' => function($q){
                $q->withTrashed();
            }
        ];

        $query = $this->with($with)->latest();

        if ($request->filled('from_date') && $request->filled('to_date')){
            $fromDate = new Carbon($request->from_date);
            $toDate = new Carbon($request->to_date);

            $query = $query->where(function ($q) use ($fromDate, $toDate){
                $q->whereDate('created_at', '>=', $fromDate)
                    ->whereDate('created_at', '<=', $toDate);
            });
        }
        if ($request->filled('dashboard')) {
            $query = $query->take(50);
        }

        if ($request->filled('per_page')) {
            $results = $query->paginate($request->per_page ?: self::PER_PAGE);
        } else {
            $results = $query->get();
        }
        return $results;
    }

    public function create($model, $desc, $type)
    {
        $user = Auth::user();
        // if (!$user->isSuperAdmin()) {
            $audit = $this->model;

            $audit->auditable_type = get_class($model);
            $audit->auditable_id = $model->id;
            $audit->action = $type;
            $audit->description = $desc;
            $audit->user_id = Auth::user()->id;
            $audit->save();
        // }
        return true;
    }
}
