<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 11/4/2017
 * Time: 1:30 PM
 */

namespace BC\Repositories\Audit;

use App\Model\BaseModel;
use App\User;

class Audit extends BaseModel
{
    protected $table = 'audits';

    const TYPE_ADD = 'create';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETED = 'delete';
    const TYPE_DESTROY = 'destroy';
    const TYPE_RECOVER = 'recover';
    const TYPE_PERMANENT = 'permanent';
    const TYPE_ARCHIVED = 'archived';
    const TYPE_MASS = 'mass';
    const TYPE_PRICE = 'price';
    const TYPE_OTHERS = 'others';

    public function auditable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Getters & Setters
     */
    public function getReadableDescriptionAttribute()
    {
        $type = $this->action;

        if ($type == self::TYPE_ADD) {
            return $this->description . ' created.';
        } else if ($type == self::TYPE_UPDATE) {
            return $this->description . ' updated.';
        } else if ($type == self::TYPE_DELETED) {
            return $this->description . ' deleted.';
        } else if ($type == self::TYPE_DESTROY) {
            return $this->description . ' permanently deleted.';
        } else if ($type == self::TYPE_RECOVER) {
            return $this->description . ' recovered.';
        } else if ($type == self::TYPE_ARCHIVED) {
            return $this->description . ' archived.';
        } else if ($type == self::TYPE_PERMANENT) {
            return $this->description . ' deleted permanently.';
        } else if ($type == self::TYPE_MASS) {
            return 'Perform a ' . $this->description;
        } else if ($type == self::TYPE_PRICE) {
            return 'Perform a ' . $this->description;
        } else if ($type == self::TYPE_OTHERS) {
            return $this->description;
        } else {
            return 'Unknown action';
        }
    }
}
