<?php

namespace BC\Repositories\Permission;

// use BC\Repositories\Contest\Contest;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Permission extends BaseModel
{
    use ValidatingTrait, SoftDeletes;

    protected $table = "permissions";

    protected $rules = [
        'title'  => 'required',
    ];

    protected $fillable = [
        'title', 'group',
    ];


   /* public function contest() {
        return $this->belongsTo(Contest::class);
    }*/

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Permission '{$this->title}'";
    }

    public function auditUpdate()
    {
        return "Permission '{$this->title}'";
    }

    public function auditDelete()
    {
        return "Permission '{$this->title}'";
    }
}

?>
