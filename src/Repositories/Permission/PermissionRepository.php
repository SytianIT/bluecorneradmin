<?php

namespace BC\Repositories\Permission;

use BC\Repositories\BaseRepository;

use BC\Repositories\Permission\Permission;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PermissionRepository extends BaseRepository {

	protected $model;

	public function __construct(Permission $permission)
	{
		$this->model = $permission;
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	public function store(Request $request)
	{
		$permission = $this->model;

		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$permission->fill($request->input());
		$permission->key = str_slug($request->title);
		$permission->save();
		$permission->audit('create');

		return $permission;
	}

	public function update(Request $request, $permission_id)
	{
		$permission = $this->model->find($permission_id);

		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$permission->fill($request->input());
		$permission->key = str_slug($request->title);
		$permission->save();
		$permission->audit('update');
		return $permission;
	}

	public function delete(Request $request, $permission_id)
    {
        $item = $this->model->find($permission_id);
        $item->delete();
		$item->audit('delete');

        return $item->title;
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Contest $contest = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'title' => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }
}
