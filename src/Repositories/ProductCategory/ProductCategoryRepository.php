<?php

namespace BC\Repositories\ProductCategory;

use BC\Repositories\BaseRepository;

use BC\Repositories\ProductCategory\ProductCategory;
use BC\Repositories\Uploadable\Uploadable;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProductCategoryRepository extends BaseRepository {

	protected $model;

	public function __construct(ProductCategory $productCategory, UploadableRepository $uploadRepo)
	{
		$this->model = $productCategory;
		$this->uploadRepo = $uploadRepo;
	}

	public function search(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = $this->model->with($with);

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('featured')) {
			$query = $query->where('featured', 1);
		}

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(ProductCategory $productCategory = null, Request $request)
	{

		$this->validateData($request);
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}
		$productCategory = $this->model;
		$productCategory->fill($request->input());
		$productCategory->parent_id = isset($request->parent_id) ? $request->parent_id : NULL;
		$productCategory->featured = isset($request->featured)? 1 : 0;
		$productCategory->save();

        if ( !empty($request['thumb_mobile'][0]['src']) ):
            $newName = str_replace("-", " ", $request->name);
            $this->uploadRepo->uploadImageIntervention($productCategory, $request['thumb_mobile'][0]['src'], $request, 'image', [
                'get_new_name' => str_slug($newName).'-'.ProductCategory::IMG_LOGO,
                'key' => ProductCategory::IMG_LOGO,
                'field_key' => 'img_logo_path',
                'path' => 'uploads/category',
                'filename' => 'img_logo_path',
            ], false);
        endif;

		return $productCategory;
	}

	public function update(Request $request)
	{
		$this->validateData($request);
		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}
		$productCategory = $this->model->find($request->id);
		$productCategory->fill($request->input());
		$productCategory->parent_id = isset($request->parent_id) ? $request->parent_id: NULL;
		$productCategory->featured = ($request->featured == true)? 1 : 0;
		$productCategory->sync_count = $productCategory->sync_count + 1;
		$productCategory->save();

        if ( !empty($request['thumb_mobile'][0]['src']) || $request['thumb_mobile'][0]['src'] != NULL ):
            $newName = str_replace("-", " ", $request->name);
            $this->uploadRepo->uploadImageIntervention($productCategory, $request['thumb_mobile'][0]['src'], $request, 'image', [
                'get_new_name' => str_slug($newName).'-'.ProductCategory::IMG_LOGO,
                'key' => ProductCategory::IMG_LOGO,
                'field_key' => 'img_logo_path',
                'path' => 'uploads/category',
                'filename' => 'img_logo_path',
            ], false);
        else:
            $productCategory->uploads()->where('id', '=', $request['thumb_mobile'][0]['id'])->delete();
        endif;
		return $productCategory;
	}

	public function delete(Request $request, $category_id)
    {

        $productCategory = $this->model->find($category_id);

        $category_mobile_thumb = isset($productCategory) && $productCategory->exists ? public_path($productCategory->img_logo_path) : false;
        $this->uploadRepo->removeFileUploads($category_mobile_thumb, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $productCategory->uploads()->where('uploadable_id', '=', $category_id)->delete();
        $productCategory->delete();

        return $productCategory->name;
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, ProductCategory $productCategory = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'name' => 'required'
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

    /**
	 * Methods to clean object to return for APIE
	 */
	public function cleanCategory($collection)
	{
		foreach ($collection as $item) {
			$ftImage = $this->encodedUploadables($item->img_logo);
			$item->featured_image = $ftImage;

			if ($item->childs->count() > 0) {
				$item->child_categories = $this->cleanCategory($item->childs);
			}
		}

		return $collection;
    }
}
