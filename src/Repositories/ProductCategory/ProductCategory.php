<?php

namespace BC\Repositories\ProductCategory;

use App\Model\BaseModel;
use BC\Repositories\Product\Product;
use BC\Repositories\Traits\Nestable;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;
use Watson\Validating\ValidatingTrait;

class ProductCategory extends BaseModel
{
	const IMG_LOGO = 'logo';

	public $table = "product_categories";

	use ValidatingTrait, SoftDeletes, NodeTrait, Nestable;

	protected $rules = [
		'name'  => 'required',
	];

	protected $fillable = [
		'name', 'description', 'order', 'featured'
	];

	protected $appends = [
		'img_logo_path'
	];

	/**
	 * Get the products that owns the categories.
	 */
	public function product()
	{
		return $this->hasOne(Product::class, 'category_id');
	}

	// Relational method from JR
	public function childs()
	{
		return $this->hasMany(ProductCategory::class, 'parent_id');
	}

	public function uploads()
	{
		return $this->morphMany(Uploadable::class, 'uploadable');
	}

	public function getImgLogoAttribute() // img_icon
	{
		$uploads = $this->uploads->filter(function ($ups) {
			return $ups->key == self::IMG_LOGO ? true : false;
		})->first();

		return $uploads;
	}

	public function getImgLogoPathAttribute() // img_icon_path
	{
		return $this->IMG_LOGO ? $this->img_logo->path : Uploadable::IMG_PLACEHOLDER;
	}

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Product Category '{$this->name}'";
    }

    public function auditUpdate()
    {
        return "Product Category '{$this->name}'";
    }

    public function auditDelete()
    {
        return "Product Category '{$this->name}'";
    }
}
?>
