<?php

namespace BC\Repositories\Contest;

use App\Mail\FeedbackSubmission;
use BC\Repositories\BaseRepository;

use BC\Repositories\Contest\Contest;
use BC\Repositories\Items\Item;
use BC\Repositories\Settings\Setting;
use BC\Repositories\Uploadable\Uploadable;
use BC\Repositories\Uploadable\UploadableRepository;
use BC\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ContestRepository extends BaseRepository {

	protected $model;

	public function __construct(Contest $contest, UploadableRepository $uploadRepo, Item $items)
	{
		$this->model = $contest;
		$this->uploadRepo = $uploadRepo;
	}

	/**
	 * @param Request $request
	 * @param array $with
	 * @param array $cols
	 * @param bool $paginated
	 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function search(Request $request, $with = [], $cols = [], $paginated = false)
	{
		$query = $this->model->with($with);

		if ($request->filled('contest_id')) {
			$query = $query->where('id', $request->contest_id);
		}

		if (count($cols) > 0) {
			$query = $query->select($cols);
		}

		if ($request->filled('not_archive')) {
			$query = $query->where('has_entries', 0);
		}

		if ($request->filled('actives')) {
			$query = $query->where('status', 'active');
		}

		if ($request->filled('published')) {
			$query = $query->where('published', 1);
		}

		if ($paginated) {
			return $query->paginate($request->per_page ?: self::PER_PAGE);
		} else {
			return $query->get();
		}
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

	public function store(Contest $contest = null, Request $request)
	{

        $this->validateData($request);
        if (count($this->errors) > 0) {
            throw new \BC\Validation\ValidationException($this->errors);
        }

        $date_range = str_replace(' ', '', explode("-",$request->date_range));
        $date_from_output = date('Y-m-d', strtotime($date_range[0]));
        $date_to_output = date('Y-m-d', strtotime($date_range[1]));

		$contest = $this->model;
		$contest->fill($request->input());
		$contest->slug = str_slug($request->title);
		$contest->date_from = $date_from_output ? $date_from_output : '0000-00-00 00:00:00';
		$contest->date_to = $date_to_output ? $date_to_output : '0000-00-00 00:00:00';
		$contest->status = ($request->status['id'])? $request->status['id'] : '';
		$contest->published = ($request->published)? 1 : 0;
		$contest->save();
		$contest->audit('create');

        /* BANNER IMAGE */
            // uploadables for mobile thumbnail 1440 x 2560
            if ( !empty($request['banner_mobile'][0]['src']) ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['banner_mobile'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_BANNER_MOBILE,
                    'key' => Contest::IMG_BANNER_MOBILE,
                    'field_key' => 'banner_mobile',
                    'path' => 'uploads/contest/mobile',
                    'filename' => 'banner_mobile',
                    'width' => '376',
                    'height' => '209'
                ], false);
            endif;
            // uploadables for tablet thumbnail 1280 x 1200
            if ( !empty($request['banner_tablet'][0]['src']) ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['banner_tablet'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_BANNER_TABLET,
                    'key' => Contest::IMG_BANNER_TABLET,
                    'field_key' => 'banner_tablet',
                    'path' => 'uploads/contest/tablet',
                    'filename' => 'banner_tablet',
                    'width' => '1280',
                    'height' => '800 '
                ], false);
            endif;
        /* END */
        /* FEATURED IMAGE */
			// uploadables for mobile thumbnail 1440 x 2560
            if ( !empty($request['thumb_mobile'][0]['src']) ):
                $newName = str_replace("-", " ", $request->title);
    			$this->uploadRepo->uploadImageIntervention($contest, $request['thumb_mobile'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_LOGO,
                    'key' => Contest::IMG_LOGO,
                    'field_key' => 'thumb_mobile',
                    'path' => 'uploads/contest/mobile',
                    'filename' => 'thumb_mobile',
    			], false);
            endif;
            // uploadables for tablet thumbnail 1920 x 1200
            if ( !empty($request['thumb_tablet'][0]['src']) ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['thumb_tablet'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_THUMB_TABLET,
                    'key' => Contest::IMG_THUMB_TABLET,
                    'field_key' => 'thumb_tablet',
                    'path' => 'uploads/contest/tablet',
                    'filename' => 'thumb_tablet',
                ], false);
            endif;
        /* END */

		return $contest;
	}

	public function update(Request $request)
	{
		$this->validateData($request);

		if (count($this->errors) > 0) {
			throw new \BC\Validation\ValidationException($this->errors);
		}

		$contest = $this->model->find($request->id);
		$date_range = str_replace(' ', '', explode("-",$request->date_range));

		$date_from_output = date('Y-m-d', strtotime($date_range[0]));
		$date_to_output = date('Y-m-d', strtotime($date_range[1]));

		$contest->fill($request->input());
		$contest->slug = str_slug($request->title);
		$contest->date_from = $date_from_output ? $date_from_output : '0000-00-00 00:00:00';
		$contest->date_to = $date_to_output ? $date_to_output : '0000-00-00 00:00:00';
        if ( !is_array( $request->status ) ) { $addStatus = $request->status; } else { $addStatus = $request->status['id'];  }
		$contest->status = $addStatus;
        $contest->published = ($request->published)? 1 : 0;
        $contest->sync_count = $contest->sync_count + 1;
		$contest->save();
        $contest->audit('update');
        /* BANNER IMAGE */
            // uploadables for mobile thumbnail 1440 x 2560
            if ( !empty($request['banner_mobile'][0]['src']) || $request['banner_mobile'][0]['src'] != NULL ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['banner_mobile'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_BANNER_MOBILE,
                    'key' => Contest::IMG_BANNER_MOBILE,
                    'field_key' => 'contest_banner_mobile',
                    'path' => 'uploads/contest/mobile',
                    'filename' => 'contest_banner_mobile',
                ], false);
            else:
                $contest->uploads()->where('id', '=', $request['banner_mobile'][0]['id'])->delete();
            endif;
            // uploadables for tablet thumbnail 1920 x 1200
            if ( !empty($request['banner_tablet'][0]['src']) || $request['banner_tablet'][0]['src'] != NULL ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['banner_tablet'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_BANNER_TABLET,
                    'key' => Contest::IMG_BANNER_TABLET,
                    'field_key' => 'contest_banner_tablet',
                    'path' => 'uploads/contest/tablet',
                    'filename' => 'contest_banner_tablet',
                ], false);
            else:
                $contest->uploads()->where('id', '=', $request['banner_tablet'][0]['id'])->delete();
            endif;
        /* END */
        /* FEATURED IMAGE */
            // uploadables for mobile thumbnail 1440 x 2560
            if (!empty($request['thumb_mobile'][0]['src']) || $request['thumb_mobile'][0]['src'] != NULL ) :
                $newName = str_replace("-", " ", $request->title);
    			$this->uploadRepo->uploadImageIntervention($contest, $request['thumb_mobile'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_LOGO,
                    'key' => Contest::IMG_LOGO,
                    'field_key' => 'contest_thumb_mobile',
                    'path' => 'uploads/contest/mobile',
                    'filename' => 'contest_thumb_mobile',
    			], false);
            else:
                $contest->uploads()->where('id', '=', $request['thumb_mobile'][0]['id'])->delete();
            endif;
            // uploadables for tablet thumbnail 1920 x 1200
            if ( !empty($request['thumb_tablet'][0]['src']) || $request['thumb_tablet'][0]['src'] != NULL ):
                $newName = str_replace("-", " ", $request->title);
                $this->uploadRepo->uploadImageIntervention($contest, $request['thumb_tablet'][0]['src'], $request, 'image', [
                    'get_new_name' => str_slug($newName).'-'.Contest::IMG_THUMB_TABLET,
                    'key' => Contest::IMG_THUMB_TABLET,
                    'field_key' => 'contest_thumb_tablet',
                    'path' => 'uploads/contest/tablet',
                    'filename' => 'contest_thumb_tablet',
                ], false);
            else:
                $contest->uploads()->where('id', '=', $request['thumb_tablet'][0]['id'])->delete();
            endif;
        /* END */

		return $contest;
	}

    public function moveToArchive(Contest $contest, Request $request )
	{
		$contest->has_entries = $request->has_entries == 0 ? 1 : 0;
        $contest->save();

        $contest->contestEntries()->update(['has_entries' => 1]);

		return $contest;
	}

	public function delete(Request $request, Item $items, $contest_id)
    {
        $contest = $this->model->find($contest_id); // removed contest

        $contest_banner_mobile_path = isset($contest) && $contest->exists ? public_path($contest->contest_banner_mobile_path) : false;
        $this->uploadRepo->removeFileUploads($contest_banner_mobile_path, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $contest_banner_tablet_path = isset($contest) && $contest->exists ? public_path($contest->contest_banner_tablet_path) : false;
        $this->uploadRepo->removeFileUploads($contest_banner_tablet_path, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $contest_thumb_mobile_path = isset($contest) && $contest->exists ? public_path($contest->contest_thumb_mobile_path) : false;
        $this->uploadRepo->removeFileUploads($contest_thumb_mobile_path, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $contest_thumb_tablet_path = isset($contest) && $contest->exists ? public_path($contest->contest_thumb_tablet_path) : false;
        $this->uploadRepo->removeFileUploads($contest_thumb_tablet_path, [
            'type' => 'SINGLE_REMOVE_UPLOAD',
        ]);

        $contest->uploads()->where('uploadable_id', '=', $contest_id)->delete();
        $contest->contestItem()->delete(); // removed items under contest
        $contest->delete();
        $contest->audit('delete');
        return $contest->name;
    }

    private function uploadMedia(Request $request, $contest)
    {

        if ($request->hasFile('icon')) {
            $this->uploadRepo->createUpload($contest, $request, 'image', [
                'key' => Contest::IMG_LOGO,
                'field_key' => 'icon',
                'path' => 'uploads/contest',
                'filename' => 'icon',
                'width' => 50,
                'height' => 50
            ]);
        }

        if ($request->hasFile('thumb')) {
            $this->uploadRepo->createUpload($contest, $request, 'image', [
                'key' => Contest::IMG_LOGO,
                'field_key' => 'thumb',
                'path' => 'uploads/contest',
                'filename' => 'thumb',
                'width' => 358,
                'height' => 182
            ]);
        }

        if ($request->hasFile('banner')) {
            $this->uploadRepo->createUpload($contest, $request, 'image', [
                'key' => Contest::IMG_LOGO,
                'field_key' => 'banner',
                'path' => 'uploads/contest',
                'filename' => 'banner',
                'width' => 1366,
                'height' => 345
            ]);
        }
    }

    /**
	 * Use to Validate Entry in Database
	 * @param Request $request
	 */
    public function validateData(Request $request, Contest $contest = null)
    {
        $validator = \Validator::make($request->all(),
            [
                'title' => 'required',
                'date_range' => 'required',
            ]
        );

        if ($validator->fails()) {
            $this->errors = $validator->messages()->merge($this->errors);
        }
    }

	/**
	 * Methods to clean object to return for APIE
	 */
	public function cleanContest($collection)
	{
		foreach ($collection as $item) {
			$item->featured_image = $this->encodedUploadables($item->contest_thumb_mobile);
			$item->tablet_banner = $this->encodedUploadables($item->contest_banner_tablet);
			$item->mobile_banner = $this->encodedUploadables($item->contest_banner_mobile);
		}

		return $collection;
	}

	/**
	 * Insert Survey
	 */
	public function saveContestAnswer(Request $request)
	{
		$siteSettings = Setting::getSettings();
		$message = 'Feedback is no longer available';
		$contest = Contest::with(['contestItem', 'contestEntries'])->find($request->contest_id);
		$formData = (object) json_decode($request->form);

		if ($contest) {
			$proceed = false;
			// Check if existing uid and email na sa mga feedback records
			$exist = $contest->contestEntries->first(function($item) use($formData, $request) {
				if ($formData->email) {
					return $item->device_uid == $request->uuid || $item->email == $formData->email;
				}
				return false;
			});

			if (!$exist) {
				$answersArr = json_decode($request->answers);

				$birthday = null;
				if ($formData->birthday) {
					$dateValue = date_parse($formData->birthday);
					$birthday = Carbon::create($dateValue['year'], $dateValue['month'], $dateValue['day'], $dateValue['hour'], $dateValue['minute'], $dateValue['second']);
				}
				if (count($answersArr) > 0) {
					$contestEntry = new ContestEntry();
					$contestEntry->contest_id = $contest->id;
					$contestEntry->contest_title = $contest->title;
					$contestEntry->name = $formData->name;
					$contestEntry->birthday = $birthday ?: null;
					$contestEntry->contact_number = $formData->contact_number;
					$contestEntry->email = $formData->email;
					$contestEntry->message = $formData->message;
					$contestEntry->device_uid = $request->uuid;
					$contestEntry->save();

					foreach ($answersArr as $arr) {
						$item = $contest->contestItem->first(function($item) use($arr) {
							return $item->id == $arr->item_id;
						});

						if ($item) {
							$entryItem = new ContestEntryAnswer();
							$entryItem->contest_entry_id = $contestEntry->id;
							$entryItem->contest_item_id = $item->id;
							$entryItem->type = $item->getType();
							$entryItem->question = $item->question;

							if ($item->type == 'checkbox') {
								$entryItem->answer = json_encode($arr->answers);
							} else {
								$entryItem->answer = $arr->answer;
							}
							$entryItem->save();
						} else {
							$itemsInsert = false;
							$message = 'Something went wrong while processing the feedback, please try again later!';
							break;
						}
					}

					if ($contestEntry){
						// Do email notification here about success of insert feedback
						Mail::to($siteSettings->site_email)->send(new FeedbackSubmission($contestEntry, $contest));
						$proceed = true;
						$message = 'Submission successful!';
					}
				} else {
					$message = 'No answer provided from the feedback form, please try again.';
				}
			} else {
				$message = 'You have already submitted an entry for this feedback, Thank you!';
			}
			return ['success' => true, 'message' => $message, 'proceed' => $proceed];
		}

		return ['success' => false, 'message' => $message];
	}
}
