<?php

namespace BC\Repositories\Contest;

use App\Model\BaseModel;
use BC\Repositories\Items\Item;
use BC\Repositories\Uploadable\Uploadable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Watson\Validating\ValidatingTrait;

class Contest extends BaseModel
{
    const IMG_LOGO = 'contest_thumb_mobile';
    const IMG_THUMB_TABLET = 'contest_thumb_tablet';
    const IMG_BANNER_MOBILE = 'contest_banner_mobile';
    const IMG_BANNER_TABLET = 'contest_banner_tablet';

    use ValidatingTrait, SoftDeletes;

	protected $rules = [
        'title'  => 'required',
    ];

    protected $fillable = [
        'title', 'description', 'date_from', 'date_to', 'status', 'published'
    ];

    protected $appends = [
        'contest_thumb_mobile_path', 'contest_thumb_tablet_path',
        'contest_banner_mobile_path', 'contest_banner_tablet_path',
    ];

    public function contestItem()
    {
        return $this->hasMany(Item::class, 'contest_id');
    }

	public function contestEntries()
	{
		return $this->hasMany(ContestEntry::class, 'contest_id');
    }

    public function uploads()
    {
        return $this->morphMany(Uploadable::class, 'uploadable');
    }

    public function getContestBannerMobileAttribute() // contest_banner_mobile
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER_MOBILE ? true : false;
        })->first();

        return $uploads;
    }

    public function getContestBannerMobilePathAttribute() // contest_banner_mobile_path
    {
        return $this->contest_banner_mobile ? $this->contest_banner_mobile->path : Uploadable::IMG_PLACEHOLDER;
    }


    public function getContestBannerTabletAttribute() // contest_banner_tablet
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_BANNER_TABLET ? true : false;
        })->first();

        return $uploads;
    }

    public function getContestBannerTabletPathAttribute() // contest_banner_tablet_path
    {
        return $this->contest_banner_tablet ? $this->contest_banner_tablet->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getContestThumbMobileAttribute() // contest_thumb_mobile
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_LOGO ? true : false;
        })->first();

        return $uploads;
    }

    public function getContestThumbMobilePathAttribute() // contest_thumb_mobile_path
    {
        return $this->contest_thumb_mobile ? $this->contest_thumb_mobile->path : Uploadable::IMG_PLACEHOLDER;
    }

    public function getContestThumbTabletAttribute() // contest_thumb_tablet
    {
        $uploads = $this->uploads->filter(function ($ups) {
            return $ups->key == self::IMG_THUMB_TABLET ? true : false;
        })->first();

        return $uploads;
    }

    public function getContestThumbTabletPathAttribute() // contest_thumb_tablet_path
    {
        return $this->contest_thumb_tablet ? $this->contest_thumb_tablet->path : Uploadable::IMG_PLACEHOLDER;
    }

	/**
	 * Scope Queries
	 */
	public function scopeOnqueue($query)
	{
		return $query->where('status', 'on-queue');
	}

    /*
     * Audit log for product
     */

    public function audit($type)
    {
        $auditRepo = resolve(\BC\Repositories\Audit\AuditRepository::class);

        $funcName = "audit{$type}";
        $message = $this->{$funcName}();

        $auditRepo->create($this, $message, $type);
    }

    public function auditCreate()
    {
        return "New Contest '{$this->title}'";
    }

    public function auditUpdate()
    {
        return "Contest '{$this->title}'";
    }

    public function auditDelete()
    {
        return "Contest '{$this->title}'";
    }
}
?>

