<?php

namespace BC;

use BC\ContextInterface;

interface ContextScope {

	public function scopeOfContext($query, ContextInterface $context);

}