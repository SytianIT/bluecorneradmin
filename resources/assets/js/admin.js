$(document).ready(function () {
	// *************************
	// Token setup for AJAX call, Laravel requirement
	// *************************
	var baseUrl = $('meta[name="base-url"]').attr('content');
	$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	});
	$(".chzn-select").chosen({allow_single_deselect: true});
	$('.switch-wrapper').on('click','.selected-wrapper', function() {
		if( $(this).hasClass('active') ) {
			$(this).removeClass('active');
			$(this).next('.options-wrapper').removeClass('active');
		} else {
			$(this).addClass('active');
			$(this).next('.options-wrapper').addClass('active');
		}
	});

	/*// remove function
	$(document).on('click','.btn-remove-row',function(){

		let id = this.id;
		let split_id = id.split("_");
		let deleteindex = split_id[1];

		$("#div_" + deleteindex).remove();
		return false;

	});*/

	/*var counter = 0;
	// add sub option
	$(document).on('click','.btn-yellow-option',function(){

		counter ++;
		let optionId = this.id;
		let split_id = optionId.split("_");
		let subOptionindex = Number(split_id[1]);

		$("#option_" + subOptionindex).append('<ul class="list-unstyled mrg10T radio-ul" id="subText_'+ counter +'"><li><div class="input-group mrg5B"><div class="input-group-btn"><a class="btn btn-danger btn-remove-option"><i class="fa fa-trash"></i></a></div><input type="text" name="survey[items]['+subOptionindex+'][options]['+counter+']" required="required" class="form-control"></div></li></ul>');

		return false;

	});*/

	/*// remove function
	$(document).on('click','.btn-remove-option',function(){

		let id = $('.radio-ul').attr("id");
		let split_id = id.split("_");
		let deleteindex = split_id[1];

		$("#subText_" + deleteindex).remove();
		return false;

	});*/


	// *************************
	// Form Plugins
	// *************************
	/*$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		todayHighlight: true,
		autoclose: true,
		orientation: "bottom"
		// popup: {
		//     position: "bottom left",
		//     origin: "top left"
		// },
	});*/

	// $(".chzn-select").chosen({allow_single_deselect: true});

    $(".chosen-container").css({width:'90%'});
	$(".phone").inputmask({"mask": "(999) 999-9999"});
	$(".mobile-phone").inputmask({"mask": "+63 (999)-999-9999"});

	// *************************
	// Tooltips Plugins
	// *************************

	// *************************
	// DataTable Plugin
	// use 'data-order="[[0, 'ASC']]"' @ table element to set the default sorting column
	// use 'data-orderable="false"' @ th element to set if column is sortable
	// use 'data-type="currency"' @ td element to set the data type of the cell eg: date, currency, number
	// *************************
	if ( $('.table-data').length ) {
        $('.table-data').each(function() {
            var aoColumns = [],
                $this = $(this);

            var $doIndex,
                $doOrdering,
                options = {
                    responsive: true,
                    paging : false,
                    filter : false,
                    "bInfo": false
                };

            if( $(this).data('default-order') ) {
                $doIndex = i;
                $doOrdering = $(this).attr('data-default-order');
            } else {
                $doIndex = 0;
                $doOrdering = 'desc';
            }

            if( $(this).data('no-default-order') ) {
            	options.order = [];
            } else {
            	options.order = [[$doIndex, $doOrdering]];
            }

            if ($this.find('thead > tr').length == 1) {
                $this.find('thead th').each(function(i) {
                    var sType = $(this).data("type"),
                    	oFalse = $(this).data("orderable");

                    if(sType != '' || sType != undefined){
                    	aoColumns.push({ "type" : sType, "targets" : i });
                    	// if (sType != 'status-sort') {
                    	// 	aoColumns.push({ "type" : sType, "targets" : i });
                    	// } else {
                    	// 	aoColumns.push({ "targets" : i, "render" : 	function ( data, sType, full, meta ) {
                         // alert(sType);
						 //        if (stype == 'status-sort') {
						 //            return $(data).find('span').hasClass('Move Up') ? 1 : 0;
						 //        }  else {
						 //            return data;
						 //        }
					    //   	}
                    	// 	});
                    	// }
                    }
                    if (oFalse != undefined){
	                    aoColumns.push({ "orderable" : oFalse, "targets" : i });
	                }
                    options.columnDefs = aoColumns;
                });
            }

            $.fn.dataTable.moment( 'MMM D, YYYY' );
            $this.DataTable(options);
        });

		$('.table-data').parent().parent().css('width', '100%');
    }

    // *************************
	// Sweetalert 2 prompts
	// *************************
	$(".confirm").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmWarning").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonText: "Yes, continue!",
			confirmButtonColor: "#DD6B55",
			closeOnConfirm: false
		}).then(function(isConfirm) {
			if (isConfirm) {
				window.location = _self.attr("href")
			}
		});
	})

	$(".confirmDelete").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You won\'t be able to revert this.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete it!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
	      	if (isConfirm) {
				// _self.closest("form").submit();
				window.location.href = _self.attr('href');
	      	}
	    });
	});

	$(".confirmCancelSubmit").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || 'You can try this again later.';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
	      	if (isConfirm) {
		        _self.closest("form").submit();
	      	}
	    });
	});

	$(".confirmSubmit").click(function(e) {
		var msg = $(this).data('confirm') || 'Are you sure?';
		var msgText = $(this).data('confirm-text') || '';
		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonColor: "#449d44",
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}).then(function(isConfirm) {
	      	if (isConfirm) {
		        _self.closest("form").submit();
	      	}
	    });
	});

	$(".campaignCreationCheckSubaccounts").click(function(e) {
		e.preventDefault();
		var _self   = $(this);
		var msg     = $(this).data('confirm') || 'Create a Sub Account';
		var msgText = $(this).data('confirm-text') || '';
		var validateSubaccounts = $(this).data('validate-subaccounts');
		if( validateSubaccounts > 0 ) {
			window.location.href = _self.attr('href');
		} else {
			swal({
				title: msg,
				text: msgText,
				type: "info",
				showCancelButton: true,
				confirmButtonColor: "#449d44",
				confirmButtonText: "Yes, Create One!",
				closeOnConfirm: false
			}).then(function(isConfirm) {
				  if (isConfirm) {
					window.location.href = _self.data('subaccount-url');
				  }
			});
		}
	});

	//FOR DATERANGE PICKER
	$('.input-daterange').daterangepicker({
		autoApply: true,
		autoUpdateInput: false,
		ranges: {
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		}
	}, function(start, end, label) {

		var selectedStartDate = start.format('YYYY-MM-DD');
		var selectedEndDate = end.format('YYYY-MM-DD');

		$checkinInput = $('#from_date_range');
		$checkoutInput = $('#to_date_range');

		$checkinInput.val(selectedStartDate);
		$checkoutInput.val(selectedEndDate);

		// Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
		// var checkOutPicker = $checkoutInput.data('daterangepicker');
		// checkOutPicker.setStartDate(selectedStartDate);
		// checkOutPicker.setEndDate(selectedEndDate);

		// Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
		// var checkInPicker = $checkinInput.data('daterangepicker');
		// checkInPicker.setStartDate(selectedStartDate);
		// checkInPicker.setEndDate(selectedEndDate);
	});

	// Date range picker
	setTimeout(() => {
		$('#date_range').daterangepicker({
			autoUpdateInput: false,
			locale: {
				cancelLabel: 'Clear'
			}
		});

		// $('#date_range').on('apply.daterangepicker', function(ev, picker) {
		// 	$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		// 	return false;
		// });
	}, 1000);
});


