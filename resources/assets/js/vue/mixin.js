export default {

	mounted() {
		setTimeout(() => {
			this.checkBoxToggle();
			this.multiSelectInitialized();
			// this.dateRangePickerInitialized();
			this.fileUploadsInitialized();
			this.phoneFormat();
			this.getDomainUrl();
		}, 200);
	},

	data : function() {
		return {
			authUser : false,
			baseUrl: window.location.origin,
			urlPrefix : 'api/',
			demo: false,
			adminFirstName: '',
			adminLastName: '',
			polling: null,
            loginInterval:null,
		}
	},

	computed : {
		api : function() {
			return {
				// General API's
				checklogin : this.urlPrefix + 'check-login',

				// User API's
				userIndex : this.urlPrefix + 'users',
				userCreate : this.urlPrefix + 'users/create/',
				userEdit : this.urlPrefix + 'users/edit/',
				userDelete : this.urlPrefix + 'users/delete/',

				// audit API's
				auditIndex : this.urlPrefix + 'audits',

				// Feedback Answer API's
				feedbackEntriesIndex : this.urlPrefix + 'feedback/entries',
				feedbackEntriesIndexFiltered : this.urlPrefix + 'feedback/'+this.$route.params.contsid+'/entries',
				feedbackEntryView : this.urlPrefix + 'feedback/entry/'+this.$route.params.entryid+'/show',
				feedbackEntryDelete : this.urlPrefix + 'feedback/entry/delete/',

				// User API's
				permissionIndex : this.urlPrefix + 'permissions',
				permissionCreate : this.urlPrefix + 'permissions/create/',
				permissionEdit : this.urlPrefix + 'permissions/edit/',
				permissionDelete : this.urlPrefix + 'permissions/delete/',

				// Contest API's
				roleIndex : this.urlPrefix + 'roles',
				roleCreate : this.urlPrefix + 'roles/create/',
				roleEdit : this.urlPrefix + 'roles/edit/',
				roleDelete : this.urlPrefix + 'roles/delete/',

				// Contest API's
				contestIndex : this.urlPrefix + 'contests',
				contestCreate : this.urlPrefix + 'contests/create',
				contestEdit : this.urlPrefix + 'contests/edit/',
				contestArchive: this.urlPrefix + 'contests/move-to-archive/',
				contestDelete : this.urlPrefix + 'contests/delete/',

				//Archive
				archiveFeedbackIndex : this.urlPrefix + 'archive/feedback',
				archiveFeedbackEntries : this.urlPrefix + 'archive/feedback/entries',

				// Contest Items API's
				contestItemsCreate : this.urlPrefix + 'contests/'+this.$route.params.contsid+'/items/store',
				contestItemsEdit : this.urlPrefix + 'contests/'+this.$route.params.contsid+'/items/edit',
				contestItemsUpdate : this.urlPrefix + 'contests/'+this.$route.params.contsid+'/items/update',
				contestItemsDelete : this.urlPrefix + 'contests/'+this.$route.params.contsid+'/items/delete/',

				// Product API's
				productIndex : this.urlPrefix + 'products',
				productCreate : this.urlPrefix + 'products/create/',
				productEdit : this.urlPrefix + 'products/edit/',
				productDelete : this.urlPrefix + 'products/delete',

				// Product Categories API's
				productCategoryIndex : this.urlPrefix + 'product-category',
				productCategoryCreate : this.urlPrefix + 'product-category/create/',
				productCategoryEdit : this.urlPrefix + 'product-category/edit/',
				productCategoryDelete : this.urlPrefix + 'product-category/delete/',

				// Color API's
				colorIndex : this.urlPrefix + 'colors',
				colorCreate : this.urlPrefix + 'colors/create/',
				colorEdit : this.urlPrefix + 'colors/edit/',
				colorDelete : this.urlPrefix + 'colors/delete/',

				// Size API's
				sizeIndex : this.urlPrefix + 'sizes',
				sizeEdit  : this.urlPrefix + 'sizes/edit/',
                sizeDelete: this.urlPrefix + 'sizes/delete/',
                sizeAllCategories: this.urlPrefix + 'sizes/sizecategories/',
                sizePerCategories: this.urlPrefix + 'sizes/sizepercat/',

				// Size Categories API's
				sizesCategoryIndex : this.urlPrefix + 'sizes-category',
				sizesCategoryCreate : this.urlPrefix + 'sizes-category/create/',
				sizesCategoryEdit : this.urlPrefix + 'sizes-category/edit/',
				sizesCategoryDelete : this.urlPrefix + 'sizes-category/delete/',

				// Settings API's
				settingIndex : this.urlPrefix + 'settings',
				settingUpdate : this.urlPrefix + 'settings/update',
				settingDelete : this.urlPrefix + 'settings/delete/'
			}
		}
	},
	methods : {
        getPublicImages: function() {
            return `${window.location.origin}/app-bluecorner/public/images/`;
        },
        getAssetsImages: function() {
            return `${window.location.origin}/app-bluecorner/public/assets/img/`;
        },
        getDomainUrl: function() {
            return `${window.location.origin}/app-bluecorner/public/`;
        },
		createNotif: function(notifMessage, type = 'success') {
			$.jGrowl({
				message: notifMessage,
				life: 3000,
				theme: (type == 'error') ? 'bg-red' : ( type == 'warning' ? 'bg-orange' : 'bg-green' ),
				openDuration: 200
			});
		},
		removeFileExt: function(data) {
			return data.split('.').slice(0, -1).join('.');
		},
		phoneFormat: function () {
			$(".phone").inputmask({"mask": "(999) 999-9999"});
			$(".mobile-phone").inputmask({"mask": "+63 (999)-999-9999"});
		},
		editorInitialized: function () {
			$('.summernote_editor').summernote({
		        height:200,
		        fontNames: ['Source Sans Pro', 'Roboto', 'Arial', 'Courier New']
		    });
		},
		selectArr: function(data) {
			let getData = data;
			let selectFormattedArray = [];
			getData.forEach((item) => {
				selectFormattedArray.push({
					id:'',
					label:item.name,
					value:item.id,
					hex:item.value,
					productvariation_image_name:'',
					productvariation_image_old_name:'',
					productvariation_image_id:'',
					productvariation_image_url:'',
					productvariation_color_name:'',
					productvariation_color_old_name:'',
					productvariation_color_id:'',
					productvariation_color_url:'',
				});
			});
			return selectFormattedArray;
		},
		countObject : function (collection) {
			return _.size(collection);
		},
		fileUploadsInitialized: function() {
			$('.dropify').dropify();
			$("[data-max-file-size]").dropify({
				error: {
					'fileSize': 'The file size is too big ({{ value }}B max).'
				}
			});
		},
		dateRangePickerInitialized: function () {
	        // Date range picker
	        $('#date_range').daterangepicker({
	            autoUpdateInput: true,
	            locale: {
	                cancelLabel: 'Clear'
	            }
	        });
		},
		formatDate : function (dateStr, format) {
			return new moment(dateStr).format(format);
		},
		checkBoxToggle: function() {
			$.each($('.make-switch-radio'), function () {
				$(this).bootstrapSwitch({
					onText: $(this).data('onText'),
					offText: $(this).data('offText'),
					onColor: $(this).data('onColor'),
					offColor: $(this).data('offColor'),
					size: $(this).data('size'),
					labelText: $(this).data('labelText')
				});
			});
		},
		multiSelectInitialized: function () {
			$(".hide_search").chosen({disable_search_threshold: 10});
			$(".chzn-select").chosen({allow_single_deselect: true});
			$(".chzn-select-deselect,#select2_sample").chosen();
		},
		makeJson : function(stringData) {
			return JSON.parse(stringData);
		},
		checkUser : function() {
			this.doApiCheckUser()
			.then(function(response) {
				let body = response.body;
				if (body.success) {
					this.authUser = body.object;
					this.adminFirstName = body.object.first_name;
					this.adminLastName = body.object.last_name;
					this.validateUser();
					return;
				} else {
					this.authUser = false;
				}
			}, function (error) {
				this.errors = error.body.errors;
			});
		},
		doApiCheckUser: function() {
			return this.$http.get(this.api.checklogin);
		},
		validateUser: function() {
			if (this.polling == null) {
				this.polling = 'initial';
				this.polling = setInterval(() => {
					this.doApiCheckUser()
						.then(function(response) {
							let body = response.body;
							if (body && body.success) {
								console.log('Authenticate success!');
								this.authUser = body.object;
							} else {
								console.log('No authenticated user!');
								this.redirectLogin();
							}
						}, function(error) {
							this.errors = error.body.errors;
						});
				}, 6000000); //6000000
			}
		},
		redirectLogin:function() {
			this.createNotif('Your session has expired, you will be logout in 5 secs','warning');
			clearTimeout(this.polling);
			setTimeout(() =>{
				window.location.href = this.url+'/login';
			}, 5000);
		},
	},
}
