
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Router from 'vue-router';
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

window.Vue = require('vue');
Vue.use(require('vue-resource'));
Vue.use(Router);
Vue.use(VuejsDialog);

// Vue.use(require('vue-resource'));
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// index for admin page
import DashboardIndex from './components/Admin/Dashboard/Index.vue';

// index for auth page
import InitialComponent from './components/InitialLoadComponent.vue';

Vue.component('dashboard-index', DashboardIndex);
Vue.component('initial-load-component', InitialComponent);

import Mixin from './mixin';
Vue.mixin(Mixin);

Vue.http.options.root = $('meta[name="base-url"]').attr('content');
Vue.http.interceptors.push(function (request, next) {
	request.headers['X-CSRF-TOKEN'] = $('meta[name="csrf_token"]').attr('content');
	next();
});

import Routes from './router';
let router = new Router(Routes);

import { Chrome } from 'vue-color'
Vue.component('chrome-picker', Chrome);

import vSelect from 'vue-select';
Vue.component('v-select', vSelect);

import Multiselect  from 'vue-multiselect';
Vue.component('multiselect', Multiselect );

import ToggleCheckbox  from 'vue-checkbox-toggle';
Vue.component('vue-checkbox-toggle', ToggleCheckbox );

import draggable from 'vuedraggable';
Vue.component('draggable', draggable );

import { VueEditor } from 'vue2-editor';
Vue.component('vue-editor', VueEditor );

import vueDropify from 'vue-dropify';
Vue.component('vue-dropify', vueDropify );


import MultiFiltersPlugin from '../pluginjs/multiFilters'
Vue.use(MultiFiltersPlugin);

import VueMatchHeights from 'vue-match-heights';
Vue.use(VueMatchHeights);

// import Sortable from 'sortablejs/modular/sortable.complete.esm.js';
// Vue.use(Sortable);

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify)
new Vue();

// for fixing datepicker bug
Vue.component('date-range-picker', {
	props:['id'],
	template: '<input type="text" :id="id" :name="id" />',
	mounted: function(){
		var self = this;
		var input = $('input[name="'+ this.id +'"]');
		input.daterangepicker();
		input.on('apply.daterangepicker', function(ev, picker) {
			self.$emit('daterangechanged',picker);
		});
	}
});

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	router,
    el: '#app',
});
