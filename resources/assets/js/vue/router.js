// index for admin page
import DashboardIndex from './components/Admin/Dashboard/Index.vue';

// index for auth page //
import LoginIndex from './components/Auth/Login/Login.vue';
import ResetIndex from './components/Auth/Password/Reset.vue';

// users vue js //
import UsersIndex from './components/Admin/Users/index.vue';
import UsersCreate from './components/Admin/Users/create.vue';
import UsersEdit from './components/Admin/Users/edit.vue';
// end //

// audit vue js //
import AuditIndex from './components/Admin/Audit/index.vue';
// end //

// users vue js //
import RolesIndex from './components/Admin/Roles/index.vue';
import RolesCreate from './components/Admin/Roles/create.vue';
import RolesEdit from './components/Admin/Roles/edit.vue';
// end //

// permission vue js //
import PermissionIndex from './components/Admin/Permissions/index.vue';
import PermissionCreate from './components/Admin/Permissions/create.vue';
import PermissionEdit from './components/Admin/Permissions/edit.vue';
// end //

// Feedback vue js //
import FeedbackEntriesIndex from './components/Admin/FeedbackEntry/index.vue';
import FeedbackEntriesIndexFiltered from './components/Admin/FeedbackEntry/index.vue';
import FeedbackEntryView from './components/Admin/FeedbackEntry/view.vue';
// end //

// Feedback Atchive vue js //
import FeedbackArchive from './components/Admin/Contests/index.vue';
import FeedbackArchiveEntriesIndexFiltered from './components/Admin/FeedbackEntry/index.vue';
import FeedbackArchiveEntries from './components/Admin/FeedbackEntry/index.vue';
import FeedbackArchiveEntryView from './components/Admin/FeedbackEntry/view.vue';
// end //

// Contests vue js //
import ContestsIndex from './components/Admin/Contests/index.vue';
import ContestsCreate from './components/Admin/Contests/create.vue';
import ContestsEdit from './components/Admin/Contests/edit.vue';
// end //

// contestItems vue js //
import ContestItemsCreateIndex from './components/Templates/Items/items.vue';
import ContestItemsEditIndex from './components/Templates/Items/edit.vue';

// products vue js //
import ProductIndex from './components/Admin/Products/index.vue';
import ProductCreate from './components/Admin/Products/create.vue';
import ProductEdit from './components/Admin/Products/edit.vue';
// end //

// products Categories vue js //
import ProductCategoriesIndex from './components/Admin/ProductCategories/index.vue';
import ProductCategoriesCreate from './components/Admin/ProductCategories/create.vue';
import ProductCategoriesEdit from './components/Admin/ProductCategories/edit.vue';
// end //

// Colors vue js //
import ColorsIndex from './components/Admin/Colors/index.vue';
import ColorsCreate from './components/Admin/Colors/create.vue';
import ColorsEdit from './components/Admin/Colors/edit.vue';
// end //

// sizes vue js //
import SizesIndex from './components/Admin/Sizes/index.vue';
import SizesCreate from './components/Admin/Sizes/create.vue';
import SizesEdit from './components/Admin/Sizes/edit.vue';
// end //

// Sizes Categories vue js //
import SizesCategoriesIndex from './components/Admin/SizesCategories/index.vue';
import SizesCategoriesCreate from './components/Admin/SizesCategories/create.vue';
import SizesCategoriesEdit from './components/Admin/SizesCategories/edit.vue';
// end //

// setting vue js //
import SettingIndex from './components/Admin/Settings/index.vue';
// end //

export default
{
	base : '/app-bluecorner/public',
	mode : 'history',
	routes: [
		{
			path : '/login',
			name : 'login',
			component : LoginIndex,
			props: true
		},
		{
			path : '/logout',
			name : 'logout',
		},
		{
			path : '/reset',
			name : 'Reset',
			component : ResetIndex,
		},
		{
			path : '/admin/dashboard',
			name : 'dashboard-index',
			component : DashboardIndex,
			props: true
		},
		{
			path : '/admin/Audit',
			name : 'audit-index',
			component : AuditIndex,
			props: true
		},
		{
			path : '/admin/users',
			name : 'users-index',
			component : UsersIndex,
			props: true
		},
		{
			path : '/admin/users/create',
			name : 'users-create',
			component : UsersCreate,
			props: true
		},
		{
			path : '/admin/users/edit/:userid',
			name : 'users-edit',
			component : UsersEdit,
			props: true
		},
		{
			path : '/admin/roles',
			name : 'roles-index',
			component : RolesIndex,
			props: true
		},
		{
			path : '/admin/roles/create',
			name : 'roles-create',
			component : RolesCreate,
			props: true
		},
		{
			path : '/admin/roles/edit/:roleid',
			name : 'roles-edit',
			component : RolesEdit,
			props: true
		},
		{
			path : '/admin/permissions',
			name : 'permissions-index',
			component : PermissionIndex,
			props: true
		},
		{
			path : '/admin/permissions/create',
			name : 'permissions-create',
			component : PermissionCreate,
			props: true
		},
		{
			path : '/admin/permissions/edit/:permissionid',
			name : 'permissions-edit',
			component : PermissionEdit,
			props: true
		},
		{
			path : '/admin/archive/feedback',
			name : 'feedback-archive',
			component : FeedbackArchive,
			props: {
				archive: true
			}
		},
		{
			path : '/admin/feedback',
			name : 'contests-index',
			component : ContestsIndex,
			props: {
				archive: false
			}
		},
		{
			path : '/admin/feedback/create',
			name : 'contests-create',
			component : ContestsCreate,
			props: true
		},
		{
			path : '/admin/feedback/edit/:contsid',
			name : 'contests-edit',
			component : ContestsEdit,
			props: true
		},
		{
			path : '/admin/feedback/:contsid/items',
			name : 'contest-items-create',
			component : ContestItemsCreateIndex,
			props: true
		},
		{
			path : '/admin/feedback/:contsid/edit-items',
			name : 'contest-items-edit',
			component : ContestItemsEditIndex,
			props : {
				xpostAction: 'update'
			}
		},
		{
			path : '/admin/feedback/:contsid/entries',
			name : 'feedback-entries-index-filtered',
			component : FeedbackEntriesIndexFiltered,
			props: {
				test : true,
				archive: false
			}
		},
		{
			path : '/admin/archive/feedback/:contsid/entries',
			name : 'feedback-archive-entries-index-filtered',
			component : FeedbackArchiveEntriesIndexFiltered,
			props: {
				test : true,
				archive: true
			}
		},
		{
			path : '/admin/feedback/entries',
			name : 'feedback-entries-index',
			component : FeedbackEntriesIndex,
			props: {
				archive: false
			}
		},
		{
			path : '/admin/archive/feedback/entries',
			name : 'feedback-archive-entries',
			component : FeedbackArchiveEntries,
			props: {
				archive: true
			}
		},
		{
			path : '/admin/feedback/entry/:entryid/show',
			name : 'feedback-entry-view',
			component : FeedbackEntryView,
			props: {
				archive: false
			}
		},
		{
			path : '/admin/archive/feedback/entry/:entryid/show',
			name : 'feedback-archive-entry-view',
			component : FeedbackArchiveEntryView,
			props: {
				archive: true
			}
		},
		{
			path : '/admin/products',
			name : 'products-index',
			component : ProductIndex,
			props: true
		},
		{
			path : '/admin/products/create',
			name : 'products-create',
			component : ProductCreate,
			props: true
		},
		{
			path : '/admin/products/edit/:prodid',
			name : 'products-edit',
			component : ProductEdit,
			props: true
		},
		{
			path : '/admin/product-category',
			name : 'product-category-index',
			component : ProductCategoriesIndex,
			props: true
		},
		{
			path : '/admin/product-category/create',
			name : 'product-category-create',
			component : ProductCategoriesCreate,
			props: true
		},
		{
			path : '/admin/product-category/edit/:catid',
			name : 'product-category-edit',
			component : ProductCategoriesEdit,
			props: true
		},
		{
			path : '/admin/colors',
			name : 'colors-index',
			component : ColorsIndex,
			props: true
		},
		{
			path : '/admin/colors/create',
			name : 'colors-create',
			component : ColorsCreate,
			props: true
		},
		{
			path : '/admin/colors/edit/:colorid',
			name : 'colors-edit',
			component : ColorsEdit,
			props: true
		},
		{
			path : '/admin/sizes',
			name : 'sizes-index',
			component : SizesIndex,
			props: true
		},
		{
			path : '/admin/sizes/create',
			name : 'sizes-create',
			component : SizesCreate,
			props: true
		},
		{
			path : '/admin/sizes/edit/:sizeid',
			name : 'sizes-edit',
			component : SizesEdit,
			props: true
		},
		{
			path : '/admin/sizes/delete/:sizeid',
			name : 'sizes-delete',
			component : SizesIndex,
			props: true
        },
		{
			path : '/admin/sizes-category',
			name : 'sizes-category-index',
			component : SizesCategoriesIndex,
			props: true
		},
		{
			path : '/admin/sizes-category/create',
			name : 'sizes-category-create',
			component : SizesCategoriesCreate,
			props: true
		},
		{
			path : '/admin/sizes-category/edit/:sizecatid',
			name : 'sizes-category-edit',
			component : SizesCategoriesEdit,
			props: true
		},
		{
			path : '/admin/sizes-category/delete/:sizecatid',
			name : 'sizes-category-delete',
			component : SizesCategoriesIndex,
			props: true
		},
		{
			path : '/admin/settings',
			name : 'settings-index',
			component : SettingIndex,
			props: true
		},
		{ path: "*", component: DashboardIndex }
	]
}
