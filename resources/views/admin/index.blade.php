@extends('layouts.dashboard_fixed_header')
@section('content')
	<dashboard-index
		url="{{ url('/') }}"
		auth-user="{{ $users }}"
	>
	</dashboard-index>
@endsection
