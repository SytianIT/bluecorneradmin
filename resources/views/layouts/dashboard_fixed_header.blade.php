<!doctype html>
<html class="no-js" lang="en">

	<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base-url" content="{{ url('/') }}">
    <meta name="robots" content="noindex">

    <title>Blue Corner Admin</title>

    <link rel="apple-touch-icon-precomposed" sizes="144x144" hrefj="{{ asset('favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('favicon.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/layouts.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/all-plugins.css') }}?v=0.0"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/components.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/custom.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}?v=0.0" />

    <link type="text/css" rel="stylesheet" href="#" id="skin_change"/>
    <!-- end of global styles-->

    @yield('header_styles')
	</head>

	<body class="fixedMenu_header fixed_header">

		<div id="app" class="bg-container">
      <main>
      	@yield('content')
      </main>
    </div>

	  <div class="preloader">
			<div class="preloader_img">
		  	<img src="{{asset('assets/img/loader.gif')}}" style="width: 40px;" alt="loading...">
		  </div>
		</div>

        <!-- global scripts-->
        <script type="text/javascript" src="{{asset('assets/js/all-plugins.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/vue.js')}}"></script>
        {{-- <script type="text/javascript" src="{{asset('js/admin.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script> --}}
        {{-- <script type="text/javascript" src="{{asset('assets/js/pages/fixed_menu.js')}}"></script> --}}
        {{-- Isama na ito sa webpack compinaltion ng all-plugin.js --}}
		<!-- end of global scripts-->
		<!-- page level js -->
		@yield('footer_scripts')
		<!-- end page level js -->

	</body>
</html>
