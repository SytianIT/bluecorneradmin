<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContestEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contest_entries', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('contest_id')->nullable();
			$table->string('contest_title')->nullable();
			$table->string('name')->nullable();
			$table->date('birthday')->nullable();
			$table->string('contact_number')->nullable();
			$table->string('message')->nullable();
			$table->string('email')->nullable();
			$table->string('device_uid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contest_entries');
    }
}
