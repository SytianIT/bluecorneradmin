<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypeToContestEntriesAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest_entries_answers', function (Blueprint $table) {
			$table->enum('type', ['text','radio','checkbox'])->default('text')->after('contest_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_entries_answers', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
