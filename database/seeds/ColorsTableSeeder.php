<?php

use BC\Repositories\Color\Color;

use Illuminate\Database\Seeder;
class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = new Color();
        $colors->name = 'Red';
        $colors->value = '#B60E0E';
        $colors->order = 1;
        $colors->save();

        $colors = new Color();
        $colors->name = 'Green';
        $colors->value = '#00BA5E';
        $colors->order = 2;
        $colors->save();

        $colors = new Color();
        $colors->name = 'Blue';
        $colors->value = '#0A23D7';
        $colors->order = 3;
        $colors->save();

        $colors = new Color();
        $colors->name = 'Yellow';
        $colors->value = '#FFF124';
        $colors->order = 4;
        $colors->save();

        $colors = new Color();
        $colors->name = 'Violet';
        $colors->value = '#870E63';
        $colors->order = 5;
        $colors->save();

    }
}
