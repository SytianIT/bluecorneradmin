<?php
use BC\Repositories\ProductCategory\ProductCategory;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = new ProductCategory();
        $cat->name = 'Category 1';
        $cat->description = 'category 1';
        $cat->save();
    }
}
