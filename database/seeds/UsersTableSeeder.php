<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $suUser = new User();
        $suUser->first_name = 'Super';
        $suUser->last_name = 'Admin';
        $suUser->email = 'admin@email.com';
        $suUser->password = bcrypt('admin123');
        $suUser->active = true;
        $suUser->save();
    }
}
