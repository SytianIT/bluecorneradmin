<?php
use BC\Repositories\Sizes\Sizes;
use Illuminate\Database\Seeder;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sizes = new Sizes();
        $sizes->name = 'Small';
        $sizes->description = 'Small';
        $sizes->width = '20';
        $sizes->length = '20';
        $sizes->order = 1;
        $sizes->save();

        $sizes = new Sizes();
        $sizes->name = 'Medium';
        $sizes->description = 'Medium';
        $sizes->width = '40';
        $sizes->length = '40';
        $sizes->order = 2;
        $sizes->save();

        $sizes = new Sizes();
        $sizes->name = 'Large';
        $sizes->description = 'Large';
        $sizes->width = '60';
        $sizes->length = '60';
        $sizes->order = 3;
        $sizes->save();

        $sizes = new Sizes();
        $sizes->name = 'Extra large';
        $sizes->description = 'Extra Large';
        $sizes->width = '80';
        $sizes->length = '80';
        $sizes->order = 5;
        $sizes->save();

    }
}
