const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var dest = 'resources/assets/vendors/';

var nm   = 'node_modules/vuetify/';

mix.js([
        'resources/assets/js/admin.js',
        ], 'public/js/admin.js')
    .sass(
        'resources/assets/sass/app.scss',
        'public/css/app.css')
    .options({ processCssUrls: false  });

mix.js([
		'resources/assets/js/pluginjs/jquery-1.11.0.min.js',
		], 'public/js/jquery.js');


/**
 * Admire Template Compiler
 * Faild to include | c3 - d3
 */
mix.styles(
    [
        dest + 'veutify-css/veutify.min.css',
        dest + 'components.css',
        dest + 'veu-dropify/veu-dropify.css',
        dest + 'custom.css',
        dest + 'layouts.css',
        dest + 'dropify/css/dropify.css',
        dest + 'chartist/css/chartist.min.css',
        dest + 'bootstrap-switch/css/bootstrap-switch.min.css',
        dest + 'bootstrap-colorpicker/css/bootstrap-colorpicker.min.css',
        dest + 'circliful/css/jquery.circliful.css',
        dest + 'toastr/css/toastr.min.css',
        dest + 'switchery/css/switchery.min.css',
        dest + 'pnotify/css/pnotify.css',
        dest + 'pnotify/css/pnotify.buttons.css',
        dest + 'sweetalert/css/sweetalert2.min.css',
        dest + 'jasny-bootstrap/css/jasny-bootstrap.min.css',
        dest + 'bootstrapvalidator/css/bootstrapValidator.min.css',
        dest + 'inputlimiter/css/jquery.inputlimiter.css',
        dest + 'tooltipster/css/tooltipster.bundle.min.css',
        dest + 'tipso/css/tipso.min.css',
        dest + 'animate/css/animate.min.css',

        dest + 'chosen/css/chosen.min.css',
        dest + 'select2/css/select2.min.css',
        dest + 'datatables/css/buttons.bootstrap.css',
        dest + 'datatables/css/scroller.bootstrap.min.css',
        dest + 'datatables/css/colReorder.bootstrap.min.css',
        dest + 'datatables/css/rowReorder.bootstrap.css',
        dest + 'datatables/css/dataTables.bootstrap.css',

        dest + 'datepicker/css/bootstrap-datepicker.min.css',
        dest + 'bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        dest + 'datepicker/css/bootstrap-datepicker3.css',

        dest + 'jasny-bootstrap/css/jasny-bootstrap.min.css',
        dest + 'jquery-tagsinput/css/jquery.tagsinput.min.css',
        dest + 'multiselect/css/multi-select.css',

        dest + 'daterangepicker/css/daterangepicker.css',
        dest + 'checkbox_css/css/checkbox.min.css',
        dest + 'radio_css/css/radiobox.min.css',
        dest + 'radio_checkbox/css/radio_checkbox.css',

        // Skins - Choose one only
        dest + 'admire-skins/mint_black_skin.css',
        dest + 'vue-multiselect/vue-multiselect.css',
        dest + 'dropzone/css/dropzone.min.css',
        dest + 'fileinput/css/fileinput.min.css',
        dest + 'summernote/css/summernote.css',
    ],
    'public/assets/css/all-plugins.css'
);

mix.scripts(
    [
        // General Plugins
        dest + 'components.js',
        dest + 'custom.js',

        // Notifications
        dest + 'countUp.js/js/countUp.min.js',
        dest + 'dropify/js/dropify.js',
		dest + 'pnotify/js/pnotify.js',
		dest + 'pnotify/js/pnotify.animate.js',
		dest + 'pnotify/js/pnotify.buttons.js',
		dest + 'jquery.newsTicker/js/newsTicker.js',
		dest + 'sweetalert/js/sweetalert2.min.js',

		// Charts and Any Graphic Plugins
		dest + 'flotchart/js/jquery.flot.js',
		dest + 'flotchart/js/jquery.flot.resize.js',
		dest + 'flotchart/js/jquery.flot.stack.js',
		dest + 'flotchart/js/jquery.flot.time.js',
		dest + 'flotspline/js/jquery.flot.spline.min.js',
		dest + 'flotchart/js/jquery.flot.categories.js',
		dest + 'flotchart/js/jquery.flot.pie.js',
		dest + 'flot.tooltip/js/jquery.flot.tooltip.min.js',
		dest + 'circliful/js/jquery.circliful.min.js',
		dest + 'raphael/js/raphael.min.js',
		dest + 'swiper/js/swiper.min.js',
		dest + 'slimscroll/js/jquery.slimscroll.min.js',
		dest + 'pluginjs/jquery.sparkline.js',
		dest + 'flip/js/jquery.flip.min.js',
		dest + 'chartist/js/chartist.min.js',
		dest + 'pluginjs/chartist-tooltip.js',
		dest + 'tooltipster/js/tooltipster.bundle.min.js',
		dest + 'tipso/js/tipso.min.js',

        // Form Behavior Plugins
		dest + 'd3/js/d3.min.js',
		dest + 'c3/js/c3.min.js',
		dest + 'toastr/js/toastr.min.js',
		dest + 'switchery/js/switchery.min.js',
		dest + 'pluginjs/jquery.validVal.min.js',
        dest + 'jquery-sticky/jquery.sticky.js',
        dest + 'holderjs/js/holder.js',
        dest + 'pluginjs/jasny-bootstrap.js',
        dest + 'jquery.uniform/js/jquery.uniform.js',
        dest + 'bootstrapvalidator/js/bootstrapValidator.min.js',
        dest + 'datepicker/js/bootstrap-datepicker.min.js',
        dest + 'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        dest + 'bootstrap-switch/js/bootstrap-switch.min.js',
        dest + 'autosize/js/jquery.autosize.min.js',
        dest + 'inputlimiter/js/jquery.inputlimiter.js',
        dest + 'bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
        dest + 'jquery-tagsinput/js/jquery.tagsinput.min.js',
        dest + 'jasny-bootstrap/js/jasny-bootstrap.min.js',
        dest + 'jasny-bootstrap/js/inputmask.js',
        dest + 'datetimepicker/js/DateTimePicker.min.js',
        dest + 'j_timepicker/js/jquery.timepicker.min.js',
        dest + 'clockpicker/js/jquery-clockpicker.min.js',
        dest + 'inputmask/js/jquery.inputmask.bundle.js',
        dest + 'multiselect/js/jquery.multi-select.js',
		dest + 'chosen/js/chosen.jquery.min.js',
		dest + 'moment/js/moment.min.js',

        dest + 'datatables/js/jquery.dataTables.js',
		dest + 'pluginjs/dataTables.tableTools.js',
		dest + 'datatables/js/dateTime.moment.js',
        dest + 'datatables/js/dataTables.bootstrap.js',
        dest + 'datatables/js/dataTables.buttons.min.js',
        dest + 'datatables/js/dataTables.colReorder.js',
        dest + 'datatables/js/dataTables.responsive.js',
        dest + 'datatables/js/dataTables.responsive.min.js',
        dest + 'datatables/js/dataTables.rowReorder.min.js',
		dest + 'datatables/js/buttons.colVis.min.js',
		dest + 'datatables/js/buttons.html5.min.js',
		dest + 'datatables/js/buttons.bootstrap.min.js',
		dest + 'datatables/js/buttons.print.min.js',
        dest + 'datatables/js/dataTables.scroller.min.js',
        dest + 'daterangepicker/js/daterangepicker.js',
        dest + 'dropzone/js/dropzone.js',
        dest + 'fileinput/js/fileinput.min.js',
        dest + 'summernote/js/summernote.js',

        'node_modules/jgrowl/jquery.jgrowl.min.js',

    ],
    'public/assets/js/all-plugins.js'
);

mix.js([
    'resources/assets/js/vue/app.js',
], 'public/js/vue.js');
